$(function() {
$("#companyform").find("input,textarea,select").jqBootstrapValidation(
{
    preventSubmit: true,
    submitError: function($form, event, errors) {
        // Here I do nothing, but you could do something like display 
        // the error messages to the user, log, etc.
    },
    submitSuccess: function($form, event) {
    	alert();
       
        event.preventDefault();

            $.ajax({
		 	url: base_url+'Company_controller/savecomapny',
			data: new FormData($('#companyform')[0]),
			processData: false,
			contentType: false,
			type: 'POST',
			  success: function(data){
				  	$.toast({
			            heading: 'Success'
			            , text: 'your data add successfully.'
			            , position: 'top-right'
			            , loaderBg: '#ff6849'
			            , icon: 'success'
			            , hideAfter: 3500
			            , stack: 6
			        })
				  	 setTimeout(function(){ window.location.href = base_url+"Company_controller"; }, 3500);
				  
				  }
			});
    },
    filter: function() {
        return $(this).is(":visible");
    }
}
);


$("#addcategoryform").find("input,textarea,select").jqBootstrapValidation(
{
    preventSubmit: true,
    submitError: function($form, event, errors) {
        // Here I do nothing, but you could do something like display 
        // the error messages to the user, log, etc.
    },
    submitSuccess: function($form, event) {
       
        event.preventDefault();

            $.ajax({
		 	url: base_url+'Category_controller/savecategory',
			data: new FormData($('#addcategoryform')[0]),
			processData: false,
			contentType: false,
			type: 'POST',
			  success: function(data){
				  	 insert();
				  	 setTimeout(function(){ window.location.href = base_url+"Category_controller"; }, 3500);
				  
				  }
			});
    },
    filter: function() {
        return $(this).is(":visible");
    }
});

$("#addgreyqutform").find("input,textarea,select").jqBootstrapValidation(
{
    preventSubmit: true,
    submitError: function($form, event, errors) {
        // Here I do nothing, but you could do something like display 
        // the error messages to the user, log, etc.
    },
    submitSuccess: function($form, event) {
       
        event.preventDefault();

            $.ajax({
		 	url: base_url+'Grey_Quality/savegreyqty',
			data: new FormData($('#addgreyqutform')[0]),
			processData: false,
			contentType: false,
			type: 'POST',
			  success: function(data){
				  	 insert();
				  	 setTimeout(function(){ window.location.href = base_url+"Grey_Quality"; }, 3500);
				  
				  }
			});
    },
    filter: function() {
        return $(this).is(":visible");
    }
});
function update()
{
	$.toast({
        heading: 'Success'
        , text: 'your data edit successfully.'
        , position: 'top-right'
        , loaderBg: '#ff6849'
        , icon: 'success'
        , hideAfter: 3500
        , stack: 6
    })
}

function insert()
{
	$.toast({
        heading: 'Success'
        , text: 'your data save successfully.'
        , position: 'top-right'
        , loaderBg: '#ff6849'
        , icon: 'success'
        , hideAfter: 3500
        , stack: 6
    })
}


$("#editcompanyform").find("input,textarea,select").jqBootstrapValidation(
{
    preventSubmit: true,
    submitError: function($form, event, errors) {
        // Here I do nothing, but you could do something like display 
        // the error messages to the user, log, etc.
    },
    submitSuccess: function($form, event) {
       
        event.preventDefault();

            $.ajax({
		 	url: base_url+'Company_controller/editsavecomapny',
			data: new FormData($('#editcompanyform')[0]),
			processData: false,
			contentType: false,
			type: 'POST',
			  success: function(data){
			  	 update();
			  	 setTimeout(function(){ window.location.href = base_url+"Company_controller"; }, 3500);
			  
			  }
			});
    },
    filter: function() {
        return $(this).is(":visible");
    }
});

$("#editcategoryform").find("input,textarea,select").jqBootstrapValidation(
{
    preventSubmit: true,
    submitError: function($form, event, errors) {
        // Here I do nothing, but you could do something like display 
        // the error messages to the user, log, etc.
    },
    submitSuccess: function($form, event) {
       
        event.preventDefault();

            $.ajax({
		 	url: base_url+'Category_controller/savecategory',
			data: new FormData($('#editcategoryform')[0]),
			processData: false,
			contentType: false,
			type: 'POST',
			  success: function(data){
			  	 update();
			  	 setTimeout(function(){ window.location.href = base_url+"Category_controller"; }, 3500);
			  
			  }
			});
    },
    filter: function() {
        return $(this).is(":visible");
    }
});

$("#editscreenform").find("input,textarea,select").jqBootstrapValidation(
{
    preventSubmit: true,
    submitError: function($form, event, errors) {
        // Here I do nothing, but you could do something like display 
        // the error messages to the user, log, etc.
    },
    submitSuccess: function($form, event) {
       
        event.preventDefault();

            $.ajax({
		 	url: base_url+'Screenregister_controller/editscreensave',
			data: new FormData($('#editscreenform')[0]),
			processData: false,
			contentType: false,
			type: 'POST',
			  success: function(data){
			  	 update();
			  	 setTimeout(function(){ window.location.href = base_url+"Screenregister_controller"; }, 3500);
			  
			  }
			});
    },
    filter: function() {
        return $(this).is(":visible");
    }
});

$("#screenregisterform").find("input,textarea,select").jqBootstrapValidation(
{
    preventSubmit: true,
    submitError: function($form, event, errors) {
        // Here I do nothing, but you could do something like display 
        // the error messages to the user, log, etc.
    },
    submitSuccess: function($form, event) {
       
        event.preventDefault();

            $.ajax({
		 	url: base_url+'Screenregister_controller/savescreen',
			data: new FormData($('#screenregisterform')[0]),
			processData: false,
			contentType: false,
			type: 'POST',
			  success: function(data){
				  	 $.toast({
			            heading: 'Success'
			            , text: 'your data add successfully.'
			            , position: 'top-right'
			            , loaderBg: '#ff6849'
			            , icon: 'success'
			            , hideAfter: 3500
			            , stack: 6
			        })
				  	 setTimeout(function(){ window.location.href = base_url+"Screenregister_controller"; }, 3500);
				  
				  }
			});
    },
    filter: function() {
        return $(this).is(":visible");
    }
});


$("#additemtypeform").find("input,textarea,select").jqBootstrapValidation(
{
    preventSubmit: true,
    submitError: function($form, event, errors) {
        // Here I do nothing, but you could do something like display 
        // the error messages to the user, log, etc.
    },
    submitSuccess: function($form, event) {
       
        event.preventDefault();

            $.ajax({
		 	url: base_url+'Itemtype_controller/saveitemtype',
			data: new FormData($('#additemtypeform')[0]),
			processData: false,
			contentType: false,
			type: 'POST',
			  success: function(data){
				  	 insert();
				  	 setTimeout(function(){ window.location.href = base_url+"Itemtype_controller"; }, 3500);
				  
				  }
			});
    },
    filter: function() {
        return $(this).is(":visible");
    }
});

$("#edititemtypeform").find("input,textarea,select").jqBootstrapValidation(
{

    preventSubmit: true,
    submitError: function($form, event, errors) {
        // Here I do nothing, but you could do something like display 
        // the error messages to the user, log, etc.
    },
    submitSuccess: function($form, event) {
       
        event.preventDefault();

            $.ajax({
		 	url: base_url+'Itemtype_controller/saveitemtype',
			data: new FormData($('#edititemtypeform')[0]),
			processData: false,
			contentType: false,
			type: 'POST',
			  success: function(data){
				  	 insert();
				  	 setTimeout(function(){ window.location.href = base_url+"Itemtype_controller"; }, 3500);
				  
				  }
			});
    },
    filter: function() {
        return $(this).is(":visible");
    }
});

$("#addpackageform").find("input,textarea,select").jqBootstrapValidation(
{

    preventSubmit: true,
    submitError: function($form, event, errors) {
        // Here I do nothing, but you could do something like display 
        // the error messages to the user, log, etc.
    },
    submitSuccess: function($form, event) {
       
        event.preventDefault();

            $.ajax({
		 	url: base_url+'Packagestyle_controller/savepackage',
			data: new FormData($('#addpackageform')[0]),
			processData: false,
			contentType: false,
			type: 'POST',
			  success: function(data){
				  	 insert();
				  	 setTimeout(function(){ window.location.href = base_url+"Packagestyle_controller"; }, 3500);
				  
				  }
			});
    },
    filter: function() {
        return $(this).is(":visible");
    }
});

$("#addhasteform").find("input,textarea,select").jqBootstrapValidation(
{
    preventSubmit: true,
    submitError: function($form, event, errors) {
        // Here I do nothing, but you could do something like display 
        // the error messages to the user, log, etc.
    },
    submitSuccess: function($form, event) {
       
        event.preventDefault();

        $.ajax({
	 	url: base_url+'Hastelist_controller/savehaste',
		data: new FormData($('#addhasteform')[0]),
		processData: false,
		contentType: false,
		type: 'POST',
		  success: function(data){
			  	insert();
			  	setTimeout(function(){ window.location.href = base_url+"Hastelist_controller"; }, 3500);
			  
			  }
		});
    },
    filter: function() {
        return $(this).is(":visible");
    }
});
$("#addaccountform").find("input,textarea,select").jqBootstrapValidation(
{
    preventSubmit: true,
    submitError: function($form, event, errors) {
        // Here I do nothing, but you could do something like display 
        // the error messages to the user, log, etc.
    },
    submitSuccess: function($form, event) {
       
        event.preventDefault();

        $.ajax({
	 	url: base_url+'Accounttype_controller/saveaccount',
		data: new FormData($('#addaccountform')[0]),
		processData: false,
		contentType: false,
		type: 'POST',
		  success: function(data){
			  	insert();
			  	setTimeout(function(){ window.location.href = base_url+"Accounttype_controller"; }, 3500);
			  
			  }
		});
    },
    filter: function() {
        return $(this).is(":visible");
    }
});

$("#editaccountform").find("input,textarea,select").jqBootstrapValidation(
{
    preventSubmit: true,
    submitError: function($form, event, errors) {
        // Here I do nothing, but you could do something like display 
        // the error messages to the user, log, etc.
    },
    submitSuccess: function($form, event) {
       
        event.preventDefault();

        $.ajax({
	 	url: base_url+'Accounttype_controller/saveaccount',
		data: new FormData($('#editaccountform')[0]),
		processData: false,
		contentType: false,
		type: 'POST',
		  success: function(data){
			  	update();
			  	setTimeout(function(){ window.location.href = base_url+"Accounttype_controller"; }, 3500);
			  
			  }
		});
    },
    filter: function() {
        return $(this).is(":visible");
    }
});

$("#edithasteform").find("input,textarea,select").jqBootstrapValidation(
{
    preventSubmit: true,
    submitError: function($form, event, errors) {
        // Here I do nothing, but you could do something like display 
        // the error messages to the user, log, etc.
    },
    submitSuccess: function($form, event) {
       
        event.preventDefault();

        $.ajax({
	 	url: base_url+'Hastelist_controller/savehaste',
		data: new FormData($('#edithasteform')[0]),
		processData: false,
		contentType: false,
		type: 'POST',
		  success: function(data){
			  	update();
			  	setTimeout(function(){ window.location.href = base_url+"Hastelist_controller"; }, 3500);
			  
			  }
		});
    },
    filter: function() {
        return $(this).is(":visible");
    }
});

$("#editpackageform").find("input,textarea,select").jqBootstrapValidation(
{

    preventSubmit: true,
    submitError: function($form, event, errors) {
        // Here I do nothing, but you could do something like display 
        // the error messages to the user, log, etc.
    },
    submitSuccess: function($form, event) {
       
        event.preventDefault();

            $.ajax({
		 	url: base_url+'Packagestyle_controller/savepackage',
			data: new FormData($('#editpackageform')[0]),
			processData: false,
			contentType: false,
			type: 'POST',
			  success: function(data){
				  	 insert();
				  	 setTimeout(function(){ window.location.href = base_url+"Packagestyle_controller"; }, 3500);
				  
				  }
			});
    },
    filter: function() {
        return $(this).is(":visible");
    }
});

});

function deletetostcl()
{
	$.toast({
	heading: 'Delete'
	, text: 'your data delete successfully.'
	, position: 'top-right'
	, loaderBg: '#ca2527'
	, icon: 'error'
	, hideAfter: 3500
	, stack: 6
	});
}

function deletedata(id,type)
{
	if(confirm("Are you sure want to delete this record?"))
	{
		$.ajax({
			type: "POST",
			url: base_url+"Home_controller/deletedata",
			data: {id:id,type:type},
			success: function(data)
			{
				if(data == 1 && type == "companydelete")
				{
					deletetostcl();
					// $('#example23').DataTable().ajax.reload();
					setTimeout(function(){ window.location.href = base_url+"Company_controller"; }, 3500);
				}
				else if(data == 1 && type == "screendelete")
				{
					deletetostcl();
					// $('#example23').DataTable().ajax.reload();
					setTimeout(function(){ window.location.href = base_url+"Screenregister_controller"; }, 3500);
				}
				else if(data == 1 && type == "categorydelete")
				{
					deletetostcl();
					// $('#example23').DataTable().ajax.reload();
					setTimeout(function(){ window.location.href = base_url+"Category_controller"; }, 3500);
				}
				else if(data == 1 && type == "itemtypedelete")
				{
					deletetostcl();
					// $('#example23').DataTable().ajax.reload();
					setTimeout(function(){ window.location.href = base_url+"Itemtype_controller"; }, 3500);
				}
				else if(data == 1 && type == "packagedelete")
				{
					deletetostcl();
					// $('#example23').DataTable().ajax.reload();
					setTimeout(function(){ window.location.href = base_url+"Packagestyle_controller"; }, 3500);
				}
				else if(data == 1 && type == "hastedelete")
				{
					deletetostcl();
					// $('#example23').DataTable().ajax.reload();
					setTimeout(function(){ window.location.href = base_url+"Hastelist_controller"; }, 3500);
				}
			}
		
		});
	}
}

function transfortcon(val)
{
	var transportid = val.value;
	$.ajax({
		url: base_url+'Hastelist_controller/transportaddress',
		data: {transportid:transportid},
		type: 'POST',
		success: function(data)
	  	{
		 	$("#station").val(data);
		}
	});
}
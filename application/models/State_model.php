<?php

class State_model extends CI_Model
{
   function __construct()
	{
		parent::__construct();
	}
	public function displayState()
	{
		$this->db->select('*');
		$this->db->from('state');
		$this->db->order_by("stateName", "asc");
		$query = $this->db->get();
		return $query->result();
	}
	public function addstate()
	{
		$statedate = date('Y-m-d',strtotime('NOW'));
		$data = array(
			'stateName'=>$this->input->post('stateName'),
			'isActive'=>$this->input->post('isActive'),
			'createDate'=>$statedate
			);
		$query = $this->db->insert('state', $data);
	}
	function singledelete($table,$stateID)
	{
		$data=array('stateID'=>$stateID);
		$result=$this->db->delete($table, $data);
	}
	function multipleDelete()
	{
		if($this->input->post('checkUncheck'))
		{
			foreach($this->input->post('checkUncheck') as $stateID)
			{
				$data=array('stateID'=>$stateID);
				$result=$this->db->delete('state', $data);
			}
		}
	}

	public function getsingle_state()
	{
		$stateID=$this->input->post('stateID');

		$this->db->where('stateID',$stateID);
		$query=$this->db->get('state');
		$result['data']=$query->result();
		//print_r($result);exit;
		return $result;
	}

	function singleStatus($stateID)
	{
		$this->db->where('stateID',$stateID);
		$query=$this->db->get('state');
		$result=$query->result();
		$isActive = $result[0]->isActive;
		
		if($isActive == 1)
		{
			$isActive=0;
		}
		else
		{
			$isActive=1;
		}
		echo $isActive;
		$data = array('isActive'=>$isActive);
		$result=$this->db->update('state', $data, array('stateID'=>$stateID));
	}



	


}
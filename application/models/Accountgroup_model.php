<?php

class Accountgroup_model extends CI_Model
{
   function __construct()
	{
		parent::__construct();
		
	}
	public function getselect_City()
	{
		$this->db->select('*');
		$this->db->where('isActive',1);
		$this->db->order_by("cityName", "asc");
		$query = $this->db->get('city');
		$result['data']=$query->result();
		return $result;
	}
	public function getselect_state()
	{
		$this->db->select('*');
		$this->db->where('isActive',1);
		$this->db->order_by("stateName", "asc");
		$query = $this->db->get('state');
		$result['state']=$query->result();
		return $result;
	}
	public function getselect_transport()
	{
		$this->db->select('*');
		$this->db->where('isActive',1);
		$this->db->order_by("transportName", "asc");
		$query = $this->db->get('transport');
		$result['transport']=$query->result();
		return $result;
	}
}
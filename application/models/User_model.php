<?php
class User_model extends CI_Model {
	function __construct()
    {
        parent::__construct();
    }

	public function adduser()
	{
		$str = $this->input->post('UserKey');
		$encode = md5($str);
	date_default_timezone_set("Asia/Kolkata");
		$userdate = date('Y-m-d',strtotime('NOW'));
		$data = array(
			'UserName'=>$this->input->post('UserName'),
			'Password'=>$this->input->post('Password'),
			'UserLevel'=>$this->input->post('UserLevel'),
			'UserKey'=>$this->input->post('UserKey'),
			'AdvanceToolbar'=>$this->input->post('AdvanceToolbar'),
			'HideCustom'=>$this->input->post('HideCustom'),
			'OnCreateDate'=>$userdate,
			'userkey_encode'=>$encode,
			'isActive'=>$this->input->post('isActive')
			);
		$query = $this->db->insert('user', $data);
	}
	
	public function displayUser()
	{
		$this->db->select('*');
		$this->db->from('user');
		$this->db->order_by("UserId", "desc");
		$query = $this->db->get();
		return $query->result();
	}
	function singledelete($table,$UserId)
	{
		$data=array('UserId'=>$UserId);
		$result=$this->db->delete($table, $data);
	}
	
	function multipleDelete()
	{
		if($this->input->post('checkUncheck'))
		{
			foreach($this->input->post('checkUncheck') as $UserId)
			{
				$data=array('UserId'=>$UserId);
				$result=$this->db->delete('user', $data);
			}
		}
	}
	
	function singleStatus($UserId)
	{
		$this->db->where('UserId',$UserId);
		$query=$this->db->get('user');
		$result=$query->result();
		$isActive = $result[0]->isActive;
		//print_r($isActive);exit;
		if($isActive == 1)
		{
			$isActive=0;
		}
		else
		{
			$isActive=1;
		}
		echo $isActive;
		$data = array('isActive'=>$isActive);
		$result=$this->db->update('user', $data, array('UserId'=>$UserId));
	}
	
	
	
}
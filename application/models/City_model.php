<?php

class City_model extends CI_Model
{
   function __construct()
	{
		parent::__construct();
	}

	public function addstate()
	{
		exit;
		$statedate = date('Y-m-d',strtotime('NOW'));
		$data = array(
			'stateName'=>$this->input->post('stateName'),
			'isActive'=>$this->input->post('isActive'),
			'createDate'=>$statedate
			);
		$query = $this->db->insert('state', $data);
	}
	
	public function selectstate()
	{
		$this->db->select('*');
		$this->db->from('state');
		$this->db->where('isActive',1);
		$this->db->order_by("stateName", "asc");
		$query = $this->db->get();
		return $query->result();
	}
	public function addcity()
	{
		$citydate = date('Y-m-d',strtotime('NOW'));
		$data = array(
			'cityName'=>$this->input->post('cityName'),
			'stateID'=>$this->input->post('stateID'),
			'isActive'=>$this->input->post('isActive'),
			'createDate'=>$citydate
			);
		$query = $this->db->insert('city', $data);
	}
	
	public function displayCity()
	{
		$this->db->select('*');
		$this->db->from('city');
		 $this->db->join('state', 'state.stateID =  city.stateID');
		$this->db->order_by("city.stateID", "asc");
		$query = $this->db->get();
		return $query->result();
	}
	
	function singledelete($table,$cityID)
	{
		$data=array('cityID'=>$cityID);
		$result=$this->db->delete($table, $data);
	}
	
	function multipleDelete()
	{
		if($this->input->post('checkUncheck'))
		{
			foreach($this->input->post('checkUncheck') as $cityID)
			{
				$data=array('cityID'=>$cityID);
				$result=$this->db->delete('city', $data);
			}
		}
	}
	
	function singleStatus($cityID)
	{
		$this->db->where('cityID',$cityID);
		$query=$this->db->get('city');
		$result=$query->result();
		$isActive = $result[0]->isActive;
		//print_r($isActive);exit;
		if($isActive == 1)
		{
			$isActive=0;
		}
		else
		{
			$isActive=1;
		}
		echo $isActive;
		$data = array('isActive'=>$isActive);
		$result=$this->db->update('city', $data, array('cityID'=>$cityID));
	}
	
}
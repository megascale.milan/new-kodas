<?php

class Accounttype_model extends CI_Model
{
   function __construct()
	{
		parent::__construct();
		
	}
	public function getallaccounttype()
	{
		$this->db->select('*');
		$query = $this->db->get('acc_type');
		$result['rows'] = $query->num_rows();
		$result['data']=$query->result();
		return $result;		
	}
	public function getsingle_Product()
	{
		$AccNo=$this->input->post('AccNo');
		$this->db->where('AccNo',$AccNo);
		$query=$this->db->get('acc_type');
		$result['data']=$query->result();
		return $result;
	}

	public function addeditacctype()
	{
		date_default_timezone_set("Asia/Kolkata");
		$createdate = date('Y-m-d H:i:s',strtotime('NOW'));
		$updatedate = date('Y-m-d H:i:s',strtotime('NOW'));
		$data = array(
			'AccType' => $this->input->post('AccType'),			
			'BalSheet' => $this->input->post('BalSheet'),
			'TRIALside' => $this->input->post('TRIALside'),
			'TrialPos' => $this->input->post('TrialPos'),
			'Creater' => 'Admin',
			'CreateTime' =>$createdate
		);

		$dataupdate = array(
			'AccType' => $this->input->post('AccType'),
			'BalSheet' => $this->input->post('BalSheet'),
			'TRIALside' => $this->input->post('TRIALside'),
			'TrialPos' => $this->input->post('TrialPos'),
			'Updater' => 'Admin',
			'UpdateTime' =>$updatedate
			);

		if ($this->input->post('AccNo') != NULL && $this->input->post('AccNo') > 0) 
		{
			$query = $this->db->update('acc_type', $dataupdate, array('AccNo'=>$this->input->post('AccNo')));
			$result=$this->input->post('AccNo');
		}
		else
		{
			$query = $this->db->insert('acc_type', $data);
		}
		return $result;
	}
	function singledelete($table,$AccNo)
	{
		$data=array('AccNo'=>$AccNo);
		$result=$this->db->delete($table, $data);
	}
	
	function multipleDelete()
	{
		if($this->input->post('checkUncheck'))
		{
			foreach($this->input->post('checkUncheck') as $AccNo)
			{
				$data=array('AccNo'=>$AccNo);
				//print_r($data);exit;
				$result=$this->db->delete('acc_type', $data);
			}
		}
	}
	
	
	
	
}
<?php

class Transport_model extends CI_Model
{
   function __construct()
	{
		parent::__construct();
		
	}
	public function getalltransport()
	{
		$this->db->select('*');
		$query = $this->db->get('transport');
		$result['rows'] = $query->num_rows();
		$result['data']=$query->result();
		return $result;		
	}
	public function getsingle_transport()
	{
		$transportID=$this->input->post('transportID');
		$this->db->where('transportID',$transportID);
		$query=$this->db->get('transport');
		$result['data']=$query->result();
		return $result;
	}
	
	public function addedittransport()
	{
		//print_r($this->input->post('transportID'));exit;
		date_default_timezone_set("Asia/Kolkata");
		$createdate = date('Y-m-d',strtotime('NOW'));
		$updatedate = date('Y-m-d',strtotime('NOW'));
		$data = array(
					'transportName' => $this->input->post('transportName'),
					'Taddress' => $this->input->post('Taddress'),
					'Tphone1' => $this->input->post('Tphone1'),
					'Tphone2' => $this->input->post('Tphone2'),
					'Tmobile' => $this->input->post('Tmobile'),
					'TEway' => $this->input->post('TEway'),
					'Tmode' => $this->input->post('Tmode'),
					'isActive' => $this->input->post('isActive'),
					'createDate' =>$createdate

				);

		$dataupdate = array(
			'transportName' => $this->input->post('transportName'),
			'Taddress' => $this->input->post('Taddress'),
			'Tphone1' => $this->input->post('Tphone1'),
			'Tphone2' => $this->input->post('Tphone2'),
			'Tmobile' => $this->input->post('Tmobile'),
			'TEway' => $this->input->post('TEway'),
			'Tmode' => $this->input->post('Tmode'),
			'isActive' => $this->input->post('isActive'),
			'updateDate' =>$updatedate
			);
		if ($this->input->post('transportID') != NULL && $this->input->post('transportID') > 0) 
		{
			
			$query = $this->db->update('transport', $dataupdate, array('transportID'=>$this->input->post('transportID')));
			$result=$this->input->post('transportID');
			//print_r($query);exit;
		}
		else
		{	
			
			$query = $this->db->insert('transport', $data);
		}
		return $result;
	}
	
	function singledelete($table,$transportID)
	{
		$data=array('transportID'=>$transportID);
		$result=$this->db->delete($table, $data);
	}
	function multipleDelete()
	{
		if($this->input->post('checkUncheck'))
		{
			foreach($this->input->post('checkUncheck') as $transportID)
			{
				$data=array('transportID'=>$transportID);
				$result=$this->db->delete('transport', $data);
			}
		}
	}
	function singleStatus($transportID)
	{
		$this->db->where('transportID',$transportID);
		$query=$this->db->get('transport');
		$result=$query->result();
		$isActive = $result[0]->isActive;
		//print_r($isActive);exit;
		if($isActive == 1)
		{
			$isActive=0;
		}
		else
		{
			$isActive=1;
		}
		echo $isActive;
		$data = array('isActive'=>$isActive);
		$result=$this->db->update('transport', $data, array('transportID'=>$transportID));
	}
	
	
	
}
<?php
class Home_model extends CI_Model {
	function __construct()
    {
        parent::__construct();
    }

    public function deletedata($table,$where)
	{
		return $this->db->delete($table,$where); 
	}

	public function select($table)
	{
		$this->db->select('*');
		$this->db->from($table);
		$query = $this->db->get();
		return $query->result();
	}
	public function select_where_row($table,$where)
	{
		$this->db->select('*');
		$this->db->from($table);
		$this->db->where($where);
		$query = $this->db->get();
		return $query->row_array();
	}

	public function insert($table,$data)
	{
		return $this->db->insert($table,$data);
	}

	public function update($table,$data,$where)
	{
		$this->db->where($where);
		return $this->db->update($table,$data);
	}

	public function select_where_haste()
	{
		$this->db->select('*');
		$this->db->from('haste');
		$this->db->join('transport', 'transport.transportID = haste.Transport');
		$this->db->join('screenregister_entry', 'screenregister_entry.ScreenRegisterID = haste.ScreenSeries');
		$query = $this->db->get();
		return $query->row_array();
	}

	public function selecthastelistdata()
	{
		$this->db->select('*');
		$this->db->from('haste');
		$this->db->join('transport', 'transport.transportID = haste.Transport','right');
		$this->db->join('screenregister_entry', 'screenregister_entry.ScreenRegisterID = haste.ScreenSeries','right');
		$this->db->join('ledger', 'ledger.Ledger_Id = haste.AdatiyaName','right');
		$query = $this->db->get();
		return $query->result();
	}
	
}
<?php 
$UserId=$this->input->post('UserId')?$this->input->post('UserId'):'';
?>
<?php $this->load->view('common/header'); ?>
<?php $this->load->view('user/user-js');?>
<div class="page-wrapper">
    <div class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h4 class="text-themecolor">User Master</h4>
            </div>
            <div class="col-md-7 align-self-center text-right">
                <div class="d-flex justify-content-end align-items-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
						<li class="breadcrumb-item"><a href="javascript:void(0)">Master</a></li>
						<li class="breadcrumb-item active">User Master</li>

                    </ol>
                    <button type="button" class="btn btn-info d-none d-lg-block m-l-15" data-toggle="modal" data-target="#exampleModal" data-whatever="@getbootstrap"><i class="fa fa-plus-circle"></i> Create New</button>
					<a href="javascript:fun_multipleDelete();" class="btn btn-info d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i> All Delete</a>	
				   <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title" id="exampleModalLabel1">User Master</h4>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                </div>
                                <div class="modal-body">
                                     <form name="frm_user_add" id="frm_user_add"  action="<?php echo base_url();?>User_controller/saveuser" method="post" class=" error" novalidate>
                                      <div class="form-group">
                                            <h5 style="text-align: left;">Enter User Name <span class="text-danger">*</span></h5>
                                            <div class="controls" style="text-align: left;">
                                                <input type="text" name="UserName" class="form-control txtName" id="txtName" required data-validation-required-message="This field is required"> 
                                            </div>
                                        </div>
										<div class="form-group">
                                            <h5 style="text-align: left;">Enter Password <span class="text-danger">*</span></h5>
                                            <div class="controls" style="text-align: left;">
                                                <input type="password" name="Password" class="form-control" required data-validation-required-message="This field is required"> 
                                            </div>
                                        </div>
										<div class="form-group">
                                            <h5 style="text-align: left;">Enter User Level <span class="text-danger">*</span></h5>
                                            <div class="controls" style="text-align: left;">
                                                <select name="UserLevel"  required class="form-control">
													<option value="">Select User Level </option>
													<option value="OPERATOR">OPERATOR</option>
													<option value="ALL DATA ENTRY">ALL DATA ENTRY</option>
													<option value="SUPERVISOR">SUPERVISOR</option>
												</select>
										   </div>
                                        </div>
										<div class="form-group">
                                            <h5 style="text-align: left;">Enter UserKey <span class="text-danger">*</span></h5>
                                            <div class="controls" style="text-align: left;">
                                                <input type="text" name="UserKey" class="form-control" required data-validation-required-message="This field is required"> 
                                            </div>
                                        </div>
										
										<div class="row">
											<div class="col-md-6">
												<div class="form-group" style="float: left;">
													  <input type="checkbox" value="1" checked  name="AdvanceToolbar" > Advance Toolbar
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group" style="float: left;">
													  <input type="checkbox" value="1" checked  name="HideCustom" > Hide Custom
												</div>
											</div>
										</div>
										

                                        <div class="form-group" style="float: left;">
                                              <input type="checkbox" value="1"  name="isActive" > isActive?
                                        </div>
                                        <div style="clear:both"></div>
                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-info">Submit</button>
                                        </div>
                                    </form>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
       <form  name="frm_user_list" method="post" action="<?php echo base_url();?>User_controller/index">
            <input type="hidden" value="<?php echo $UserId; ?>" name="UserId" />
            <input type="hidden" name="method" value="" />
         <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <!-- <h4 class="card-title">Data Export</h4>
                                <h6 class="card-subtitle">Export data to Copy, CSV, Excel, PDF & Print</h6> -->
                                <div class="table-responsive m-t-40">
                                    <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th><input type="checkbox" name="checkall" id= "checkall" value="" /> Checkbox</th>
                                                <th>UserName</th>
                                                <th>UserLevel</th>
                                                <th>Advance Toolbar</th>
                                                <th>Hide Custom</th>
                                                <th>OnCreateDate</th>
                                                <th>Is Active</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        
                                        <tbody>
                                            <?php 
                                            foreach($displayUser as $displayUserdata)
                                            {
                                            ?>
                                            <tr id="ID_<?php echo $displayUserdata->UserId; ?>">
                                                <td><input class="checkbox" name="checkUncheck[]"  id="checkAllAuto" value="<?php echo $displayUserdata->UserId; ?>" type="checkbox" /></td>
                                                <td><?php echo $displayUserdata->UserName; ?></td>
                                                <td><?php echo $displayUserdata->UserLevel; ?></td>
                                                <td><?php echo $displayUserdata->AdvanceToolbar; ?></td>
                                                <td><?php echo $displayUserdata->HideCustom; ?></td>
                                                <td><?php echo $displayUserdata->OnCreateDate; ?></td>
                                                <td>
                                                   <a href="javascript:fun_single_status(<?php echo $displayUserdata->UserId; ?>);">
														<span id="user_<?php echo $displayUserdata->UserId; ?>">
															<?php if($displayUserdata->isActive==1){ echo"Active"; }else{ echo"Inactive";} ?>
														</span>
													</a>
                                                </td>
                                                <td class="editdelaction">
                                                    <a href="javascript:fun_edit(<?php echo 
                                                    $displayUserdata->UserId; ?>);" " ><i class="fa fa-edit"></i></a>&nbsp;&nbsp;
                                                    
                                                     <a href="javascript:fun_single_delete(<?php echo $displayUserdata->UserId; ?>);"><i class="fa fa-trash-o"></i></a>
                                                </td>
                                            </tr>
                                            <?php 
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        

                    </div>
                </div>
               </form>
    </div>
</div>
 <?php $this->load->view('common/footer'); ?>   
 <script>
  $( document ).ready(function() {
	$( ".txtName" ).keypress(function(e) {
		var key = e.keyCode;
		if (key >= 48 && key <= 57) {
			e.preventDefault();
		}
	});
});
 </script>
 <script src="<?php echo base_url(); ?>assets/node_modules/jquery/jquery-3.2.1.min.js"></script>
<script type="text/javascript">
function fun_single_delete(UserId)
{
	if(confirm("Are you sure want to delete this record?"))
	{
		$.ajax({
			type: "POST",
			url: "<?php echo base_url() ?>User_controller/singledelete",
			data: "UserId=" + UserId,
			success: function(total){
			$("#ID_"+UserId).html(total);
			}
		
		});
	}
}
/******************* CHECK ALL CHECKBOX *****************/
jQuery(document).ready(function($)
{
	$('#checkall').click(function()
	{
		$(':checkbox').each(function()
		{
			if(this.checked)
			{
				this.checked = false;
			}
			else
			{
				this.checked = true;
			}
		});
		return false;
	}); 
});


/****************** MULTIPLE DELETE *************/
	function fun_multipleDelete()
	{
	  var count = $(":checkbox:checked").length;

	  if(count > 0)
	  {
		  var status = confirm("Are you sure want to delete this record?");
		  if(status==true)
		  {
		  	
			  frmobj=window.document.frm_user_list;
			  frmobj.method.value="multipleDelete";
			  document.frm_user_list.action="<?php echo base_url();?>User_controller/index";
			  frmobj.submit();
		  }
	  }
	  else
	  {
		alert('Please select atleast one record.');
	  }
	}
	
/****************** fun single Status *************/
function fun_single_status(UserId)
{
	//alert(cityID);
	$.ajax({
		type: "POST",
		url: "<?php echo base_url(); ?>User_controller/singleStatus",
		data: "UserId=" + UserId,
		success: function(result){
		//alert(result);
			if(result == 0)
			{
				$('#user_'+UserId).html("Inactive");
			}
			if(result == 1)
			{
				$('#user_'+UserId).html("Active");
			}
		}
	});
}	

</script>
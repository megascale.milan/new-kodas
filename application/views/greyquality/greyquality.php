
<?php $this->load->view('common/header'); ?>
<style>
    #success_message{ display: none;}
	.form-group{
	    margin-bottom: 10px!important;
	}
	.form-control
	{
		    min-height: 30px!important;
	}
	
	/* change css form design */	
	html body .p-20 {
		padding: 20px 0!important;
	}
	select.form-control:not([size]):not([multiple]) {
		height: calc(1.0625rem + 2px);
	}
</style>

<div class="page-wrapper">
    <div class="container-fluid">
	
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h4 class="text-themecolor">Grey Quality Master</h4>
			</div>
            <div class="col-md-7 align-self-center text-right">
                <div class="d-flex justify-content-end align-items-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Master</a></li>
                        <li class="breadcrumb-item active">Grey Quality Master</li>
                    </ol>
					<a href="javascript:fun_multipleDelete();" class="btn btn-info d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i> All Delete</a>
                </div>
            </div>
        </div>
		
		
		<div class="row">
			<div class="card" style="width: 100%;">
				<div class="card-body p-b-0">
				
					<!-- Nav tabs -->
					<ul class="nav nav-tabs customtab2" role="tablist">
						<li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#home7" role="tab"><span class="hidden-sm-up"><i class="ti-home"></i></span> <span class="hidden-xs-down">Grey Quality List</span></a> </li>
						<li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#profile7" role="tab"><span class="hidden-sm-up"><i class="ti-user"></i></span> <span class="hidden-xs-down">Add Form</span></a> </li>
						<?php
						if(!empty($editgreyqualitydata))
						{
						?>
							<li class="nav-item"> <a class="nav-link foractive" data-toggle="tab" href="#editform" role="tab"><span class="hidden-sm-up"><i class="ti-user"></i></span> <span class="hidden-xs-down">Edit Form</span></a> </li>
						<?php
						}
						?>
					</ul>
					<!-- Tab panes -->
					<div class="tab-content">
						<div class="tab-pane active" id="home7" role="tabpanel">
							<div class="p-20">
							<form  name="frm_broker_list" method="post" action="<?php echo base_url();?>Broker_controller/index">
							<input type="hidden" value="" name="brokerID" />
							 <input type="hidden" name="method" value="" /> 
								<div class="table-responsive">
                                    <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th><input type="checkbox" name="checkall" id= "checkall" value=""  /> Checkbox</th>
                                                <th> Category</th>
                                                <th>Is Active</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                        	<?php
                                        	foreach ($greyqualitydata as $value)
                                        	{
                                        		?>
                                        		<tr id="">
                                                <td><input class="checkbox" name="checkUncheck[]"  id="checkAllAuto" value="" type="checkbox" /></td>
                                                <td><?=$value->Grey_Quality?></td>
                                               <td>
												<a href="javascript:fun_single_status();">
													<span id="">
														<?php if($value->IsActive == 0){echo "inactive";}else{echo "active";}?>
													</span>
												</a>
                                                </td>
                                                <td class="editdelaction">
                                                    <a href="<?=base_url();?>Category_controller/?qreyqualityid=<?=$value->Grey_Quality_Id?>"><i class="fa fa-edit"></i></a>&nbsp;&nbsp;
                                                    
                                                    <a href="javascript:deletedata('<?=$value->Grey_Quality_Id?>','categorydelete');"><i class="fa fa-trash-o"></i></a>
                                                </td>
                                            	</tr>
                                        		<?php
                                        	}
                                        	?>
                                            
                                           
                                        </tbody>
                                    </table>
                                </div>
								</form>
								</div>
						</div>
						<div class="tab-pane  p-20" id="profile7" role="tabpanel">
							<form  method="post" name="addgreyqutform" id="addgreyqutform" novalidate>
								<?php
								if(empty($editgreyqualitydata))
								{
									?>
										<input type="hidden" value="" id="qreyqualityid" name="qreyqualityid">
										<?php
									}
								?>
								<div class="row">
									<div class="form-group">
										<h5>Grey Quality <span class="text-danger">*</span></h5>
										<div class="controls">
											<input type="text" name="greyquality" id="greyquality" placeholder="Enter Grey Quality" class="form-control"  required data-validation-required-message="This field is required" value=""> 
										</div>
									</div>
									</div>
									<div class="row">
										 <div class="form-group" >
											  <input type="checkbox" value="1" name="isActive" > isActive?
										</div>
									</div>									
								</div>
								<div class="row">
									<div class="text-xs-right">
										<button type="submit" class="btn btn-success">Submit</button>
										<a  href="<?php echo base_url()?>Grey_Quality" class="btn btn-info">
                                            Cancel
                                        </a>
									</div>

								</div>
							</form>								
						</div>
						<?php
						if(!empty($editgreyqualitydata))
						{
						?>

						<div class="tab-pane  p-20" id="editform" role="tabpanel">

							<form class="m-t-40" method="post" name="editcategoryform" id="editcategoryform" novalidate>
								<input type="hidden" value="<?=$editgreyqualitydata['CategoryID']?>" id="qreyqualityid" name="qreyqualityid">

								<div class="row">
									<div class="col-md-6">
										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<h5>Grey Quality <span class="text-danger">*</span></h5>
													<div class="controls">
														<input type="text" name="greyquality" id="greyquality" placeholder="Enter Grey Quality" class="form-control"  required data-validation-required-message="This field is required" value="<?=$editgreyqualitydata['Grey_Quality'];?>"> 
													</div>
												</div>
											</div>
											<div class="col-md-6">
												 <div class="form-group" >
													  <input <?php if($editgreyqualitydata['IsActive'] == 1){echo "selected";}?> type="checkbox" value="1" name="isActive" > isActive?
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-6"></div>
									
								</div>
								<div class="row">
									<div class="text-xs-right">
										<button type="submit" class="btn btn-success">Submit</button>
										<a  href="<?php echo base_url()?>Grey_Quality" class="btn btn-info">
                                            Cancel
                                        </a>
									</div>
								</div>
							</form>												


						</div>
						<?php
						}
						?>
					</div>
				</div>
			</div>
		</div>
</div>
</div>
 <?php $this->load->view('common/footer'); ?>
<script type="text/javascript">
var inside = $("#qreyqualityid").val();

if(inside != "")
{
	$("#home7").removeClass('active');
	$(".nav-link").removeClass('active');
	$(".foractive").addClass('active');
	$("#editform").addClass('active');
}
</script>

<?php 
$gNo=$this->input->post('gNo')?$this->input->post('gNo'):'0';
?>
<?php $this->load->view('common/header'); ?>
<?php $this->load->view('market/market-js');?>
<style>
    #success_message{ display: none;}
	.form-group{
	    margin-bottom: 10px!important;
	}
	.form-control
	{
		    min-height: 30px!important;
	}
	
	/* change css form design */	
	html body .p-20 {
		padding: 20px 0!important;
	}
	select.form-control:not([size]):not([multiple]) {
		height: calc(1.0625rem + 2px);
	}
</style>

<div class="page-wrapper">
    <div class="container-fluid">
	
	
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h4 class="text-themecolor">Account Group Manager</h4>
			</div>
            <div class="col-md-7 align-self-center text-right">
                <div class="d-flex justify-content-end align-items-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Master</a></li>
                        <li class="breadcrumb-item active">Account Group Manager</li>
                    </ol>
					<a href="javascript:fun_multipleDelete();" class="btn btn-info d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i> All Delete</a>
                </div>
            </div>
        </div>
		
		
		<div class="row">
			<div class="card" style="width: 100%;">
				<div class="card-body p-b-0">
				
					<!-- Nav tabs -->
					<ul class="nav nav-tabs customtab2" role="tablist">
						<li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#home7" role="tab"><span class="hidden-sm-up"><i class="ti-home"></i></span> <span class="hidden-xs-down">Account Group List</span></a> </li>
						<li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#profile7" role="tab"><span class="hidden-sm-up"><i class="ti-user"></i></span> <span class="hidden-xs-down">Add Form</span></a> </li>
					</ul>
					<!-- Tab panes -->
					<div class="tab-content">
						<div class="tab-pane active" id="home7" role="tabpanel">
							<div class="p-20">
							<form  name="frm_broker_list" method="post" action="<?php echo base_url();?>Broker_controller/index">
							<input type="hidden" value="" name="brokerID" />
							 <input type="hidden" name="method" value="" /> 
								<div class="table-responsive">
                                    <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th><input type="checkbox" name="checkall" id= "checkall" value=""  /> Checkbox</th>
                                                <th> Name</th>
                                                <th>Address</th>
                                                <th>Contact No.</th>
                                                <th>CreateDate</th>
                                                <th>Is Active</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                           
                                            <tr id="">
                                                <td><input class="checkbox" name="checkUncheck[]"  id="checkAllAuto" value="" type="checkbox" /></td>
                                                <td>DIVYESH</td>
                                                <td>surat</td>
                                                <td>8080114457</td>
                                                <td>2018-12-08</td>
                                               <td>
												<a href="javascript:fun_single_status();">
													<span id="">
														<?php echo"Active"; ?>
													</span>
												</a>
                                                </td>
                                                <td class="editdelaction">
                                                    <a href="javascript:fun_edit();" ><i class="fa fa-edit"></i></a>&nbsp;&nbsp;
                                                    
                                                    <a href="javascript:fun_single_delete();"><i class="fa fa-trash-o"></i></a>
                                                </td>
                                            </tr>
                                           
                                        </tbody>
                                    </table>
                                </div>
								</form>
								</div>
						</div>
						<div class="tab-pane  p-20" id="profile7" role="tabpanel">
							
								<form action="<?php echo base_url() ?>Accountgroup_controller/saveuser" class="" method="post" name="addform" novalidate>
									<div class="row common_master_form_div">
										<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 ">
											<div class="formtitle">
												<div class="row removemargin">
													<div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
														<div class="row" style="margin-top: 2px;margin-bottom: -3px;">
															<div class="col-12 col-sm-4 col-md-2 col-lg-2 col-xl-2 d-flex ">
																<div class="form-group">
																	<label>FIND NAME <span class="fored"><b>*</b></span> :</label>
																</div>
															</div>
															<div class="col-12 col-sm-8 col-md-10 col-lg-10 col-xl-10">
																<div class="form-group">
																	<div class="controls">
																		<select name="" id="" required class="form-control">
																			<option value=""> -- Select -- </option>
																			<option value="1"> 1 </option>
																		</select>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
													</div>
												</div>
											</div>
											<div class="formtitle">
												<h4 class="backcolor">Account Information</h4>
												<div class="row removemargin">
													<div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
														<div class="row">
															<div class="col-12 col-sm-4 col-md-2 col-lg-2 col-xl-2 d-flex ">
																<div class="form-group">
																	<label>CODE <span class="fored"><b>*</b></span> :</label>
																</div>
															</div>
															<div class="col-12 col-sm-8 col-md-10 col-lg-10 col-xl-10">
																<div class="form-group ">
																	<div class="controls">
																		<input type="text" name="" class="form-control" placeholder="ENTER CODE" required data-validation-required-message="This field is required"> 
																		<input type="hidden" name="gNo" >
																	</div>
																</div>
															</div>
															
															<div class="col-12 col-sm-4 col-md-2 col-lg-2 col-xl-2 d-flex ">
																<div class="form-group">
																	<label>A/C TYPE  <span class="fored"><b>*</b></span> :</label>
																</div>
															</div>
															<div class="col-12 col-sm-8 col-md-10 col-lg-10 col-xl-10">
																<div class="form-group field">
																	<div class="controls">
																		<select name="" id="" required class="form-control">
																			<option value=""> --Select -- </option>
																			<option value="1"> 1 </option>
																		</select>
																	</div>
																</div>
															</div>
															
															<div class="col-12 col-sm-4 col-md-2 col-lg-2 col-xl-2 d-flex ">
																<div class="form-group">
																	<label>SHORT NAME <span class="fored"><b>*</b></span> :</label>
																</div>
															</div>
															<div class="col-12 col-sm-8 col-md-3 col-lg-3 col-xl-3">
																<div class="form-group field">
																	<div class="controls">
																		<input type="text" name="" class="form-control" placeholder="SHORT NAME" required data-validation-required-message="This field is required"> 
																	</div>
																</div>
															</div>
															
															<div class="col-12 col-sm-4 col-md-1 col-lg-1 col-xl-1 d-flex ">
																<div class="form-group">
																	<label> NAME <span class="fored"><b>*</b></span> :</label>
																</div>
															</div>
															<div class="col-12 col-sm-8 col-md-6 col-lg-6 col-xl-6">
																<div class="form-group field">
																	<div class="controls">
																		<input type="text" name="" class="form-control txtName" id="txtName" placeholder="ENTER NAME"required data-validation-required-message="This field is required"> 
																	</div>
																</div>
															</div>
															 
															<div class="col-12 col-sm-4 col-md-2 col-lg-2 col-xl-2 d-flex">
																<div class="form-group">
																	<label> DHARA <span class="fored"><b>*</b></span> :</label>
																</div>
															</div>
															<div class="col-12 col-sm-8 col-md-10 col-lg-10 col-xl-10">
																<div class="form-group field">
																	<div class="controls">
																		<input type="text"  name="" placeholder="0.00" class="form-control" required data-validation-required-message="This field is required"> 
																	</div>
																</div>
															</div>
															
															
															<div class="col-12 col-sm-4 col-md-2 col-lg-2 col-xl-2 d-flex ">
																<div class="form-group">
																	<label>SELECTED <span class="fored"><b>*</b></span> :</label>
																</div>
															</div>
															<div class="col-12 col-sm-8 col-md-10 col-lg-10 col-xl-10">
																<div class="form-group field">
																	<div class="controls">
																		<input type="checkbox" value="1"  name="isActive" > Selected
																	</div>
																</div>
															</div>
															
															
															
														</div>
													</div>
													<div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
														<div class="row">
															
															
															<div class="col-12 col-sm-4 col-md-2 col-lg-2 col-xl-2 d-flex ">
																<div class="form-group">
																	<label> COMM.% <span class="fored"><b>*</b></span> :</label>
																</div>
															</div>
															<div class="col-12 col-sm-8 col-md-10 col-lg-10 col-xl-10">
																<div class="form-group field">
																	<div class="controls">
																		<input type="text" name="" class="form-control" placeholder="0.00"  required data-validation-required-message="This field is required"> 
																	</div>
																</div>
															</div>
															
															<div class="col-12 col-sm-4 col-md-2 col-lg-2 col-xl-2 d-flex ">
																<div class="form-group">
																	<label> DR.INT.@ <span class="fored"><b>*</b></span> :</label>
																</div>
															</div>
															<div class="col-12 col-sm-8 col-md-10 col-lg-10 col-xl-10">
																<div class="form-group field">
																	<div class="controls">
																		<input type="text" name="" class="form-control" placeholder="0.00"  required data-validation-required-message="This field is required"> 
																	</div>
																</div>
															</div>
															
															<div class="col-12 col-sm-4 col-md-2 col-lg-2 col-xl-2 d-flex">
																<div class="form-group">
																	<label>MARKET <span class="fored"><b>*</b></span> :</label>
																</div>
															</div>
															<div class="col-12 col-sm-8 col-md-10 col-lg-10 col-xl-10">
																<div class="form-group field">
																	<div class="controls">
																		<select name="" id="" required class="form-control">
																			<option value=""> --Select -- </option>
																			<option value="1"> 1 </option>
																		</select>
																	</div>
																</div>
															</div>
															
															<div class="col-12 col-sm-4 col-md-2 col-lg-2 col-xl-2 d-flex ">
																<div class="form-group">
																	<label>BROKER <span class="fored"><b>*</b></span> :</label>
																</div>
															</div>
															<div class="col-12 col-sm-8 col-md-10 col-lg-10 col-xl-10">
																<div class="form-group field">
																	<div class="controls">
																		<select name="" id="" required class="form-control">
																			<option value=""> --Select -- </option>
																			<option value="1"> 1 </option>
																		</select>
																	</div>
																</div>
															</div>
															<div class="col-12 col-sm-4 col-md-2 col-lg-2 col-xl-2 d-flex ">
																<div class="form-group">
																	<label>TRANSPORT <span class="fored"><b>*</b></span> :</label>
																</div>
															</div>
															<div class="col-12 col-sm-8 col-md-10 col-lg-10 col-xl-10">
																<div class="form-group field">
																	<div class="controls">
																		<select name="" id="" required class="form-control">
																			<option value=""> --Select -- </option>
																			<?php 
																			foreach($selectTransport as $selectTransportdata)
																			{
																			?>
																			<option value="<?php echo $selectTransportdata->transportID; ?>"> <?php echo $selectTransportdata->transportName; ?></option>
																			<?php 
																			}
																			?>
																		</select>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>	

											</div>
											
											<div class="formtitle">
												<h4 class="backcolor">Address Information</h4>
												<div class="row removemargin">
													<div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
														<div class="row">
															<div class="col-12 col-sm-4 col-md-3 col-lg-3 col-xl-3 d-flex ">
																<div class="form-group">
																	<label>ADDRESS<span class="fored"><b>*</b></span> :</label>
																</div>
															</div>
															<div class="col-12 col-sm-8 col-md-9 col-lg-9 col-xl-9">
																<div class="form-group field">
																	<div class="controls">
																		<input type="text" name="" class="form-control" placeholder="ENTER ADDRESS"  required data-validation-required-message="This field is required"> 
																	</div>
																</div>
															</div>
															<div class="col-12 col-sm-4 col-md-3 col-lg-3 col-xl-3 d-flex ">
																<div class="form-group">
																	<label>GODOWN/OTH ADD.<span class="fored"><b>*</b></span> :</label>
																</div>
															</div>
															<div class="col-12 col-sm-8 col-md-9 col-lg-9 col-xl-9">
																<div class="form-group field">
																	<div class="controls">
																		<input type="text" name="" class="form-control" placeholder="ENTER GODOWN/OTH ADDRESS"  required data-validation-required-message="This field is required"> 
																	</div>
																</div>
															</div>
															<div class="col-12 col-sm-4 col-md-3 col-lg-3 col-xl-3 d-flex ">
																<div class="form-group">
																	<label>ADD.(CONT.)<span class="fored"><b>*</b></span> :</label>
																</div>
															</div>
															<div class="col-12 col-sm-8 col-md-9 col-lg-9 col-xl-9">
																<div class="form-group field">
																	<div class="controls">
																		<input type="text" name="" class="form-control" placeholder="ENTER ADD.(CONT.)"  required data-validation-required-message="This field is required"> 
																	</div>
																</div>
															</div>
															<div class="col-12 col-sm-4 col-md-3 col-lg-3 col-xl-3 d-flex ">
																<div class="form-group">
																	<label>OTH. ADDRESS CONT.<span class="fored"><b>*</b></span> :</label>
																</div>
															</div>
															<div class="col-12 col-sm-8 col-md-9 col-lg-9 col-xl-9">
																<div class="form-group field">
																	<div class="controls">
																		<input type="text" name="" class="form-control" placeholder="ENTER OTH. ADDRESS CONT."  required data-validation-required-message="This field is required"> 
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
														<div class="row">
															<div class="col-12 col-sm-4 col-md-2 col-lg-2 col-xl-2 d-flex ">
																<div class="form-group">
																	<label>CITY<span class="fored"><b>*</b></span> :</label>
																</div>
															</div>
															<div class="col-12 col-sm-8 col-md-10 col-lg-10 col-xl-10">
																<div class="form-group field">
																	<div class="controls">
																		<select name="" id="" required class="form-control">
																			<option value=""> --Select -- </option>
																			<?php 
																			foreach($selectCity as $selectCitydata)
																			{
																			?>
																			<option value="<?php echo $selectCitydata->cityID; ?>"> <?php echo $selectCitydata->cityName; ?></option>
																			<?php 
																			}
																			?>
																		</select>
																	</div>
																</div>
															</div>
															<div class="col-12 col-sm-4 col-md-2 col-lg-2 col-xl-2 d-flex ">
																<div class="form-group">
																	<label>STATE<span class="fored"><b>*</b></span> :</label>
																</div>
															</div>
															<div class="col-12 col-sm-8 col-md-10 col-lg-10 col-xl-10">
																<div class="form-group field">
																	<div class="controls">
																		<select name="" id="" required class="form-control">
																			<option value=""> --Select -- </option>
																			<?php 
																			foreach($selectState as $selectStatedata)
																			{
																			?>
																			<option value="<?php echo $selectStatedata->stateID; ?>"> <?php echo $selectStatedata->stateName; ?></option>
																			<?php 
																			}
																			?>
																		</select>
																	</div>
																</div>
															</div>
															
															<div class="col-12 col-sm-4 col-md-2 col-lg-2 col-xl-2 d-flex">
																<div class="form-group">
																	<label>EMAIL<span class="fored"><b>*</b></span> :</label>
																</div>
															</div>
															<div class="col-12 col-sm-8 col-md-10 col-lg-10 col-xl-10">
																<div class="form-group field">
																	<div class="controls">
																		<input type="email" name="" class="form-control" placeholder="ENTER EMAIL"  required data-validation-required-message="This field is required"> 
																	</div>
																</div>
															</div>
															
															<div class="col-12 col-sm-4 col-md-2 col-lg-2 col-xl-2 d-flex ">
																<div class="form-group">
																	<label>PIN<span class="fored"><b>*</b></span> :</label>
																</div>
															</div>
															<div class="col-12 col-sm-8 col-md-4 col-lg-4 col-xl-4">
																<div class="form-group field">
																	<div class="controls">
																		<input type="text" name="" class="form-control" placeholder="ENTER PIN"  required data-validation-required-message="This field is required"> 
																	</div>
																</div>
															</div>
															
															<div class="col-12 col-sm-4 col-md-2 col-lg-2 col-xl-2 d-flex ">
																<div class="form-group">
																	<label>STDCODE<span class="fored"><b>*</b></span> :</label>
																</div>
															</div>
															<div class="col-12 col-sm-8 col-md-4 col-lg-4 col-xl-4">
																<div class="form-group field">
																	<div class="controls">
																		<input type="text" name="" class="form-control" placeholder="ENTER STDCODE"  required data-validation-required-message="This field is required"> 
																	</div>
																</div>
															</div>
															
															
														</div>
													</div>
												</div>
											</div>
																
												
												
											<div class="row">
												<div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 pdr5">
													<div class="formtitle">
														<h4 class="backcolor">Contact Information</h4>
														<div class="row removemargin">
															<div class="col-12 col-sm-4 col-md-2 col-lg-2 col-xl-2 d-flex ">
																<div class="form-group">
																	<label>NAME<span class="fored"><b>*</b></span> :</label>
																</div>
															</div>
															<div class="col-12 col-sm-8 col-md-10 col-lg-10 col-xl-10">
																<div class="form-group field">
																	<div class="controls">
																		<input type="text" name="" class="form-control txtName" id="txtName" placeholder="ENTER NAME"  required data-validation-required-message="This field is required"> 
																	</div>
																</div>
															</div>
															
															<div class="col-12 col-sm-4 col-md-2 col-lg-2 col-xl-2 d-flex ">
																<div class="form-group">
																	<label>PHONE 1<span class="fored"><b>*</b></span> :</label>
																</div>
															</div>
															<div class="col-12 col-sm-8 col-md-4 col-lg-4 col-xl-4">
																<div class="form-group field"  id="staticParent">
																	<div class="controls">
																		<input type="text" name="" id="child" maxlength="10" class="form-control" placeholder="ENTER PHONE 1"  required data-validation-required-message="This field is required"> 
																	</div>
																</div>
															</div>
															
															<div class="col-12 col-sm-4 col-md-2 col-lg-2 col-xl-2 d-flex ">
																<div class="form-group">
																	<label>PHONE 2<span class="fored"><b>*</b></span> :</label>
																</div>
															</div>
															<div class="col-12 col-sm-8 col-md-4 col-lg-4 col-xl-4">
																<div class="form-group field" id="staticParent1">
																	<div class="controls">
																		<input type="text" name="" id="child1" maxlength="10" class="form-control" placeholder="ENTER PHONE 2"  required data-validation-required-message="This field is required"> 
																	</div>
																</div>
															</div>
															
															<div class="col-12 col-sm-4 col-md-2 col-lg-2 col-xl-2 d-flex ">
																<div class="form-group">
																	<label>FAX<span class="fored"><b>*</b></span> :</label>
																</div>
															</div>
															<div class="col-12 col-sm-8 col-md-4 col-lg-4 col-xl-4">
																<div class="form-group field">
																	<div class="controls">
																		<input type="text" name="" maxlength="10" class="form-control" placeholder="ENTER FAX"  required data-validation-required-message="This field is required"> 
																	</div>
																</div>
															</div>
															
															<div class="col-12 col-sm-4 col-md-2 col-lg-2 col-xl-2 d-flex ">
																<div class="form-group">
																	<label>MOBILE<span class="fored"><b>*</b></span> :</label>
																</div>
															</div>
															<div class="col-12 col-sm-8 col-md-4 col-lg-4 col-xl-4">
																<div class="form-group field" id="staticParent2">
																	<div class="controls">
																		<input type="text"  name="" id="child2" maxlength="10" class="form-control" placeholder="ENTER MOBILE"  required data-validation-required-message="This field is required"> 
																	</div>
																</div>
															</div>
														</div>
													</div>	
												</div>
												<div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 pdr5">
													<div class="formtitle">
														<h4 class="backcolor">Bank Information</h4>
														<div class="row removemargin">
															<div class="col-12 col-sm-4 col-md-2 col-lg-2 col-xl-2 d-flex ">
																<div class="form-group">
																	<label>DEBIT LIMIT<span class="fored"><b>*</b></span> :</label>
																</div>
															</div>
															<div class="col-12 col-sm-8 col-md-4 col-lg-4 col-xl-4">
																<div class="form-group field">
																	<div class="controls">
																		<input type="text" name="" maxlength="10" class="form-control" placeholder="ENTER DEBIT LIMIT"  required data-validation-required-message="This field is required"> 
																	</div>
																</div>
															</div>
															
															<div class="col-12 col-sm-4 col-md-2 col-lg-2 col-xl-2 d-flex ">
																<div class="form-group">
																	<label>LIMIT DAYS<span class="fored"><b>*</b></span> :</label>
																</div>
															</div>
															<div class="col-12 col-sm-8 col-md-4 col-lg-4 col-xl-4">
																<div class="form-group field">
																	<div class="controls">
																		<input type="text" name="" maxlength="10" class="form-control" placeholder="ENTER LIMIT DAYS"  required data-validation-required-message="This field is required"> 
																	</div>
																</div>
															</div>
															
															<div class="col-12 col-sm-4 col-md-2 col-lg-2 col-xl-2 d-flex ">
																<div class="form-group">
																	<label>GRADE<span class="fored"><b>*</b></span> :</label>
																</div>
															</div>
															<div class="col-12 col-sm-8 col-md-4 col-lg-4 col-xl-4">
																<div class="form-group field">
																	<div class="controls">
																		<select name="" id="" required class="form-control">
																			<option value=""> --Select -- </option>
																			<option value="1"> 1 </option>
																		</select>
																	</div>
																</div>
															</div>
															<div class="col-12 col-sm-4 col-md-2 col-lg-2 col-xl-2 d-flex ">
																<div class="form-group">
																	<label>BILL CO<span class="fored"><b>*</b></span> :</label>
																</div>
															</div>
															<div class="col-12 col-sm-8 col-md-4 col-lg-4 col-xl-4">
																<div class="form-group field">
																	<div class="controls">
																		<select name="" id="" required class="form-control">
																			<option value=""> --Select -- </option>
																			<option value="PF"> PF </option>
																			<option value="SF"> SF </option>
																		</select>
																	</div>
																</div>
															</div>
															<div class="col-12 col-sm-4 col-md-2 col-lg-2 col-xl-2 d-flex ">
																<div class="form-group">
																	<label>REMARK<span class="fored"><b>*</b></span> :</label>
																</div>
															</div>
															<div class="col-12 col-sm-8 col-md-10 col-lg-10 col-xl-10">
																<div class="form-group field">
																	<div class="controls">
																		<input type="text" name="" class="form-control" placeholder="ENTER REMARK"  required data-validation-required-message="This field is required"> 
																	</div>
																</div>
															</div>
															
														</div>
													</div>
												</div>
											</div>	
											<div class="row">
												<div class="form-group">
													<div class="text-xs-right" style="margin-left: 8px;">
														<button type="submit" class="btn btn-info">Submit</button>
													</div>
												</div>
											</div>
									
										</div>	
									</div>
								</form>
								
						</div>
					</div>
				</div>
			</div>
		</div>

            
				
          


       
    </div>
</div>
 <?php $this->load->view('common/footer'); ?>
<script>
	 $(function() {
	  $('#staticParent').on('keydown', '#child', function(e){-1!==$.inArray(e.keyCode,[46,8,9,27,13,110])||(/65|67|86|88/.test(e.keyCode)&&(e.ctrlKey===true||e.metaKey===true))&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()});
	})
	$(function() {
	  $('#staticParent1').on('keydown', '#child1', function(e){-1!==$.inArray(e.keyCode,[46,8,9,27,13,110])||(/65|67|86|88/.test(e.keyCode)&&(e.ctrlKey===true||e.metaKey===true))&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()});
	})
	$(function() {
	  $('#staticParent2').on('keydown', '#child2', function(e){-1!==$.inArray(e.keyCode,[46,8,9,27,13,110])||(/65|67|86|88/.test(e.keyCode)&&(e.ctrlKey===true||e.metaKey===true))&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()});
	})

	$( document ).ready(function() {
		$( ".txtName" ).keypress(function(e) {
			var key = e.keyCode;
			if (key >= 48 && key <= 57) {
				e.preventDefault();
			}
		});
	});
</script>

 

<?php 
$gNo=$this->input->post('gNo')?$this->input->post('gNo'):'0';



?>
<?php $this->load->view('common/header'); ?>
<?php $this->load->view('market/market-js');?>
<style>
#success_message{ display: none;}
.form-group{
margin-bottom: 10px!important;
}
.form-control
{
min-height: 30px!important;
}
</style>

<div class="page-wrapper">
<div class="container-fluid">
	
	
	<div class="row page-titles">
		<div class="col-md-5 align-self-center">	
			<h4 class="text-themecolor">Company Manager</h4>
			
		</div>
		<div class="col-md-7 align-self-center text-right">
			<div class="d-flex justify-content-end align-items-center">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
					<li class="breadcrumb-item"><a href="javascript:void(0)">Master</a></li>
					<li class="breadcrumb-item active">Company Manager</li>
				</ol>
				<a href="javascript:fun_multipleDelete();" class="btn btn-info d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i> All Delete</a>
			</div>
		</div>
	</div>


<div class="row">
	<div class="card" style="width: 100%;">
		<div class="card-body p-b-0">
			
			<!-- Nav tabs -->
			<ul class="nav nav-tabs customtab2" role="tablist">
				<!-- <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#home7" role="tab"><span class="hidden-sm-up"><i class="ti-home"></i></span> <span class="hidden-xs-down">Company Manager List</span></a> </li> -->
				<li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#profile7" role="tab"><span class="hidden-sm-up"><i class="ti-user"></i></span> <span class="hidden-xs-down">Edit Form</span></a> </li>
				
			</ul>
			<!-- Tab panes -->
			<div class="tab-content">
				
				<div class="tab-pane active p-20" id="profile7" role="tabpanel">
					<form  class="companyform" id="companyform" method="POST" name="" novalidate>
						<div class="row common_master_form_div">
							<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 ">
								
								<div class="formtitle">
									<h4 class="backcolor">Account Information</h4>
									<div class="row removemargin">
										<div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
											<div class="row">
												<div class="col-12 col-sm-4 col-md-2 col-lg-2 col-xl-2 d-flex ">
													<div class="form-group">
														<label>CODE <span class="fored"><b>*</b></span> :</label>
													</div>
												</div>
												<div class="col-12 col-sm-8 col-md-10 col-lg-10 col-xl-10">
													<div class="form-group ">
														<div class="controls">
															<input type="text" name="code" id="code" class="form-control" placeholder="ENTER CODE" required data-validation-required-message="This field is required"> 
															<input type="hidden" name="gNo" >
														</div>
													</div>
												</div>
												
												
												<div class="col-12 col-sm-4 col-md-2 col-lg-2 col-xl-2 d-flex ">
													<div class="form-group">
														<label>SHORT NAME <span class="fored"><b>*</b></span> :</label>
													</div>
												</div>
												<div class="col-12 col-sm-8 col-md-3 col-lg-3 col-xl-3">
													<div class="form-group field">
														<div class="controls">
															<input type="text" name="shortname" id="shortname" class="form-control" placeholder="SHORT NAME" required data-validation-required-message="This field is required"> 
														</div>
													</div>
												</div>
												
												<div class="col-12 col-sm-4 col-md-1 col-lg-1 col-xl-1 d-flex ">
													<div class="form-group">
														<label> NAME <span class="fored"><b>*</b></span> :</label>
													</div>
												</div>
												<div class="col-12 col-sm-8 col-md-6 col-lg-6 col-xl-6">
													<div class="form-group field">
														<div class="controls">
															<input type="text" name="name" id="name" class="form-control txtName" id="txtName" placeholder="ENTER NAME" required data-validation-required-message="This field is required"> 
														</div>
													</div>
												</div>
												<div class="col-12 col-sm-4 col-md-3 col-lg-3 col-xl-3 d-flex ">
													<div class="form-group">
														<label>COMPANY TYPE  <span class="fored"><b>*</b></span> :</label>
													</div>
												</div>
												<div class="col-12 col-sm-8 col-md-9 col-lg-9 col-xl-9">
													<div class="form-group field">
														<div class="controls">
															<select name="companytype" id="companytype" required class="form-control">
																<option value=""> --Select -- </option>
																<option value="1"> 1 </option>
															</select>
														</div>
													</div>
												</div>
												
												
												<div class="col-12 col-sm-4 col-md-3 col-lg-3 col-xl-9 d-flex ">
													<div class="form-group">
														<label>COMPANY GROUP<span class="fored"><b>*</b></span> :</label>
													</div>
												</div>
												<div class="col-12 col-sm-8 col-md-9 col-lg-9 col-xl-9">
													<div class="form-group field">
														<div class="controls">
															<select name="companygroup" id="companygroup" required class="form-control">
																<option value=""> --Select -- </option>
																<option value="1"> 1 </option>
															</select>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
											<div class="row">
												<div class="col-12 col-sm-4 col-md-2 col-lg-2 col-xl-2 d-flex ">
													<div class="form-group">
														<label>ADDRESS<span class="fored"><b>*</b></span> :</label>
													</div>
												</div>
												<div class="col-12 col-sm-8 col-md-10 col-lg-10 col-xl-10">
													<div class="form-group field">
														<div class="controls">
															<input type="text" name="address" id="address" class="form-control" placeholder="ENTER ADDRESS"  required data-validation-required-message="This field is required"> 
														</div>
													</div>
												</div>
												<div class="col-12 col-sm-4 col-md-2 col-lg-2 col-xl-2 d-flex ">
													<div class="form-group">
														<label>ADD.(CONT.)<span class="fored"><b>*</b></span> :</label>
													</div>
												</div>
												<div class="col-12 col-sm-8 col-md-10 col-lg-10 col-xl-10">
													<div class="form-group field">
														<div class="controls">
															<input type="text" name="addcont" id="addcont" class="form-control" placeholder="ENTER ADD.(CONT.)"  required data-validation-required-message="This field is required"> 
														</div>
													</div>
												</div>
												
												<div class="col-12 col-sm-4 col-md-2 col-lg-2 col-xl-2 d-flex align-items-center">
													<div class="form-group">
														<label>CITY<span class="fored"><b>*</b></span> :</label>
													</div>
												</div>
												<div class="col-12 col-sm-8 col-md-4 col-lg-4 col-xl-4">
													<div class="form-group field">
														<div class="controls">
															<select name="city" id="city" required class="form-control">
																<option value=""> --Select -- </option>
																<?php 
																foreach($selectCity as $selectCitydata)
																{
																	?>
																	<option value="<?php echo $selectCitydata->cityID; ?>"> <?php echo $selectCitydata->cityName; ?></option>
																	<?php 
																}
																?>
															</select>
														</div>
													</div>
												</div>
												
												<div class="col-12 col-sm-4 col-md-2 col-lg-2 col-xl-2 d-flex align-items-center">
													<div class="form-group">
														<label> PIN<span class="fored"><b>*</b></span> :</label>
													</div>
												</div>
												<div class="col-12 col-sm-8 col-md-4 col-lg-4 col-xl-4">
													<div class="form-group field">
														<div class="controls">
															<input type="text" name="pin" id="pin" class="form-control" placeholder="PIN"  required data-validation-required-message="This field is required"> 
														</div>
													</div>
												</div>
												
												<div class="col-12 col-sm-4 col-md-2 col-lg-2 col-xl-2 d-flex align-items-center">
													<div class="form-group">
														<label> EMAIL <span class="fored"><b>*</b></span> :</label>
													</div>
												</div>
												<div class="col-12 col-sm-8 col-md-4 col-lg-4 col-xl-4">
													<div class="form-group field">
														<div class="controls">
															<input type="email" name="email" id="email" class="form-control" placeholder="EMAIL"  required data-validation-required-message="This field is required"> 
														</div>
													</div>
												</div>
												<div class="col-12 col-sm-4 col-md-2 col-lg-2 col-xl-2 d-flex align-items-center">
													<div class="form-group">
														<label> MOBILE NO.<span class="fored"><b>*</b></span> :</label>
													</div>
												</div>
												<div class="col-12 col-sm-8 col-md-4 col-lg-4 col-xl-4">
													<div class="form-group field" id="staticParent">
														<div class="controls">
															<input type="text" name="mobileno" id="child" maxlength="10" class="form-control" placeholder="MOBILE NO."  required data-validation-required-message="This field is required"> 
														</div>
													</div>
												</div>
												<div class="col-12 col-sm-4 col-md-2 col-lg-2 col-xl-2 d-flex align-items-center">
													<div class="form-group">
														<label> FAX<span class="fored"><b>*</b></span> :</label>
													</div>
												</div>
												<div class="col-12 col-sm-8 col-md-4 col-lg-4 col-xl-4">
													<div class="form-group field">
														<div class="controls">
															<input type="text" name="fax" id="fax" class="form-control" placeholder="FAX"  required data-validation-required-message="This field is required"> 
														</div>
													</div>
												</div>
												<div class="col-12 col-sm-4 col-md-2 col-lg-2 col-xl-2 d-flex align-items-center">
													<div class="form-group">
														<label> PHONE NO.<span class="fored"><b>*</b></span> :</label>
													</div>
												</div>
												<div class="col-12 col-sm-8 col-md-4 col-lg-4 col-xl-4">
													<div class="form-group field" id="staticParent1">
														<div class="controls">
															<input type="text" name="phoneno" id="child1" maxlength="10" class="form-control" placeholder="PHONE NO."  required data-validation-required-message="This field is required"> 
														</div>
													</div>
												</div>
												
												
												
												
											</div>
										</div>
									</div>	

								</div>
								
								<div class="formtitle">
									<h4 class="backcolor">Address Information</h4>
									<div class="row removemargin">
										<div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
											<div class="row">
												<div class="col-12 col-sm-4 col-md-3 col-lg-3 col-xl-3 d-flex ">
													<div class="form-group">
														<label>ADDRESS<span class="fored"><b>*</b></span> :</label>
													</div>
												</div>
												<div class="col-12 col-sm-8 col-md-9 col-lg-9 col-xl-9">
													<div class="form-group field">
														<div class="controls">
															<input type="text" name="address" id="address" class="form-control" placeholder="ENTER ADDRESS"  required data-validation-required-message="This field is required"> 
														</div>
													</div>
												</div>
												<div class="col-12 col-sm-4 col-md-3 col-lg-3 col-xl-3 d-flex ">
													<div class="form-group">
														<label>ADD.(CONT.)<span class="fored"><b>*</b></span> :</label>
													</div>
												</div>
												<div class="col-12 col-sm-8 col-md-9 col-lg-9 col-xl-9">
													<div class="form-group field">
														<div class="controls">
															<input type="text" name="addcont" name="addcont" class="form-control" placeholder="ENTER ADD.(CONT.)"  required data-validation-required-message="This field is required"> 
														</div>
													</div>
												</div>
												
												
												
												
											</div>
										</div>
										<div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
											<div class="row">
												<div class="col-12 col-sm-4 col-md-3 col-lg-3 col-xl-3 d-flex ">
													<div class="form-group">
														<label>BUSINESS DESC.<span class="fored"><b>*</b></span> :</label>
													</div>
												</div>
												<div class="col-12 col-sm-8 col-md-9 col-lg-9 col-xl-9">
													<div class="form-group field">
														<div class="controls">
															<input type="text" name="bussinessdesc" id="bussinessdesc" class="form-control" placeholder="ENTER BUSINESS DESCRIPTION"  required data-validation-required-message="This field is required"> 
														</div>
													</div>
												</div>
												<div class="col-12 col-sm-4 col-md-3 col-lg-3 col-xl-3 d-flex ">
													<div class="form-group">
														<label>PROPRIETOR <span class="fored"><b>*</b></span> :</label>
													</div>
												</div>
												<div class="col-12 col-sm-8 col-md-9 col-lg-9 col-xl-9">
													<div class="form-group field">
														<div class="controls">
															<input type="text" name="proprietor" id="proprietor" class="form-control" placeholder="ENTER PROPRIETOR"  required data-validation-required-message="This field is required"> 
														</div>
													</div>
												</div>
												
												
												
											</div>
										</div>
									</div>
								</div>
								<div class="formtitle">
									<h4 class="backcolor">Other Information</h4>
									<div class="row removemargin">
										<div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
											<div class="row">
												<div class="col-12 col-sm-4 col-md-2 col-lg-2 col-xl-2 d-flex align-items-center">
													<div class="form-group">
														<label>MULTI CHAL<span class="fored"><b>*</b></span> :</label>
													</div>
												</div>
												<div class="col-12 col-sm-8 col-md-10 col-lg-10 col-xl-10">
													<div class="form-group field">
														<div class="controls">
															<select name="multichal" id="multichal" required class="form-control">
																<option value=""> --Select -- </option>
																<option value="NO"> NO </option>
																<option value="YES"> YES </option>
															</select>
														</div>
													</div>
												</div>
												
												<div class="col-12 col-sm-4 col-md-2 col-lg-2 col-xl-2 d-flex ">
													<div class="form-group">
														<label>SELECTED <span class="fored"><b>*</b></span> :</label>
													</div>
												</div>
												<div class="col-12 col-sm-8 col-md-10 col-lg-10 col-xl-10">
													<div class="form-group field">
														<div class="controls">
															<input type="checkbox" value="1"  name="isActive" > J.V. OF OLD YEAR BILLS DISCOUNT IN NEW YEAR ?
														</div>
													</div>
												</div>
												<div class="col-12 col-sm-4 col-md-2 col-lg-2 col-xl-2 d-flex ">
													<div class="form-group">
														<label>J.V. FROM DATE  <span class="fored"><b>*</b></span> :</label>
													</div>
												</div>
												<div class="col-12 col-sm-8 col-md-4 col-lg-4 col-xl-4">
													<div class="form-group field">
														<div class="controls">
															<input type="date" name="fromdate" id="fromdate" class="form-control"  required data-validation-required-message="This field is required"> 
														</div>
													</div>
												</div>
												<div class="col-12 col-sm-4 col-md-2 col-lg-2 col-xl-2 d-flex align-items-center">
													<div class="form-group">
														<label>PAN NO. <span class="fored"><b>*</b></span> :</label>
													</div>
												</div>
												<div class="col-12 col-sm-8 col-md-4 col-lg-4 col-xl-4">
													<div class="form-group field">
														<div class="controls">
															<input type="text" name="panno" id="panno" maxlength="10" class="form-control" placeholder="ENTER PAN NO."  required data-validation-required-message="This field is required"> 
														</div>
													</div>
												</div>
												
												<div class="col-12 col-sm-4 col-md-2 col-lg-2 col-xl-2 d-flex align-items-center">
													<div class="form-group">
														<label>TDS A/C No.<span class="fored"><b>*</b></span> :</label>
													</div>
												</div>
												<div class="col-12 col-sm-8 col-md-4 col-lg-4 col-xl-4">
													<div class="form-group field">
														<div class="controls">
															<input type="text" name="tdsacno" id="tdsacno" maxlength="10" class="form-control" placeholder="ENTER TDS A/C No."  required data-validation-required-message="This field is required"> 
														</div>
													</div>
												</div>
												
												<div class="col-12 col-sm-4 col-md-2 col-lg-2 col-xl-2 d-flex align-items-center">
													<div class="form-group">
														<label>WARD <span class="fored"><b>*</b></span> :</label>
													</div>
												</div>
												<div class="col-12 col-sm-8 col-md-4 col-lg-4 col-xl-4">
													<div class="form-group field">
														<div class="controls">
															<input type="text" name="ward" id="ward" maxlength="10" class="form-control" placeholder="ENTER WARD"  required data-validation-required-message="This field is required"> 
														</div>
													</div>
												</div>
												
												<div class="col-12 col-sm-4 col-md-2 col-lg-2 col-xl-2 d-flex align-items-center">
													<div class="form-group">
														<label>ECC NO.<span class="fored"><b>*</b></span> :</label>
													</div>
												</div>
												<div class="col-12 col-sm-8 col-md-4 col-lg-4 col-xl-4">
													<div class="form-group field">
														<div class="controls">
															<input type="text" name="eccno" id="eccno" maxlength="10" class="form-control" placeholder="ENTER ECC NO."  required data-validation-required-message="This field is required"> 
														</div>
													</div>
												</div>
												
												<div class="col-12 col-sm-4 col-md-2 col-lg-2 col-xl-2 d-flex align-items-center">
													<div class="form-group">
														<label>RANGE <span class="fored"><b>*</b></span> :</label>
													</div>
												</div>
												<div class="col-12 col-sm-8 col-md-4 col-lg-4 col-xl-4">
													<div class="form-group field">
														<div class="controls">
															<input type="text" name="range" id="range" maxlength="10" class="form-control" placeholder="ENTER RANGE"  required data-validation-required-message="This field is required"> 
														</div>
													</div>
												</div>
												
												
												
												
												
												
											</div>
										</div>
										<div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
											<div class="row">
												<div class="col-12 col-sm-4 col-md-2 col-lg-2 col-xl-2 d-flex align-items-center">
													<div class="form-group">
														<label>DIVISION<span class="fored"><b>*</b></span> :</label>
													</div>
												</div>
												<div class="col-12 col-sm-8 col-md-4 col-lg-4 col-xl-4">
													<div class="form-group field">
														<div class="controls">
															<input type="text" name="division" id="division" maxlength="10" class="form-control" placeholder="ENTER DIVISION"  required data-validation-required-message="This field is required"> 
														</div>
													</div>
												</div>
												
												<div class="col-12 col-sm-4 col-md-2 col-lg-2 col-xl-2 d-flex align-items-center">
													<div class="form-group">
														<label>COLLECTRATE <span class="fored"><b>*</b></span> :</label>
													</div>
												</div>
												<div class="col-12 col-sm-8 col-md-4 col-lg-4 col-xl-4">
													<div class="form-group field">
														<div class="controls">
															<input type="text" name="collectrate" id="collectrate" maxlength="10" class="form-control" placeholder="ENTER COLLECTRATE"  required data-validation-required-message="This field is required"> 
														</div>
													</div>
												</div>
												<div class="col-12 col-sm-4 col-md-2 col-lg-2 col-xl-2 d-flex align-items-center">
													<div class="form-group">
														<label>POLICY NO.<span class="fored"><b>*</b></span> :</label>
													</div>
												</div>
												<div class="col-12 col-sm-8 col-md-4 col-lg-4 col-xl-4">
													<div class="form-group field">
														<div class="controls">
															<input type="text" name="policyno" id="policyno" maxlength="10" class="form-control" placeholder="ENTER POLICY NO."  required data-validation-required-message="This field is required"> 
														</div>
													</div>
												</div>
												<div class="col-12 col-sm-4 col-md-2 col-lg-2 col-xl-2 d-flex ">
													<div class="form-group">
														<label>DATE  <span class="fored"><b>*</b></span> :</label>
													</div>
												</div>
												<div class="col-12 col-sm-8 col-md-4 col-lg-4 col-xl-4">
													<div class="form-group field">
														<div class="controls">
															<input type="date" name="date" id="date" class="form-control"  required data-validation-required-message="This field is required"> 
														</div>
													</div>
												</div>
												<div class="col-12 col-sm-4 col-md-2 col-lg-2 col-xl-2 d-flex ">
													<div class="form-group">
														<label>GST NO.(VAT)  <span class="fored"><b>*</b></span> :</label>
													</div>
												</div>
												<div class="col-12 col-sm-8 col-md-4 col-lg-4 col-xl-4">
													<div class="form-group field">
														<div class="controls">
															<input type="text" id="gstno" name="gstno" placeholder="ENTER GST NO.(VAT)" name="" class="form-control"  required data-validation-required-message="This field is required"> 
														</div>
													</div>
												</div>
												<div class="col-12 col-sm-4 col-md-2 col-lg-2 col-xl-2 d-flex ">
													<div class="form-group">
														<label>DT.  <span class="fored"><b>*</b></span> :</label>
													</div>
												</div>
												<div class="col-12 col-sm-8 col-md-4 col-lg-4 col-xl-4">
													<div class="form-group field">
														<div class="controls">
															<input type="text" placeholder="ENTER DT." name="dt" id="dt" class="form-control"  required data-validation-required-message="This field is required"> 
														</div>
													</div>
												</div>
												
												
												<div class="col-12 col-sm-4 col-md-2 col-lg-2 col-xl-2 d-flex ">
													<div class="form-group">
														<label>CIN NO.  <span class="fored"><b>*</b></span> :</label>
													</div>
												</div>
												<div class="col-12 col-sm-8 col-md-4 col-lg-4 col-xl-4">
													<div class="form-group field">
														<div class="controls">
															<input type="text" placeholder="ENTER CIN NO." name="cinno" id="cinno" class="form-control"  required data-validation-required-message="This field is required"> 
														</div>
													</div>
												</div>
												<div class="col-12 col-sm-4 col-md-2 col-lg-2 col-xl-2 d-flex ">
													<div class="form-group">
														<label>GST IN/UIN  <span class="fored"><b>*</b></span> :</label>
													</div>
												</div>
												<div class="col-12 col-sm-8 col-md-4 col-lg-4 col-xl-4">
													<div class="form-group field">
														<div class="controls">
															<input type="text" placeholder="ENTER GST IN/UIN" name="gstinuin" id="gstinuin" class="form-control"  required data-validation-required-message="This field is required"> 
														</div>
													</div>
												</div>
												<div class="col-12 col-sm-4 col-md-3 col-lg-3 col-xl-3 d-flex ">
													<div class="form-group">
														<label>CEN. EXCISE REG. NO.  <span class="fored"><b>*</b></span> :</label>
													</div>
												</div>
												<div class="col-12 col-sm-8 col-md-9 col-lg-9 col-xl-9">
													<div class="form-group field">
														<div class="controls">
															<input type="text" placeholder="ENTER CEN. EXCISE REG. NO." name="cenexcise" id="cenexcise" class="form-control"  required data-validation-required-message="This field is required"> 
														</div>
													</div>
												</div>
												
											</div>
										</div>
										<div class="col-12 col-sm-4 col-md-2 col-lg-2 col-xl-2 d-flex ">
											<div class="form-group">
												<label>INSURANCE POLICY DETAILS  <span class="fored"><b>*</b></span> :</label>
											</div>
										</div>
										<div class="col-12 col-sm-8 col-md-10 col-lg-10 col-xl-10">
											<div class="form-group field">
												<div class="controls">
													<input type="text" placeholder="INSURANCE POLICY DETAILS" name="insurance" id="insurance" class="form-control"  required data-validation-required-message="This field is required"> 
												</div>
											</div>
										</div>
										
									</div>
								</div>
								
								<div class="row">
									<div class="form-group">
										<div class="text-xs-right" style="margin-left: 8px;">
											<button type="submit" class="btn btn-primary">
                                                    Test Validation <i class="icon-ok icon-white"></i>
                                                </button>

											<!-- <input type="submit" value="Submit" class="btn btn-info"> -->
										</div>
									</div>
								</div>
								
								
							</div>	
						</div>
					</form>
					
					
				</div>
			</div>
		</div>
	</div>
</div>

</div>
</div>
<?php $this->load->view('common/footer'); ?>

<script>

	var base_url = '<?php echo base_url();?>';

$(function() {
	$('#staticParent').on('keydown', '#child', function(e){-1!==$.inArray(e.keyCode,[46,8,9,27,13,110])||(/65|67|86|88/.test(e.keyCode)&&(e.ctrlKey===true||e.metaKey===true))&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()});
})
$(function() {
	$('#staticParent1').on('keydown', '#child1', function(e){-1!==$.inArray(e.keyCode,[46,8,9,27,13,110])||(/65|67|86|88/.test(e.keyCode)&&(e.ctrlKey===true||e.metaKey===true))&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()});
})


$( document ).ready(function() {
	$( ".txtName" ).keypress(function(e) {
		var key = e.keyCode;
		if (key >= 48 && key <= 57) {
			e.preventDefault();
		}
	});
});

   
</script>
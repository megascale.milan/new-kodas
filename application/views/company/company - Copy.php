
<?php 
$gNo=$this->input->post('gNo')?$this->input->post('gNo'):'0';



?>
<?php $this->load->view('common/header'); ?>
<?php $this->load->view('market/market-js');?>
<style>
    #success_message{ display: none;}
	.form-group{
	    margin-bottom: 10px!important;
	}
	.form-control
	{
		    min-height: 30px!important;
	}
</style>

<div class="page-wrapper">
    <div class="container-fluid">
	
	
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h4 class="text-themecolor">Account Group Manager</h4>
				
            </div>
            <div class="col-md-7 align-self-center text-right">
                <div class="d-flex justify-content-end align-items-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Master</a></li>
                        <li class="breadcrumb-item active">Account Group Manager</li>
                    </ol>
					<!--<a href="javascript:fun_edit(0);" class="btn btn-info d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i> Create New</a>-->
                    <a href="javascript:fun_multipleDelete();" class="btn btn-info d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i> All Delete</a>
                </div>
            </div>
        </div>
		
		
		<div class="row">
			<div class="card" style="width: 100%;">
				<div class="card-body p-b-0">
				
					<!-- Nav tabs -->
					<ul class="nav nav-tabs customtab2" role="tablist">
						<li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#home7" role="tab"><span class="hidden-sm-up"><i class="ti-home"></i></span> <span class="hidden-xs-down">Market List</span></a> </li>
						<li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#profile7" role="tab"><span class="hidden-sm-up"><i class="ti-user"></i></span> <span class="hidden-xs-down">Add Form</span></a> </li>
						
					</ul>
					<!-- Tab panes -->
					<div class="tab-content">
						<div class="tab-pane active" id="home7" role="tabpanel">
							<div class="p-20">
							<form  name="frm_market_list" method="post" action="<?php echo base_url();?>Market_controller/index">
							<input type="hidden" value="<?php echo $gNo; ?>" name="gNo" />
							 <input type="hidden" name="method" value="" /> 
								<div class="table-responsive">
                                    <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th><input type="checkbox" name="checkall" id= "checkall" value=""  /> Checkbox</th>
                                                <th>Market Name</th>
                                                <th>CreateDate</th>
                                                <th>Is Active</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            
                                        </tbody>
                                    </table>
                                </div>
								</form>
								</div>
						</div>
						<div class="tab-pane  p-20" id="profile7" role="tabpanel">
							<form action="<?php echo base_url() ?>Company_controller/saveuser" class="" method="post" name="addform" novalidate>
									
									
									<div class="row">
										<div class="col-md-1">
											<div class="form-group">
												<h5>CODE <span class="text-danger">*</span></h5>
												<div class="controls">
													<input type="text" name="gCode" class="form-control" required data-validation-required-message="This field is required"> 
													<input type="hidden" name="gNo" >
												</div>
											</div>
										</div>
										
										<div class="col-md-2">
											<div class="form-group">
												<h5>SHORT NAME <span class="text-danger">*</span></h5>
												<div class="controls">
													<input type="text" name="" class="form-control" required data-validation-required-message="This field is required"> 
												</div>
											</div>
										</div>
										
										<div class="col-md-2">
											<div class="form-group">
												<h5 style="text-align: left;">Company Type <span class="text-danger">*</span></h5>
												<div class="controls" style="text-align: left;">
													<select name="" id="" required class="form-control">
														
														<option value=""> --Select -- </option>
														<option value=""> 1 </option>
													</select>
												</div>
											</div>
										</div>
										
										<div class="col-md-3">
											<div class="form-group">
												<h5>NAME<span class="text-danger">*</span></h5>
												<div class="controls">
													<input type="text" name="" class="form-control" required data-validation-required-message="This field is required"> 
												</div>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<h5 style="text-align: left;">Company Group <span class="text-danger">*</span></h5>
												<div class="controls" style="text-align: left;">
													<select name="" id="" required class="form-control">
														
														<option value=""> --Select -- </option>
														<option value=""> 1 </option>
													</select>
												</div>
											</div>
										</div>
									</div>	
									
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<h5> ADDRESS <span class="text-danger">*</span></h5>
												<div class="controls">
													<input type="text" name="" class="form-control" required data-validation-required-message="This field is required"> 
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<h5>ADDRESS(CONT.) <span class="text-danger">*</span></h5>
												<div class="controls">
													<input type="text" name="" class="form-control" required data-validation-required-message="This field is required"> 
												</div>
											</div>
										</div>
									</div>
									
									
									<div class="row">
										<div class="col-md-2">
											<div class="form-group">
												<h5 style="text-align: left;">CITY <span class="text-danger">*</span></h5>
												<div class="controls" style="text-align: left;">
													<select name="" id="" required class="form-control">
														<option value=""> --Select -- </option>
														<?php 
														foreach($selectCity as $selectCitydata)
														{
														?>
														<option value="<?php echo $selectCitydata->cityID; ?>"> <?php echo $selectCitydata->cityName; ?></option>
														<?php 
														}
														?>
													</select>
												</div>
											</div>
										</div>
										<div class="col-md-1">
											<div class="form-group">
												<h5>PIN <span class="text-danger">*</span></h5>
												<div class="controls">
													<input type="text" name="" class="form-control" required data-validation-required-message="This field is required"> 
													
												</div>
											</div>
										</div>
										<div class="col-md-2">
											<div class="form-group">
												<h5>EMAIL <span class="text-danger">*</span></h5>
												<div class="controls">
													<input type="email" name="" class="form-control" required data-validation-required-message="This field is required"> 
												
												</div>
											</div>
										</div>
										
										<div class="col-md-2">
											<div class="form-group">
												<h5>Mobile No. <span class="text-danger">*</span></h5>
												<div class="controls">
													<input type="text" name="" class="form-control" required data-validation-required-message="This field is required"> 
												
												</div>
											</div>
										</div>
										<div class="col-md-2">
											<div class="form-group">
												<h5>FAX <span class="text-danger">*</span></h5>
												<div class="controls">
													<input type="text" name="" class="form-control" required data-validation-required-message="This field is required"> 
												
												</div>
											</div>
										</div>
										
										<div class="col-md-3">
											<div class="form-group">
												<h5>Phone No. <span class="text-danger">*</span></h5>
												<div class="controls">
													<input type="text" name="" class="form-control" required data-validation-required-message="This field is required"> 
												
												</div>
											</div>
										</div>
									</div>
									
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<h5> ADDRESS <span class="text-danger">*</span></h5>
												<div class="controls">
													<input type="text" name="" class="form-control" required data-validation-required-message="This field is required"> 
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<h5>ADDRESS(CONT.) <span class="text-danger">*</span></h5>
												<div class="controls">
													<input type="text" name="" class="form-control" required data-validation-required-message="This field is required"> 
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-8">
											<div class="form-group">
												<h5> BUSINESS DESCRIPTION <span class="text-danger">*</span></h5>
												<div class="controls">
													<input type="text" name="" class="form-control" required data-validation-required-message="This field is required"> 
												</div>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<h5>PROPRIETOR <span class="text-danger">*</span></h5>
												<div class="controls">
													<input type="text" name="" class="form-control" required data-validation-required-message="This field is required"> 
												</div>
											</div>
										</div>
									</div>
									
									<div class="row">
										<div class="col-md-2">
											<div class="form-group">
												<h5 style="text-align: left;">CITY <span class="text-danger">*</span></h5>
												<div class="controls" style="text-align: left;">
													<select name="" id="" required class="form-control">
														<option value=""> --Select -- </option>
														<option value="YES">YES </option>
														<option value="NO">NO</option>
														
													</select>
												</div>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group" style="padding-top: 35px;" >
												 J.V. OF OLD YEAR BILLS DISCOUNT IN NEW YEAR ? <input type="checkbox" value="1"  name="isActive" >
											</div>
										</div>
										<div class="col-md-2">
											<div class="form-group">
												<h5>J.V. FROM DATE <span class="text-danger">*</span></h5>
												<div class="controls">
													<input type="DATE" name="" class="form-control" required data-validation-required-message="This field is required"> 
												</div>
											</div>
										</div>
										<div class="col-md-2">
											<div class="form-group">
												<h5>PAN NO. <span class="text-danger">*</span></h5>
												<div class="controls">
													<input type="text" name="" class="form-control" required data-validation-required-message="This field is required"> 
												</div>
											</div>
										</div>
										<div class="col-md-2">
											<div class="form-group">
												<h5>TDS A/C No. <span class="text-danger">*</span></h5>
												<div class="controls">
													<input type="text" name="" class="form-control" required data-validation-required-message="This field is required"> 
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-2">
											<div class="form-group">
												<h5>WARD<span class="text-danger">*</span></h5>
												<div class="controls">
													<input type="text" name="" class="form-control" required data-validation-required-message="This field is required"> 
												</div>
											</div>
										</div>
										<div class="col-md-2">
											<div class="form-group">
												<h5>ECC NO.<span class="text-danger">*</span></h5>
												<div class="controls">
													<input type="text" name="" class="form-control" required data-validation-required-message="This field is required"> 
												</div>
											</div>
										</div>
										<div class="col-md-2">
											<div class="form-group">
												<h5>RANGE<span class="text-danger">*</span></h5>
												<div class="controls">
													<input type="text" name="" class="form-control" required data-validation-required-message="This field is required"> 
												</div>
											</div>
										</div>
										<div class="col-md-2">
											<div class="form-group">
												<h5>DIVISION<span class="text-danger">*</span></h5>
												<div class="controls">
													<input type="text" name="" class="form-control" required data-validation-required-message="This field is required"> 
												</div>
											</div>
										</div>
										<div class="col-md-2">
											<div class="form-group">
												<h5>COLLECTRATE<span class="text-danger">*</span></h5>
												<div class="controls">
													<input type="text" name="" class="form-control" required data-validation-required-message="This field is required"> 
												</div>
											</div>
										</div>
										<div class="col-md-2">
											<div class="form-group">
												<h5>POLICY NO.<span class="text-danger">*</span></h5>
												<div class="controls">
													<input type="text" name="" class="form-control" required data-validation-required-message="This field is required"> 
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-2">
											<div class="form-group">
												<h5>DATE<span class="text-danger">*</span></h5>
												<div class="controls">
													<input type="date" name="" class="form-control" required data-validation-required-message="This field is required"> 
												</div>
											</div>
										</div>
										<div class="col-md-2">
											<div class="form-group">
												<h5>GST  NO.(VAT)<span class="text-danger">*</span></h5>
												<div class="controls">
													<input type="text" name="" class="form-control" required data-validation-required-message="This field is required"> 
												</div>
											</div>
										</div>
										<div class="col-md-2">
											<div class="form-group">
												<h5>DT.<span class="text-danger">*</span></h5>
												<div class="controls">
													<input type="text" name="" class="form-control" required data-validation-required-message="This field is required"> 
												</div>
											</div>
										</div>
										<div class="col-md-4">	
											<div class="form-group">
												<h5>CEN. EXCISE REG. NO.<span class="text-danger">*</span></h5>
												<div class="controls">
													<input type="text" name="" class="form-control" required data-validation-required-message="This field is required"> 
												</div>
											</div>
										</div>
										<div class="col-md-2">	
											<div class="form-group">
												<h5>CIN NO.<span class="text-danger">*</span></h5>
												<div class="controls">
													<input type="text" name="" class="form-control" required data-validation-required-message="This field is required"> 
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-2">	
											<div class="form-group">
												<h5>GST IN/UIN<span class="text-danger">*</span></h5>
												<div class="controls">
													<input type="text" name="" class="form-control" required data-validation-required-message="This field is required"> 
												</div>
											</div>
										</div>
										<div class="col-md-4">	
											<div class="form-group">
												<h5>INSURANCE POLICY DETAILS<span class="text-danger">*</span></h5>
												<div class="controls">
													<input type="text" name="" class="form-control" required data-validation-required-message="This field is required"> 
												</div>
											</div>
										</div>
										
									</div>	
										<div class="form-group">
											<div class="text-xs-right">
												<button type="submit" class="btn btn-info">Submit</button>
											</div>
										</div>		

											
									
								</form>	
						
						</div>
					</div>
				</div>
			</div>
		</div>

            
				
          


       
    </div>
</div>
 <?php $this->load->view('common/footer'); ?>

 
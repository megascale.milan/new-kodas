<?php 
$AccNo=$AccNo=$this->input->post('AccNo')?$this->input->post('AccNo'):'0';
$AccType=$this->input->post('AccType')?$this->input->post('AccType'):'';
$Name=$this->input->post('Name')?$this->input->post('Name'):'';
$Letter=$this->input->post('Letter')?$this->input->post('Letter'):'';
$BalSheet=$this->input->post('BalSheet')?$this->input->post('BalSheet'):'';
$TRIALside=$this->input->post('TRIALside')?$this->input->post('TRIALside'):'';
$TrialPos=$this->input->post('TrialPos')?$this->input->post('TrialPos'):'';
if($AccNo != NULL && $AccNo >0)
{
	if(isset($getsingleaddtype))
	{
		foreach($getsingleaddtype as $getsingleaddtypedata)
		{
			$AccType=$getsingleaddtypedata[0]->AccType;
			$Name=$getsingleaddtypedata[0]->Name;
			$Letter=$getsingleaddtypedata[0]->Letter;
			$BalSheet=$getsingleaddtypedata[0]->BalSheet;
			$TRIALside=$getsingleaddtypedata[0]->TRIALside;
			$TrialPos=$getsingleaddtypedata[0]->TrialPos;
		}
	}
}
?>
<?php $this->load->view('common/header'); ?>
<style>
    #success_message{ display: none;}
</style>
<div class="page-wrapper">
    <div class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h4 class="text-themecolor">Add/Edit Account Type Master</h4>
            </div>
            <div class="col-md-7 align-self-center text-right">
                <div class="d-flex justify-content-end align-items-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Master</a></li>
                        <li class="breadcrumb-item active">Add/Edit Account Type Master</li>
                    </ol>
                 </div>
            </div>
        </div>
        

		<div class="row">
                    <div class="col-md-3 col-sm-12 col-xs-12"></div>
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="card-body">
                               <form action="<?php echo base_url() ?>Accounttype_controller/saveuser" class="m-t-40" method="post" name="addform" novalidate>
									<div class="form-group">
                                        <h5>Account Type <span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="text" name="AccType" class="form-control"  value="<?php echo $AccType; ?>" required data-validation-required-message="This field is required"> 
										
										</div>
									</div>
									<div class="form-group">
                                        <h5>Name <span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="text" name="Name" class="form-control"  value="<?php echo $Name; ?>" required data-validation-required-message="This field is required"> 
											<input type="hidden" name="AccNo" value="<?php echo $AccNo; ?>">
										</div>
									</div>
									<div class="form-group">
                                        <h5>Letter <span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="text" name="Letter" class="form-control"  value="<?php echo $Letter; ?>" required data-validation-required-message="This field is required"> 
										</div>
									</div>

									<div class="form-group">
                                        <h5>Balance Sheet <span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="text" name="BalSheet" class="form-control"  value="<?php echo $BalSheet; ?>" required data-validation-required-message="This field is required"> 
										</div>
									</div>
									<div class="form-group">
                                        <h5>Trail Side <span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="text" name="TRIALside" class="form-control"  value="<?php echo $TRIALside; ?>" required data-validation-required-message="This field is required"> 
										</div>
									</div>
									<div class="form-group">
                                        <h5>Trail Position <span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="text" name="TrialPos" class="form-control"  value="<?php echo $TrialPos; ?>" required data-validation-required-message="This field is required"> 
										</div>
									</div>	


									
									
                                    <div class="text-xs-right">
                                        <button type="submit" class="btn btn-info">Submit</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
					<div class="col-md-3 col-sm-12 col-xs-12"></div>
                </div>
       
    </div>
</div>
 <?php $this->load->view('common/footer'); ?>

 
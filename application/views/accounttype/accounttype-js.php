 <script src="<?php echo base_url(); ?>assets/node_modules/jquery/jquery-3.2.1.min.js"></script>
<script type="text/javascript">
function fun_edit(AccNo)
{
	
	var frmobj=window.document.frm_acctype_list;
	frmobj.AccNo.value=AccNo;
	frmobj.action="<?php echo base_url()?>Accounttype_controller/index";
	frmobj.submit();
}

function fun_single_delete(AccNo)
{
	if(confirm("Are you sure want to delete this record? "))
	{
		$.ajax({
			type: "POST",
			url: "<?php echo base_url() ?>Accounttype_controller/singledelete",
			data: "AccNo=" + AccNo,
			success: function(total){
			$("#ID_"+AccNo).html(total);
			}
		
		});
	}
}
/******************* CHECK ALL CHECKBOX *****************/
jQuery(document).ready(function($)
{
	$('#checkall').click(function()
	{
		$(':checkbox').each(function()
		{
			if(this.checked)
			{
				this.checked = false;
			}
			else
			{
				this.checked = true;
			}
		});
		return false;
	}); 
}); 

/****************** MULTIPLE DELETE *************/
	function fun_multipleDelete()
	{
	  var count = $(":checkbox:checked").length;

	  if(count > 0)
	  {
		  var status = confirm("Are you sure want to delete this record?");
		  if(status==true)
		  {
		  	
			  frmobj=window.document.frm_acctype_list;
			  frmobj.method.value="multipleDelete";
			  document.frm_acctype_list.action="<?php echo base_url();?>Accounttype_controller/index";
			  frmobj.submit();
		  }
	  }
	  else
	  {
		alert('Please select atleast one record.');
	  }
	}
</script>
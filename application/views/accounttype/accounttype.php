
<?php $this->load->view('common/header'); ?>
<?php $this->load->view('accounttype/accounttype-js');?>
<style>
    #success_message{ display: none;}
</style>

<div class="page-wrapper">
    <div class="container-fluid">
	
	
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h4 class="text-themecolor">Account Type Master</h4>
				
            </div>
            <div class="col-md-7 align-self-center text-right">
                <div class="d-flex justify-content-end align-items-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Master</a></li>
                        <li class="breadcrumb-item active">Account Type Master</li>
                    </ol>
					<!--<a href="javascript:fun_edit(0);" class="btn btn-info d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i> Create New</a>-->
                    <a href="javascript:fun_multipleDelete();" class="btn btn-info d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i> All Delete</a>
                </div>
            </div>
        </div>
		
		
		<div class="row">
			<div class="card" style="width: 100%;">
				<div class="card-body p-b-0">
					<!-- Nav tabs -->
					<ul class="nav nav-tabs customtab2" role="tablist">
						<li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#home7" role="tab"><span class="hidden-sm-up"><i class="ti-home"></i></span> <span class="hidden-xs-down">Account Type List</span></a> </li>
						<li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#profile7" role="tab"><span class="hidden-sm-up"><i class="ti-user"></i></span> <span class="hidden-xs-down">Add Form</span></a> </li>

						<?php
						if(!empty($editaccountdata))
						{
							?>
							<li class="nav-item"> <a class="nav-link foractive" data-toggle="tab" href="#editform" role="tab"><span class="hidden-sm-up"><i class="ti-user"></i></span> <span class="hidden-xs-down">Edit Form</span></a> </li>
							<?php
						}
						?>

					</ul>
					<!-- Tab panes -->
					<div class="tab-content">
						<div class="tab-pane active" id="home7" role="tabpanel">
							<div class="p-20">
								<form name="frm_acctype_list" method="post" action="<?php echo base_url();?>Accounttype_controller/index">
									<input type="hidden" name="method" value="" /> 
									<div class="table-responsive">
										<table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
											<thead>
												<tr>
													<th><input type="checkbox" name="checkall" id= "checkall" value=""  /> Checkbox</th>
													<th>Account Type</th>
													<th>Balance Sheet</th>
													<th>Trial side</th>
													<th>Trial Pos</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody>
												<?php 
												foreach($getallaccounttype as $getallaccounttypedata)
												{
													?>
													<tr id="ID_<?php echo $getallaccounttypedata->AccNo; ?>">
														<td><input class="checkbox" name="checkUncheck[]"  id="checkAllAuto" value="<?php echo $getallaccounttypedata->AccNo; ?>" type="checkbox" /></td>
														<td><?php echo $getallaccounttypedata->AccType; ?></td>
														<td><?php echo $getallaccounttypedata->BalSheet; ?></td>
														<td><?php echo $getallaccounttypedata->TRIALside; ?></td>
														<td><?php echo $getallaccounttypedata->TrialPos; ?></td>
													    <td class="editdelaction">
															<a href="<?=base_url();?>Accounttype_controller/?accid=<?=$getallaccounttypedata->AccNo;?>"  ><i class="fa fa-edit"></i></a>&nbsp;&nbsp;
															<a href="javascript:fun_single_delete(<?php echo $getallaccounttypedata->AccNo; ?>);"><i class="fa fa-trash-o"></i></a>
														</td>
													</tr>
													<?php 
												}
												?>
											</tbody>
										</table>
									</div>
								</form>
							</div>
						</div>
						<div class="tab-pane  p-20" id="profile7" role="tabpanel">
							<form action="" class="m-t-40" method="post" name="addaccountform" id="addaccountform" novalidate>

								<?php
								if(empty($editaccountdata))
								{
									?>
										<input type="hidden" value="" id="accid" name="accid">
									<?php
								}
								?>

								<div class="row">
									<div class="col-md-4">
										<div class="form-group">
											<h5>Account Type <span class="text-danger">*</span></h5>
											<div class="controls">
												<input type="text" name="AccType" class="form-control"  value="" required data-validation-required-message="This field is required" placeholder="Enter Account Type"> 
											</div>
										</div>
									</div>
									<div class="col-md-4">

										<div class="form-group field">
											<h5>In P&L <span class="text-danger">*</span></h5>
											<div class="controls">
												<select name="inpl" id="inpl" required="" class="form-control" aria-invalid="false">
													<option value=""> --Select In P&L-- </option>
													<option value="Yes"> Yes </option>
													<option value="No"> No </option>
												</select>
											<div class="help-block"></div></div>
										</div>

										<!-- 	<div class="form-group">
											<h5>In P&L <span class="text-danger">*</span></h5>
											<div class="controls">
												<input type="text" name="Name" class="form-control txtName" id="txtName"  value="<?php echo $Name; ?>" required data-validation-required-message="This field is required"> 
												<input type="hidden" name="AccNo" value="<?php echo $AccNo; ?>" placeholder="In">
											</div>
										</div> -->
									</div>
									<div class="col-md-4">
										<!-- <div class="form-group">
											<h5>In Tranding<span class="text-danger">*</span></h5>
											<div class="controls">
												<input type="text" name="Letter" class="form-control"  value="<?php echo $Letter; ?>" required data-validation-required-message="This field is required"> 
											</div>
										</div> -->
										<div class="form-group field">
											<h5>In Tranding <span class="text-danger">*</span></h5>
											<div class="controls">
												<select name="intranding" id="intranding" required="" class="form-control" aria-invalid="false">
													<option value=""> --Select In Tranding-- </option>
													<option value="Yes"> Yes </option>
													<option value="No"> No </option>
												</select>
											<div class="help-block"></div></div>
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group field">
											<h5>Balance Sheet <span class="text-danger">*</span></h5>
											<div class="controls">
												<select name="BalSheet" id="BalSheet" required="" class="form-control" aria-invalid="false">
													<option value=""> --Select Balance Sheet-- </option>
													<option value="Yes"> Yes </option>
													<option value="No"> No </option>
												</select>
											<div class="help-block"></div></div>
										</div>
									</div>

									<div class="col-md-4">
										<div class="form-group field">
											<h5>Trail Balance Side <span class="text-danger">*</span></h5>
											<div class="controls">
												<select name="TRIALside" id="TRIALside" required="" class="form-control" aria-invalid="false">
													<option value=""> --Select Trail Balance Side-- </option>
													<option value="Credit"> Credit </option>
													<option value="Debit"> Debit </option>
												</select>
											<div class="help-block"></div></div>
										</div>
									</div>

									<div class="col-md-4">
										<div class="form-group">
											<h5>Trail Position <span class="text-danger">*</span></h5>
											<div class="controls">
												<input type="text" name="TrialPos" class="form-control"  value="" required data-validation-required-message="This field is required" placeholder="Enter Trail Position"> 
											</div>
										</div>
									</div>
									<div class="col-md-4">
										<div class="text-xs-right">
											<button type="submit" class="btn btn-success">Submit</button>
											<a  href="<?php echo base_url()?>Accounttype_controller" class="btn btn-info">Cancel</a>
										</div>
									</div>
								</div>
							</form>
						</div>
						<?php
						if(!empty($editaccountdata))
						{
							?>
							<!-- <input type="hidden" value="inside" id="inside"> -->
							
							<div class="tab-pane  p-20" id="editform" role="tabpanel">
								<form action="" class="m-t-40" method="post" name="editaccountform" id="editaccountform" novalidate>

								<input type="hidden" id="accid" name="accid" value="<?=$editaccountdata['AccNo']?>">

								<div class="row">
									<div class="col-md-4">
										<div class="form-group">
											<h5>Account Type <span class="text-danger">*</span></h5>
											<div class="controls">
												<input type="text" name="AccType" class="form-control"  value="<?=$editaccountdata['AccType']?>" required data-validation-required-message="This field is required" placeholder="Enter Account Type"> 
											</div>
										</div>
									</div>
									<div class="col-md-4">

										<div class="form-group field">
											<h5>In P&L <span class="text-danger">*</span></h5>
											<div class="controls">
												<select name="inpl" id="inpl" required="" class="form-control" aria-invalid="false">
													<option value=""> --Select In P&L-- </option>

													<option <?php if($editaccountdata['In_PL'] == 'Yes'){echo "selected";}?>
													 value="Yes"> Yes </option>
													<option <?php if($editaccountdata['In_PL'] == 'No'){echo "selected";}?> value="No"> No </option>
												</select>
											<div class="help-block"></div></div>
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group field">
											<h5>In Tranding <span class="text-danger">*</span></h5>
											<div class="controls">
												<select name="intranding" id="intranding" required="" class="form-control" aria-invalid="false">
													<option value=""> --Select In Tranding-- </option>
													<option <?php if($editaccountdata['In_Tranding'] == 'Yes'){echo "selected";}?> value="Yes"> Yes </option>
													<option <?php if($editaccountdata['In_Tranding'] == 'No'){echo "selected";}?> value="No"> No </option>
												</select>
											<div class="help-block"></div></div>
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group field">
											<h5>Balance Sheet <span class="text-danger">*</span></h5>
											<div class="controls">
												<select name="BalSheet" id="BalSheet" required="" class="form-control" aria-invalid="false">
													<option value=""> --Select Balance Sheet-- </option>
													<option <?php if($editaccountdata['BalSheet'] == 'Yes'){echo "selected";}?> value="Yes"> Yes </option>
													<option <?php if($editaccountdata['BalSheet'] == 'No'){echo "selected";}?> value="No"> No </option>
												</select>
											<div class="help-block"></div></div>
										</div>
									</div>

									<div class="col-md-4">
										<div class="form-group field">
											<h5>Trail Balance Side <span class="text-danger">*</span></h5>
											<div class="controls">
												<select name="TRIALside" id="TRIALside" required="" class="form-control" aria-invalid="false">
													<option value=""> --Select Trail Balance Side-- </option>
													<option <?php if($editaccountdata['TRIALside'] == 'Credit'){echo "selected";}?> value="Credit"> Credit </option>
													<option <?php if($editaccountdata['TRIALside'] == 'Debit'){echo "selected";}?> value="Debit"> Debit </option>
												</select>
											<div class="help-block"></div></div>
										</div>

									</div>
									<div class="col-md-4">
										<div class="form-group">
											<h5>Trail Position <span class="text-danger">*</span></h5>
											<div class="controls">
												<input type="text" name="TrialPos" class="form-control"  value="<?=$editaccountdata['TrialPos']?>" required data-validation-required-message="This field is required" placeholder="Enter Trail Position"> 
											</div>
										</div>
									</div>
									<div class="col-md-4">
										<div class="text-xs-right">
											<button type="submit" class="btn btn-success">Submit</button>
											<a  href="<?php echo base_url()?>Accounttype_controller" class="btn btn-info">Cancel</a>
										</div>
									</div>
								</div>
							</form>
							</div>
							<?php
						}
						?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
 <?php $this->load->view('common/footer'); ?>
  <script>
  $( document ).ready(function() {
	$( ".txtName" ).keypress(function(e) {
		var key = e.keyCode;
		if (key >= 48 && key <= 57) {
			e.preventDefault();
		}
	});
});

var inside = $("#accid").val();

if(inside != "")
{
	$("#home7").removeClass('active');
	$(".nav-link").removeClass('active');
	$(".foractive").addClass('active');
	$("#editform").addClass('active');
}

 </script>
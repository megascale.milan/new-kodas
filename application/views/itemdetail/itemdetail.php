
<?php $this->load->view('common/header'); ?>
<style>
    #success_message{ display: none;}
	.form-group{
	    margin-bottom: 10px!important;
	}
	.form-control
	{
		    min-height: 30px!important;
	}
	
	/* change css form design */	
	html body .p-20 {
		padding: 20px 0!important;
	}
	select.form-control:not([size]):not([multiple]) {
		height: calc(1.0625rem + 2px);
	}
</style>

<div class="page-wrapper">
    <div class="container-fluid">
	
	
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h4 class="text-themecolor">Item Detail Master</h4>
			</div>
            <div class="col-md-7 align-self-center text-right">
                <div class="d-flex justify-content-end align-items-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Master</a></li>
                        <li class="breadcrumb-item active">Item Detail Master</li>
                    </ol>
					<a href="javascript:fun_multipleDelete();" class="btn btn-info d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i> All Delete</a>
                </div>
            </div>
        </div>
		
		
		<div class="row">
			<div class="card" style="width: 100%;">
				<div class="card-body p-b-0">
				
					<!-- Nav tabs -->
					<ul class="nav nav-tabs customtab2" role="tablist">
						<li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#home7" role="tab"><span class="hidden-sm-up"><i class="ti-home"></i></span> <span class="hidden-xs-down">Item Detail List</span></a> </li>
						<li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#profile7" role="tab"><span class="hidden-sm-up"><i class="ti-user"></i></span> <span class="hidden-xs-down">Add Form</span></a> </li>
					</ul>
					<!-- Tab panes -->
					<div class="tab-content">
						<div class="tab-pane active" id="home7" role="tabpanel">
							<div class="p-20">
							<form  name="frm_itemdetails" method="post" action="<?php echo base_url();?>Broker_controller/index">
							<input type="hidden" value="" name="brokerID" />
							 <input type="hidden" name="method" value="" /> 
								<div class="table-responsive">
                                    <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th><input type="checkbox" name="checkall" id= "checkall" value=""  /> Checkbox</th>
                                                <th>Name</th>
                                                <th>Cut</th>
                                                <th>Contact No.</th>
                                                <th>CreateDate</th>
                                                <th>Is Active</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                           
                                            <tr id="">
                                            	<?php
                                            	if(sizeof($item_detail) > 0)
                                            	{
                                            		foreach ($item_detail as $value)
                                            		{
                                            			?>
                                                		<td><?=$value->Name;?></td>
                                                		<td><?=$value->Cut;?></td>
                                            			<?php
                                            		}
                                            	}
                                            	?>
                                               
                                                <td>surat</td>
                                                <td>8080114457</td>
                                                <td>2018-12-08</td>
                                               <td>
												<a href="javascript:fun_single_status();">
													<span id="">
														<?php echo"Active"; ?>
													</span>
												</a>
                                                </td>
                                                <td class="editdelaction">
                                                    <a href="javascript:fun_edit();" ><i class="fa fa-edit"></i></a>&nbsp;&nbsp;
                                                    
                                                    <a href="javascript:fun_single_delete();"><i class="fa fa-trash-o"></i></a>
                                                </td>
                                            </tr>
                                           
                                        </tbody>
                                    </table>
                                </div>
								</form>
								</div>
						</div>
						<div class="tab-pane  p-20" id="profile7" role="tabpanel">
							
								<form action="" class="" method="post" name="additemdetailform" id="additemdetailform" novalidate>
									<div class="row common_master_form_div">
										<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 ">
											
											
											<div class="formtitle">
												<h4 class="backcolor">Account Information</h4>
												<div class="row removemargin">
													<div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
														<div class="row">
															<div class="col-12 col-sm-4 col-md-2 col-lg-2 col-xl-2 d-flex ">
																<div class="form-group">
																	<label>CODE<span class="fored"><b>*</b></span> :</label>
																</div>
															</div>
															<div class="col-12 col-sm-8 col-md-4 col-lg-4 col-xl-4">
																<div class="form-group field">
																	<div class="controls">
																		<input type="text" name="code" id="code"  class="form-control" placeholder="ENTER CODE"  required data-validation-required-message="This field is required"> 
																	</div>
																</div>
															</div>
															
															<div class="col-12 col-sm-4 col-md-2 col-lg-2 col-xl-2 d-flex ">
																<div class="form-group">
																	<label>ITEM SRNO.<span class="fored"><b>*</b></span> :</label>
																</div>
															</div>
															<div class="col-12 col-sm-8 col-md-4 col-lg-4 col-xl-4">
																<div class="form-group field">
																	<div class="controls">
																		<input type="text" name="itemsrno" id="itemsrno" class="form-control" placeholder="ENTER ITEM SRNO."  required data-validation-required-message="This field is required"> 
																	</div>
																</div>
															</div>
															
															<div class="col-12 col-sm-4 col-md-2 col-lg-2 col-xl-2 d-flex ">
																<div class="form-group">
																	<label>NAME<span class="fored"><b>*</b></span> :</label>
																</div>
															</div>
															<div class="col-12 col-sm-8 col-md-10 col-lg-10 col-xl-10">
																<div class="form-group field">
																	<div class="controls">
																		<input type="text" name="name" class="form-control txtName" id="name" placeholder="ENTER NAME"  required data-validation-required-message="This field is required"> 
																	</div>
																</div>
															</div>
															
															
															<div class="col-12 col-sm-4 col-md-2 col-lg-2 col-xl-2 d-flex ">
																<div class="form-group">
																	<label>MAIN SCREEN <span class="fored"><b>*</b></span> :</label>
																</div>
															</div>
															<div class="col-12 col-sm-8 col-md-10 col-lg-10 col-xl-10">
																<div class="form-group field">
																	<div class="controls">
																		<select name="mainscreen" id="mainscreen" required class="form-control">
																			<option value=""> --Select -- </option>
																			<?php
																			foreach ($screenregisterentry as $screenvalue)
																			{
																				?>
																				<option value="<?=$screenvalue->ScreenRegisterID?>"><?=$screenvalue->Cut?></option>
																				<?php
																			}
																			?>
																		</select>
																	</div>
																</div>
															</div>
															
															<div class="col-12 col-sm-4 col-md-2 col-lg-2 col-xl-2 d-flex ">
																<div class="form-group">
																	<label>CUT<span class="fored"><b>*</b></span> :</label>
																</div>
															</div>
															<div class="col-12 col-sm-8 col-md-4 col-lg-4 col-xl-4">
																<div class="form-group field" >
																	<div class="controls">
																		<input type="text" name="cut" id="cut" class="form-control" placeholder="0.00"  required data-validation-required-message="This field is required"> 
																	</div>
																</div>
															</div>
															
															<div class="col-12 col-sm-4 col-md-2 col-lg-2 col-xl-2 d-flex ">
																<div class="form-group">
																	<label>PACKING<span class="fored"><b>*</b></span> :</label>
																</div>
															</div>
															<div class="col-12 col-sm-8 col-md-4 col-lg-4 col-xl-4">
																<div class="form-group field" >
																	<div class="controls">
																		<select name="packing" id="packing" required class="form-control">
																			<option value=""> --Select -- </option>
																			<?php
																			foreach ($packagestyledata as $packagestylevalue)
																			{
																				?>
																				<option value="<?=$packagestylevalue->PackagestyleID;?>"><?=$packagestylevalue->packing;?></option>
																				<?php	
																			}
																			?>
																		</select>
																	</div>
																</div>
															</div>
															
															<div class="col-12 col-sm-4 col-md-2 col-lg-2 col-xl-2 d-flex ">
																<div class="form-group">
																	<label>GREY QUALITY <span class="fored"><b>*</b></span> :</label>
																</div>
															</div>
															<div class="col-12 col-sm-8 col-md-10 col-lg-10 col-xl-10">
																<div class="form-group field">
																	<div class="controls">
																		<select name="greyquality" id="greyquality" required class="form-control">
																			<option value=""> --Select -- </option>
																			<option value="1"> 1 </option>
																		</select>
																	</div>
																</div>
															</div>
															
															<div class="col-12 col-sm-4 col-md-2 col-lg-2 col-xl-2 d-flex ">
																<div class="form-group">
																	<label>TYPE<span class="fored"><b>*</b></span> :</label>
																</div>
															</div>
															<div class="col-12 col-sm-8 col-md-4 col-lg-4 col-xl-4">
																<div class="form-group field" >
																	<div class="controls">
																		<select name="type" id="type" required class="form-control">
																			<option value=""> --Select -- </option>
																			<option value="FINISH"> FINISH </option>
																			<option value="GRAY"> GRAY </option>
																			<option value="GENRAL"> GENRAL </option>
																		</select>
																	</div>
																</div>
															</div>
															<div class="col-12 col-sm-4 col-md-2 col-lg-2 col-xl-2 d-flex ">
																<div class="form-group">
																	<label>ITEM TYPE<span class="fored"><b>*</b></span> :</label>
																</div>
															</div>
															<div class="col-12 col-sm-8 col-md-4 col-lg-4 col-xl-4">
																<div class="form-group field" >
																	<div class="controls">
																		<select name="itemtype" id="itemtype" required class="form-control">
																			<option value=""> --Select -- </option>
																			<option value="0"> 0 </option>
																			<option value="DRED WORK"> DRED WORK </option>
																			<option value="FINISH"> FINISH </option>
																			<option value="KODUC PRINT"> KODUC PRINT </option>
																			<option value="LADO DHARA"> LADO DHARA </option>
																			<option value="NEGATIVE WORK"> NEGATIVE WORK </option>
																			<option value="PRINT"> PRINT </option>
																			<option value="PRINT WORK"> PRINT WORK </option>
																			<option value="SAREE"> SAREE </option>
																		</select>
																	</div>
																</div>
															</div>
															
															<div class="col-12 col-sm-4 col-md-2 col-lg-2 col-xl-2 d-flex ">
																<div class="form-group">
																	<label>SCREEN SERIE<span class="fored"><b>*</b></span> :</label>
																</div>
															</div>
															<div class="col-12 col-sm-8 col-md-10 col-lg-10 col-xl-10">
																<div class="form-group field">
																	<div class="controls">
																		<select name="screenserie" id="screenserie" required class="form-control">
																			<option value=""> --Select -- </option>
																			<option value="1"> 1 </option>
																		</select>
																	</div>
																</div>
															</div>
															
														</div>
													</div>
													<div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
														<div class="row">
															<div class="col-12 col-sm-4 col-md-2 col-lg-2 col-xl-2 d-flex ">
																<div class="form-group">
																	<label>CATEGORY<span class="fored"><b>*</b></span> :</label>
																</div>
															</div>
															<div class="col-12 col-sm-8 col-md-10 col-lg-10 col-xl-10">
																<div class="form-group field">
																	<div class="controls">
																		<select name="category" id="category" required class="form-control">
																			<option value=""> --Select -- </option>
																			<option value="1"> 1 </option>
																		</select>
																	</div>
																</div>
															</div>
															<div class="col-12 col-sm-4 col-md-2 col-lg-2 col-xl-2 d-flex ">
																<div class="form-group">
																	<label>SELLING RATE<span class="fored"><b>*</b></span> :</label>
																</div>
															</div>
															<div class="col-12 col-sm-8 col-md-4 col-lg-4 col-xl-4">
																<div class="form-group field" >
																	<div class="controls">
																		<input type="text" name="sellingrate" id="sellingrate"  class="form-control" placeholder="0.00"  required data-validation-required-message="This field is required"> 
																	</div>
																</div>
															</div>
															<div class="col-12 col-sm-4 col-md-2 col-lg-2 col-xl-2 d-flex ">
																<div class="form-group">
																	<label>UNIT<span class="fored"><b>*</b></span> :</label>
																</div>
															</div>
															<div class="col-12 col-sm-8 col-md-4 col-lg-4 col-xl-4">
																<div class="form-group field" >
																	<div class="controls">
																		<select name="unit" id="unit" required class="form-control">
																			<option value=""> --Select -- </option>
																			<option value="PCS"> PCS </option>
																			<option value="NTS"> NTS </option>
																			<option value="KAD"> KAD </option>
																			<option value="OTHER"> OTHER </option>
																			<option value="SUIT"> SUIT </option>
																		</select>
																	</div>
																</div>
															</div>
															<div class="col-12 col-sm-4 col-md-2 col-lg-2 col-xl-2 d-flex ">
																<div class="form-group">
																	<label>RATE 2<span class="fored"><b>*</b></span> :</label>
																</div>
															</div>
															<div class="col-12 col-sm-8 col-md-4 col-lg-4 col-xl-4">
																<div class="form-group field"  >
																	<div class="controls">
																		<input type="text" name="rete2" id="rete2" class="form-control" placeholder="0.00"  required data-validation-required-message="This field is required"> 
																	</div>
																</div>
															</div>
															
															<div class="col-12 col-sm-4 col-md-2 col-lg-2 col-xl-2 d-flex ">
																<div class="form-group">
																	<label>RATE 3<span class="fored"><b>*</b></span> :</label>
																</div>
															</div>
															<div class="col-12 col-sm-8 col-md-4 col-lg-4 col-xl-4">
																<div class="form-group field">
																	<div class="controls">
																		<input type="text" name="rate3" id="rate3" class="form-control" placeholder="0.00"  required data-validation-required-message="This field is required"> 
																	</div>
																</div>
															</div>
															
															<div class="col-12 col-sm-4 col-md-2 col-lg-2 col-xl-2 d-flex ">
																<div class="form-group">
																	<label>SELECTED <span class="fored"><b>*</b></span> :</label>
																</div>
															</div>
															<div class="col-12 col-sm-8 col-md-4 col-lg-4 col-xl-4">
																<div class="form-group field">
																	<div class="controls">
																		<input type="checkbox" id="selected" name="selected" value="1"  name="isActive" > Selected
																	</div>
																</div>
															</div>

															<div class="col-12 col-sm-4 col-md-2 col-lg-2 col-xl-2 d-flex ">
																<div class="form-group">
																	<label>WORK CUT<span class="fored"><b>*</b></span> :</label>
																</div>
															</div>
															<div class="col-12 col-sm-8 col-md-4 col-lg-4 col-xl-4">
																<div class="form-group field">
																	<div class="controls">
																		<input type="text" name="workcut" id="workcut" class="form-control" placeholder="ENTER WORK CUT"  required data-validation-required-message="This field is required"> 
																	</div>
																</div>
															</div>
															
															<div class="col-12 col-sm-4 col-md-3 col-lg-3 col-xl-3 d-flex">
																<div class="form-group">
																	<label>PACK+CUT+BOX COST<span class="fored"><b>*</b></span> :</label>
																</div>
															</div>
															<div class="col-12 col-sm-8 col-md-9 col-lg-9 col-xl-9">
																<div class="form-group field">
																	<div class="controls">
																		<input type="text"  name="packcutboxcost" id="packcutboxcost" placeholder="0.00" class="form-control" required data-validation-required-message="This field is required"> 
																	</div>
																</div>
															</div>
															
															<div class="col-12 col-sm-4 col-md-3 col-lg-3 col-xl-3 d-flex">
																<div class="form-group">
																	<label>SALE RATE MU%<span class="fored"><b>*</b></span> :</label>
																</div>
															</div>
															<div class="col-12 col-sm-8 col-md-9 col-lg-9 col-xl-9">
																<div class="form-group field">
																	<div class="controls">
																		<input type="text" name="saleratemu" id="saleratemu" placeholder="ENTER SALE RATE MU%" class="form-control" required data-validation-required-message="This field is required"> 
																	</div>
																</div>
															</div>
															<div class="col-12 col-sm-4 col-md-2 col-lg-2 col-xl-2 d-flex ">
																<div class="form-group">
																	<label>HSN/SAC<span class="fored"><b>*</b></span> :</label>
																</div>
															</div>
															<div class="col-12 col-sm-8 col-md-4 col-lg-4 col-xl-4">
																<div class="form-group field"  >
																	<div class="controls">
																		<input type="text" name="hsn" id="hsn" class="form-control" placeholder="ENTER HSN/SAC"  required data-validation-required-message="This field is required"> 
																	</div>
																</div>
															</div>
															
															<div class="col-12 col-sm-4 col-md-2 col-lg-2 col-xl-2 d-flex ">
																<div class="form-group">
																	<label>GST %<span class="fored"><b>*</b></span> :</label>
																</div>
															</div>
															<div class="col-12 col-sm-8 col-md-4 col-lg-4 col-xl-4">
																<div class="form-group field"  >
																	<div class="controls">
																		<input type="text" name="gst" id="gst" class="form-control" placeholder="0.00"  required data-validation-required-message="This field is required"> 
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>	
											</div>	
											<div class="row">
												<div class="form-group">
													<div class="text-xs-right" style="margin-left: 8px;">
														<button type="submit" class="btn btn-info">Submit</button>
													</div>
												</div>
											</div>
									
										</div>	
									</div>
								</form>
								
						</div>
					</div>
				</div>
			</div>
		</div>
       
    </div>
</div>
 <?php $this->load->view('common/footer'); ?>
<script>
	 $(function() {
	  $('#staticParent').on('keydown', '#child', function(e){-1!==$.inArray(e.keyCode,[46,8,9,27,13,110])||(/65|67|86|88/.test(e.keyCode)&&(e.ctrlKey===true||e.metaKey===true))&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()});
	})
	$(function() {
	  $('#staticParent1').on('keydown', '#child1', function(e){-1!==$.inArray(e.keyCode,[46,8,9,27,13,110])||(/65|67|86|88/.test(e.keyCode)&&(e.ctrlKey===true||e.metaKey===true))&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()});
	})
	$(function() {
	  $('#staticParent2').on('keydown', '#child2', function(e){-1!==$.inArray(e.keyCode,[46,8,9,27,13,110])||(/65|67|86|88/.test(e.keyCode)&&(e.ctrlKey===true||e.metaKey===true))&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()});
	})
	  $( document ).ready(function() {
	$( ".txtName" ).keypress(function(e) {
		var key = e.keyCode;
		if (key >= 48 && key <= 57) {
			e.preventDefault();
		}
	});
});

</script>

 
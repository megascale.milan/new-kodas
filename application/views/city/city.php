<?php
$cityID=$this->input->post('cityID')?$this->input->post('cityID'):'';
?>

<?php $this->load->view('common/header'); ?>
<?php $this->load->view('city/city-js');?>
<style>
    #success_message{ display: none;}
</style>
<div class="page-wrapper">
    <div class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h4 class="text-themecolor">City Master</h4>
            </div>
            <div class="col-md-7 align-self-center text-right">
                <div class="d-flex justify-content-end align-items-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Master</a></li>
                        <li class="breadcrumb-item active">City Master</li>
                    </ol>
                    <button type="button" class="btn btn-info d-none d-lg-block m-l-15" data-toggle="modal" data-target="#exampleModal" data-whatever="@getbootstrap"><i class="fa fa-plus-circle"></i> Create New</button>
					<a href="javascript:fun_multipleDelete();" class="btn btn-info d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i> All Delete</a>	
					<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title" id="exampleModalLabel1">City Master</h4>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                </div>
                                <div class="modal-body">
                                    <form name="frm_state_add" id="frm_state_add"  action="<?php echo base_url();?>City_controller/savecity" method="post" class=" error" novalidate>
                                        <div class="form-group">
                                        <h5 style="text-align: left;">Select State <span class="text-danger">*</span></h5>
                                        <div class="controls" style="text-align: left;">
                                            <select name="stateID" id="stateID" required class="form-control">
                                                <option value="">Select Your State</option>
                                                <?php 
												foreach($State as $statedata)
												{
												?>
												<option value="<?php echo $statedata->stateID; ?>"><?php echo $statedata->stateName; ?></option>
												<?php 
												}
												?>
                                            </select>
                                        </div>
                                    </div>
										<div class="form-group">
                                            <h5 style="text-align: left;">Enter City <span class="text-danger">*</span></h5>
                                            <div class="controls" style="text-align: left;">
                                                <input type="text" name="cityName" class="form-control" required data-validation-required-message="This field is required"> 
                                            </div>
                                        </div>

                                        <div class="form-group" style="float: left;">
                                              <input type="checkbox" value="1"  name="isActive" > isActive?
                                        </div>
                                        <div style="clear:both"></div>
                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-info">Submit</button>
                                        </div>
                                    </form>
                                    
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
       <form  name="frm_city_list" method="post" action="<?php echo base_url();?>City_controller/index">
            <input type="hidden" value="<?php echo $cityID; ?>" name="cityID" />
            <input type="hidden" name="method" value="" />
         <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <!-- <h4 class="card-title">Data Export</h4>
                                <h6 class="card-subtitle">Export data to Copy, CSV, Excel, PDF & Print</h6> -->
                                <div class="table-responsive m-t-40">
                                    <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th><input type="checkbox" name="checkall" id= "checkall" value=""  /> Checkbox</th>
                                                <th>State Name</th>
												<th>City Name</th>
                                                <th>Is Active</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                       
                                        <tbody>
                                            <?php 
                                            foreach($displayCity as $displayCitydata)
                                            {
                                            ?>
                                            <tr id="ID_<?php echo $displayCitydata->cityID; ?>">
                                                <td><input class="checkbox" name="checkUncheck[]"  id="checkAllAuto" value="<?php echo $displayCitydata->cityID; ?>" type="checkbox" /></td>
                                                <td><?php echo $displayCitydata->stateName; ?></td>
                                                <td><?php echo $displayCitydata->cityName; ?></td>
                                                <td>
													<a href="javascript:fun_single_status(<?php echo $displayCitydata->cityID; ?>);">
														<span id="city_<?php echo $displayCitydata->cityID; ?>">
															<?php if($displayCitydata->isActive==1){ echo"Active"; }else{ echo"Inactive";} ?>
														</span>
													</a>
                                                </td>
                                                <td class="editdelaction">
                                                    <a href="javascript:fun_edit(<?php echo 
                                                    $displayCitydata->cityID; ?>);" " ><i class="fa fa-edit"></i></a>&nbsp;&nbsp;
                                                    
                                                    <a href="javascript:fun_single_delete(<?php echo $displayCitydata->cityID; ?>);"><i class="fa fa-trash-o"></i></a>
                                                </td>
                                            </tr>
                                            <?php 
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        

                    </div>
                </div>
               </form>
    </div>
</div>
 <?php $this->load->view('common/footer'); ?>

 
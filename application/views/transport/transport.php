
<?php 
$transportID=$this->input->post('transportID')?$this->input->post('transportID'):'0';
$transportName=$this->input->post('transportName')?$this->input->post('transportName'):'';
$Taddress=$this->input->post('Taddress')?$this->input->post('Taddress'):'';
$Tphone1=$this->input->post('Tphone1')?$this->input->post('Tphone1'):'';
$Tphone2=$this->input->post('Tphone2')?$this->input->post('Tphone2'):'';
$Tmobile=$this->input->post('Tmobile')?$this->input->post('Tmobile'):'';
$TEway=$this->input->post('TEway')?$this->input->post('TEway'):'';
$Tmode=$this->input->post('Tmode')?$this->input->post('Tmode'):'';
$isActive=$this->input->post('isActive')?$this->input->post('isActive'):'';

if($transportID != NULL && $transportID >0)
{
	if(isset($getsingletransport))
	{
		foreach($getsingletransport as $getsingletransportdata)
		{
			$transportName=$getsingletransportdata[0]->transportName;
			$Taddress=$getsingletransportdata[0]->Taddress;
			$Tphone1=$getsingletransportdata[0]->Tphone1;
			$Tphone2=$getsingletransportdata[0]->Tphone2;
			$Tmobile=$getsingletransportdata[0]->Tmobile;
			$TEway=$getsingletransportdata[0]->TEway;
			$Tmode=$getsingletransportdata[0]->Tmode;
			$isActive=$getsingletransportdata[0]->isActive;
		}
	}
}
?>
<?php $this->load->view('common/header'); ?>
<?php $this->load->view('transport/transport-js');?>
<style>
    #success_message{ display: none;}
</style>

<div class="page-wrapper">
    <div class="container-fluid">
	
	
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h4 class="text-themecolor">Transport Master</h4>
				
            </div>
            <div class="col-md-7 align-self-center text-right">
                <div class="d-flex justify-content-end align-items-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Master</a></li>
                        <li class="breadcrumb-item active">Transport Master</li>
                    </ol>
					<!--<a href="javascript:fun_edit(0);" class="btn btn-info d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i> Create New</a>-->
                    <a href="javascript:fun_multipleDelete();" class="btn btn-info d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i> All Delete</a>
                </div>
            </div>
        </div>
		
		
		<div class="row">
			<div class="card" style="width: 100%;">
				<div class="card-body p-b-0">
				
					<!-- Nav tabs -->
					<ul class="nav nav-tabs customtab2" role="tablist">
						<li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#home7" role="tab"><span class="hidden-sm-up"><i class="ti-home"></i></span> <span class="hidden-xs-down">Market List</span></a> </li>
						<li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#profile7" role="tab"><span class="hidden-sm-up"><i class="ti-user"></i></span> <span class="hidden-xs-down">Add Form</span></a> </li>
					</ul>
					<!-- Tab panes -->
					<div class="tab-content">
						<div class="tab-pane active" id="home7" role="tabpanel">
							<div class="p-20">
							<form  name="frm_transport_list" method="post" action="<?php echo base_url();?>Market_controller/index">
							<input type="hidden" value="<?php echo $transportID; ?>" name="transportID" />
							 <input type="hidden" name="method" value="" /> 
								<div class="table-responsive">
                                    <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th><input type="checkbox" name="checkall" id= "checkall" value=""  /> Checkbox</th>
                                                <th>Transport Name</th>
                                                <th>Address</th>
                                                <th>Phone 1</th>
                                                <th>Phone 2</th>
                                                <th>Mobile</th>
                                                <th>Eway GSTIN/ID</th>
                                                <th>Mode</th>
                                                <th>Date</th>
                                                <th>Is Active</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php 
                                            foreach($getalltransport as $getalltransportdata)
                                            {
                                            ?>
                                            <tr id="ID_<?php echo $getalltransportdata->transportID; ?>">
                                                <td><input class="checkbox" name="checkUncheck[]"  id="checkAllAuto" value="<?php echo $getalltransportdata->transportID; ?>" type="checkbox" /></td>
                                                <td><?php echo $getalltransportdata->transportName; ?></td>
												<td><?php echo $getalltransportdata->Taddress; ?></td>
												
                                                <td><?php echo $getalltransportdata->Tphone1; ?></td>
                                                <td><?php echo $getalltransportdata->Tphone2; ?></td>
                                                <td><?php echo $getalltransportdata->Tmobile; ?></td>
                                                <td><?php echo $getalltransportdata->TEway; ?></td>
                                                <td><?php echo $getalltransportdata->Tmode; ?></td>
                                                
                                                <td><?php echo $getalltransportdata->createDate; ?></td>
                                               <td>
		<a href="javascript:fun_single_status(<?php echo $getalltransportdata->transportID; ?>);">
			<span id="transport_<?php echo $getalltransportdata->transportID; ?>">
				<?php if($getalltransportdata->isActive==1){ echo"Active"; }else{ echo"Inactive";} ?>
			</span>
		</a>
                                                </td>
                                                <td class="editdelaction">
                                                    <a href="javascript:fun_edit(<?php echo 
                                                    $getalltransportdata->transportID; ?>);" ><i class="fa fa-edit"></i></a>&nbsp;&nbsp;
                                                    
                                                    <a href="javascript:fun_single_delete(<?php echo $getalltransportdata->transportID; ?>);"><i class="fa fa-trash-o"></i></a>
                                                </td>
                                            </tr>
                                            <?php 
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
								</form>
								</div>
						</div>
						<div class="tab-pane  p-20" id="profile7" role="tabpanel">
								<form action="<?php echo base_url() ?>Transport_controller/saveuser" class="m-t-40" method="post" name="addform" novalidate>
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
												<h5>Enter Transport Name <span class="text-danger">*</span></h5>
												<div class="controls">
													<input type="text" value="<?php echo $transportName; ?>" class="form-control txtName" id="txtName"  name="transportName" class="form-control"    required data-validation-required-message="This field is required"> 
													<input type="hidden" value="<?php echo $transportID; ?>" name="transportID" >
												</div>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<h5>Enter Transport Address <span class="text-danger">*</span></h5>
												<div class="controls">
													<input type="text" name="Taddress" value="<?php echo $Taddress; ?>" class="form-control"    required data-validation-required-message="This field is required"> 
												</div>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<h5>Enter Phone 1<span class="text-danger">*</span></h5>
												<div class="controls" id="staticParent">
													<input type="text" name="Tphone1" id="child" maxlength="10" value="<?php echo $Tphone1; ?>" class="form-control"    required data-validation-required-message="This field is required"> 
												</div>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<h5>Enter Phone 2<span class="text-danger">*</span></h5>
												<div class="controls" id="staticParent1">
													<input type="text" name="Tphone2" id="child1" maxlength="10" value="<?php echo $Tphone2; ?>" class="form-control"    required data-validation-required-message="This field is required"> 
												</div>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<h5>Enter Mobile<span class="text-danger">*</span></h5>
												<div class="controls" id="staticParent2">
													<input type="text" name="Tmobile" id="child2" maxlength="10" value="<?php echo $Tmobile; ?>" class="form-control"    required data-validation-required-message="This field is required"> 
												</div>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<h5>Enter TEway GSTIN/ID<span class="text-danger">*</span></h5>
												<div class="controls">
													<input type="text" name="TEway" value="<?php echo $TEway; ?>" class="form-control"    required data-validation-required-message="This field is required"> 
												</div>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<h5>Enter Mode<span class="text-danger">*</span></h5>
												<div class="controls">
													 <select name="Tmode" id="Tmode" required class="form-control">
													 	<option value="<?php echo $Tmode; ?>"><?php echo $Tmode; ?></option>
		                                                <option value="">Select Mode</option>
		                                               	<option value="1">ROAD</option>
														<option value="2">RAIL</option>
														<option value="3">AIR</option>
														<option value="4">SHIP</option>
														
		                                            </select>
													
												</div>
											</div>
										</div>
										
										<div class="col-md-12">
											 <div class="form-group" >
	                                              <input type="checkbox" <?php if($isActive==1){ echo "checked='checked'";} ?>  value="1"  name="isActive" > isActive?
	                                        </div>
	                                    </div>	
	                                    <div class="form-group">
	                                        <div class="text-xs-right">
											   <button type="submit" class="btn btn-info">Submit</button>
											</div>
										</div>
								</div>	
								</form>
						
						</div>
					</div>
				</div>
			</div>
		</div>

            
				
          


       
    </div>
</div>
 <?php $this->load->view('common/footer'); ?>
 <script>
 $(function() {
  $('#staticParent').on('keydown', '#child', function(e){-1!==$.inArray(e.keyCode,[46,8,9,27,13,110])||(/65|67|86|88/.test(e.keyCode)&&(e.ctrlKey===true||e.metaKey===true))&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()});
})
$(function() {
  $('#staticParent1').on('keydown', '#child1', function(e){-1!==$.inArray(e.keyCode,[46,8,9,27,13,110])||(/65|67|86|88/.test(e.keyCode)&&(e.ctrlKey===true||e.metaKey===true))&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()});
})
$(function() {
  $('#staticParent2').on('keydown', '#child2', function(e){-1!==$.inArray(e.keyCode,[46,8,9,27,13,110])||(/65|67|86|88/.test(e.keyCode)&&(e.ctrlKey===true||e.metaKey===true))&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()});
})

 $( document ).ready(function() {
	$( ".txtName" ).keypress(function(e) {
		var key = e.keyCode;
		if (key >= 48 && key <= 57) {
			e.preventDefault();
		}
	});
});
 </script>
 

<?php $this->load->view('common/header'); ?>
<style>
    #success_message{ display: none;}
	.form-group{
	    margin-bottom: 10px!important;
	}
	.form-control
	{
		    min-height: 30px!important;
	}
	
	/* change css form design */	
	html body .p-20 {
		padding: 20px 0!important;
	}
	select.form-control:not([size]):not([multiple]) {
		height: calc(1.0625rem + 2px);
	}
</style>

<div class="page-wrapper">
    <div class="container-fluid">
	
	
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h4 class="text-themecolor">Remark Master</h4>
			</div>
            <div class="col-md-7 align-self-center text-right">
                <div class="d-flex justify-content-end align-items-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Master</a></li>
                        <li class="breadcrumb-item active">Remark Master</li>
                    </ol>
					<a href="javascript:fun_multipleDelete();" class="btn btn-info d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i> All Delete</a>
                </div>
            </div>
        </div>
		
		
		<div class="row">
			<div class="card" style="width: 100%;">
				<div class="card-body p-b-0">
				
					<!-- Nav tabs -->
					<ul class="nav nav-tabs customtab2" role="tablist">
						<li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#home7" role="tab"><span class="hidden-sm-up"><i class="ti-home"></i></span> <span class="hidden-xs-down">Remark List</span></a> </li>
						<li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#profile7" role="tab"><span class="hidden-sm-up"><i class="ti-user"></i></span> <span class="hidden-xs-down">Add Form</span></a> </li>
					</ul>
					<!-- Tab panes -->
					<div class="tab-content">
						<div class="tab-pane active" id="home7" role="tabpanel">
								<div class="p-20">
							<form  name="frm_broker_list" method="post" action="<?php echo base_url();?>Broker_controller/index">
							<input type="hidden" value="" name="brokerID" />
							 <input type="hidden" name="method" value="" /> 
								<div class="table-responsive">
                                    <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th><input type="checkbox" name="checkall" id= "checkall" value=""  /> Checkbox</th>
                                                <th> Name</th>
                                                <th>Address</th>
                                                <th>Contact No.</th>
                                                <th>CreateDate</th>
                                                <th>Is Active</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                           
                                            <tr id="">
                                                <td><input class="checkbox" name="checkUncheck[]"  id="checkAllAuto" value="" type="checkbox" /></td>
                                                <td>DIVYESH</td>
                                                <td>surat</td>
                                                <td>8080114457</td>
                                                <td>2018-12-08</td>
                                               <td>
												<a href="javascript:fun_single_status();">
													<span id="">
														<?php echo"Active"; ?>
													</span>
												</a>
                                                </td>
                                                <td class="editdelaction">
                                                    <a href="javascript:fun_edit();" ><i class="fa fa-edit"></i></a>&nbsp;&nbsp;
                                                    
                                                    <a href="javascript:fun_single_delete();"><i class="fa fa-trash-o"></i></a>
                                                </td>
                                            </tr>
                                           
                                        </tbody>
                                    </table>
                                </div>
								</form>
								</div>
						</div>
						<div class="tab-pane  p-20" id="profile7" role="tabpanel">
							<form action="<?php echo base_url() ?>Itemtype_controller/saveuser" class="m-t-40" method="post" name="addform" novalidate>
								<div class="row">
									<div class="col-md-4">
										<div class="row">
											<div class="col-md-12">
												<div class="form-group">
													<h5>REMARK  <span class="text-danger">*</span></h5>
													<div class="controls">
														<input type="text" name="" class="form-control"   required data-validation-required-message="This field is required"> 
													</div>
												</div>
											</div>
											<div class="col-md-12">
												<div class="form-group">
													<h5>REMARK1 TYPE  <span class="text-danger">*</span></h5>
													<div class="controls">
														<input type="text" name="" class="form-control"    required data-validation-required-message="This field is required"> 
														<input type="hidden" name="AccNo">
													</div>
												</div>
											</div>
											<div class="col-md-12">
												<div class="form-group">
													<h5>RMK TYPE <span class="text-danger">*</span></h5>
													<div class="controls">
														<input type="text" name="Letter" class="form-control"   required data-validation-required-message="This field is required"> 
													</div>
												</div>
											</div>
											
											<div class="col-md-12">
												<div class="form-group" >
													  <input type="checkbox" value="1" name="isActive" > isActive?
												</div>
											</div>
											<div class="col-md-12">
												<div class="text-xs-right">
													<button type="submit" class="btn btn-info">Submit</button>
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-8"></div>
								</div>
								
							</form>
								
								
						</div>
					</div>
				</div>
			</div>
		</div>

            
				
          


       
    </div>
</div>
 <?php $this->load->view('common/footer'); ?>


 
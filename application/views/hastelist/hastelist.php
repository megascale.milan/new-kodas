
<?php $this->load->view('common/header'); ?>
<style>
#success_message{ display: none;}
.form-group{
margin-bottom: 10px!important;
}
.form-control
{
min-height: 30px!important;
}

/* change css form design */	
html body .p-20 {
padding: 20px 0!important;
}
select.form-control:not([size]):not([multiple]) {
height: calc(1.0625rem + 2px);
}
</style>

<div class="page-wrapper">
<div class="container-fluid">


<div class="row page-titles">
<div class="col-md-5 align-self-center">
<h4 class="text-themecolor">Haste List Master</h4>
</div>
<div class="col-md-7 align-self-center text-right">
<div class="d-flex justify-content-end align-items-center">
<ol class="breadcrumb">
<li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
<li class="breadcrumb-item"><a href="javascript:void(0)">Master</a></li>
<li class="breadcrumb-item active">Haste List Master</li>
</ol>
<a href="javascript:fun_multipleDelete();" class="btn btn-info d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i> All Delete</a>
</div>
</div>
</div>


<div class="row">
<div class="card" style="width: 100%;">
<div class="card-body p-b-0">

<!-- Nav tabs -->
<ul class="nav nav-tabs customtab2" role="tablist">
<li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#home7" role="tab"><span class="hidden-sm-up"><i class="ti-home"></i></span> <span class="hidden-xs-down">Haste List</span></a> </li>
<li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#profile7" role="tab"><span class="hidden-sm-up"><i class="ti-user"></i></span> <span class="hidden-xs-down">Add Form</span></a> </li>

<?php
if(!empty($edithastedata))
{
?>
	<li class="nav-item"> <a class="nav-link foractive" data-toggle="tab" href="#editform" role="tab"><span class="hidden-sm-up"><i class="ti-user"></i></span> <span class="hidden-xs-down">Edit Form</span></a> </li>
<?php
}
?>
</ul>
<!-- Tab panes -->
<div class="tab-content">
<div class="tab-pane active" id="home7" role="tabpanel">
<div class="p-20">
<form  name="frm_broker_list" method="post" action="<?php echo base_url();?>Broker_controller/index">
<input type="hidden" value="" name="brokerID" />
 <input type="hidden" name="method" value="" /> 
	<div class="table-responsive">
        <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th><input type="checkbox" name="checkall" id= "checkall" value=""  /> Checkbox</th>
                    <th>AdatiyaName</th>
                    <th>Haste</th>
                    <th>Transport</th>
                    <th>ScreenSeries</th>
                    <th>GstIn</th>
                    <th>Address1</th>
                    <th>Address2</th>
                    <th>Remark</th>
                    <th>Contact</th>
                    <th>Mobile</th>
                    <th>EmailID</th>
                    <th>Phone1</th>
                    <th>Phone2</th>
                    <th>Fax</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>

            	<?php
            	foreach ($hastedata as $value)
            	{
            	?>
            	<tr id="">
                    <td><input class="checkbox" name="checkUncheck[]"  id="checkAllAuto" value="" type="checkbox" /></td>
                    <td><?=$value->AdatiyaName;?></td>
                    <td><?=$value->Haste;?></td>
                    <td><?=$value->Transport;?></td>
                    <td><?=$value->ScreenSeries;?></td>
                    <td><?=$value->GstIn;?></td>
                    <td><?=$value->Address1;?></td>
                    <td><?=$value->Address2;?></td>
                    <td><?=$value->Remark;?></td>
                    <td><?=$value->Contact;?></td>
                    <td><?=$value->Mobile;?></td>
                    <td><?=$value->EmailID;?></td>
                    <td><?=$value->Phone1;?></td>
                    <td><?=$value->Phone2;?></td>
                    <td><?=$value->Fax;?></td>
                    <td><?php if($value->IsActive == 0){echo "inactive";}else{echo "active";}?></td>
                    <td class="editdelaction">
                        <a href="<?=base_url();?>Hastelist_controller?hasteid=<?=$value->HasteID;?>" ><i class="fa fa-edit"></i></a>&nbsp;&nbsp;
                        
                        <a href="javascript:deletedata('<?=$value->HasteID;?>','hastedelete');"><i class="fa fa-trash-o"></i></a>
                    </td>
                </tr>
            	<?php                                        		
            	}
            	?>

            </tbody>
        </table>
    </div>
	</form>
	</div>
</div>
<div class="tab-pane  p-20" id="profile7" role="tabpanel">

<form action="" class="" method="post" name="addhasteform" id="addhasteform" novalidate>

	<?php
	if(empty($edithastedata))
	{
		?>
			<input type="hidden" value="" id="hasteid" name="hasteid">
		<?php
	}
	?>

	<div class="row common_master_form_div">
		<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 ">
			<div class="formtitle">
				<!-- <div class="row removemargin">
					<div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
						<div class="row" style="margin-top: 2px;margin-bottom: -3px;">
							<div class="col-12 col-sm-4 col-md-3 col-lg-3 col-xl-3 d-flex ">
								<div class="form-group">
									<label>SEARCH ADATIYA <span class="fored"><b>*</b></span> :</label>
								</div>
							</div>
							<div class="col-12 col-sm-8 col-md-9 col-lg-9 col-xl-9">
								<div class="form-group">
									<div class="controls">
										<select name="searchaditya" id="searchaditya" required class="form-control">
											<option value=""> -- Select -- </option>
											<option value="1"> 1 </option>
										</select>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
						<div class="row" style="margin-top: 2px;margin-bottom: -3px;">
							<div class="col-12 col-sm-4 col-md-3 col-lg-3 col-xl-3 d-flex ">
								<div class="form-group">
									<label>SEARCH HASTE <span class="fored"><b>*</b></span> :</label>
								</div>
							</div>
							<div class="col-12 col-sm-8 col-md-9 col-lg-9 col-xl-9">
								<div class="form-group">
									<div class="controls">
										<select name="searchhaste" id="searchhaste" required class="form-control">
											<option value=""> -- Select -- </option>
											<option value="1"> 1 </option>
										</select>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div> -->
			</div>
			
			<div class="formtitle">
				<h4 class="backcolor">Account Information</h4>
				<div class="row removemargin">
					<div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
						<div class="row">
							<div class="col-12 col-sm-4 col-md-2 col-lg-2 col-xl-2 d-flex ">
								<div class="form-group">
									<label>ADATIYA NAME  <span class="fored"><b>*</b></span> :</label>
								</div>
							</div>
							<div class="col-12 col-sm-8 col-md-10 col-lg-10 col-xl-10">
								<div class="form-group field">
									<div class="controls">
										<select name="adatyaname" id="adatyaname" required class="form-control">
											<option value=""> --Select -- </option>
											<?php
											foreach ($ledgernamedata as $ledvalue)
											{
												?>
												<option value="<?=$ledvalue->Ledger_Id;?>"><?=$ledvalue->Name;?></option>
												<?php
											}
											?>
										</select>
									</div>
								</div>
							</div>
							<div class="col-12 col-sm-4 col-md-2 col-lg-2 col-xl-2 d-flex">
								<div class="form-group">
									<label> HASTE <span class="fored"><b>*</b></span> :</label>
								</div>
							</div>
							<div class="col-12 col-sm-8 col-md-10 col-lg-10 col-xl-10">
								<div class="form-group field">
									<div class="controls">
										<input type="text"  name="haste" id="haste" placeholder="ENTER HASTE" class="form-control" required data-validation-required-message="This field is required"> 
									</div>
								</div>
							</div>
							<div class="col-12 col-sm-4 col-md-2 col-lg-2 col-xl-2 d-flex ">
								<div class="form-group">
									<label>TRANSPORT  <span class="fored"><b>*</b></span> :</label>
								</div>
							</div>
							<div class="col-12 col-sm-8 col-md-10 col-lg-10 col-xl-10">
								<div class="form-group field">
									<div class="controls">
										<select name="transport" id="transport" required class="form-control" onchange="transfortcon(this);">
											<option value=""> --Select -- </option>
											<?php
											foreach ($transportdata as $value)
											{
												?>
												<option value="<?=$value->transportID?>"> <?=$value->transportName?> </option>
												<?php
											}
											?>
											
										</select>
									</div>
								</div>
							</div>
							<div class="col-12 col-sm-4 col-md-2 col-lg-2 col-xl-2 d-flex ">
								<div class="form-group">
									<label>STATION<span class="fored"><b>*</b></span> :</label>
								</div>
							</div>
							<div class="col-12 col-sm-8 col-md-10 col-lg-10 col-xl-10">
								<div class="form-group field">
									<div class="controls">
										<!-- <select name="station" id="station" required class="form-control">
											<option value=""> --Select -- </option>
										</select> -->
										<input type="text"  name="station" id="station" placeholder="ENTER HASTE" class="form-control" required data-validation-required-message="This field is required">
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
						<div class="row">
							<div class="col-12 col-sm-4 col-md-2 col-lg-2 col-xl-2 d-flex ">
								<div class="form-group">
									<label>SCREEN SERIES<span class="fored"><b>*</b></span> :</label>
								</div>
							</div>
							<div class="col-12 col-sm-8 col-md-10 col-lg-10 col-xl-10">
								<div class="form-group field">
									<div class="controls">
										<select name="screenseries" id="screenseries" required class="form-control">
											<option value=""> --Select -- </option>

											<?php
											foreach ($screenregisterentry as $value)
											{
												?>
												<option value="<?=$value->ScreenRegisterID;?>"><?=$value->KodasMain;?></option>
												<?php
											}
											?>
										</select>
									</div>
								</div>
							</div>
							<div class="col-12 col-sm-4 col-md-2 col-lg-2 col-xl-2 d-flex">
								<div class="form-group">
									<label>GSTIN<span class="fored"><b>*</b></span> :</label>
								</div>
							</div>
							<div class="col-12 col-sm-8 col-md-10 col-lg-10 col-xl-10">
								<div class="form-group field">
									<div class="controls">
										<input type="text" id="gstin"  name="gstin" placeholder="ENTER GSTIN" class="form-control" required data-validation-required-message="This field is required"> 
									</div>
								</div>
							</div>	
							<div class="col-12 col-sm-4 col-md-2 col-lg-2 col-xl-2 d-flex">
								<div class="form-group">
									<label>ADDRESS 1<span class="fored"><b>*</b></span> :</label>
								</div>
							</div>
							<div class="col-12 col-sm-8 col-md-10 col-lg-10 col-xl-10">
								<div class="form-group field">
									<div class="controls">
										<input type="text"  name="address1" id="address1" placeholder="ENTER ADDRESS 1" class="form-control" required data-validation-required-message="This field is required"> 
									</div>
								</div>
							</div>
							<div class="col-12 col-sm-4 col-md-2 col-lg-2 col-xl-2 d-flex">
								<div class="form-group">
									<label>ADDRESS 2<span class="fored"><b>*</b></span> :</label>
								</div>
							</div>
							<div class="col-12 col-sm-8 col-md-10 col-lg-10 col-xl-10">
								<div class="form-group field">
									<div class="controls">
										<input type="text"  name="address2" id="address2" placeholder="ENTER ADDRESS 2" class="form-control" required data-validation-required-message="This field is required"> 
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>	

			</div>
			
			<div class="formtitle">
				<div class="row removemargin">
					<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
						<div class="row" style="margin-top: 2px;margin-bottom: -3px;">
							<div class="col-12 col-sm-4 col-md-1 col-lg-1 col-xl-1 d-flex ">
								<div class="form-group">
									<label>REMARK <span class="fored"><b>*</b></span> :</label>
								</div>
							</div>
							<div class="col-12 col-sm-8 col-md-11 col-lg-11 col-xl-11">
								<div class="form-group">
									<div class="controls">
										<input type="text" id="remark" name="remark" placeholder="ENTER REMARK" class="form-control" required data-validation-required-message="This field is required"> 
									</div>
								</div>
							</div>
						</div>
					</div>
				
				</div>
			</div>
			
			<div class="formtitle">
				<h4 class="backcolor">CONTACT  Information</h4>
				<div class="row removemargin">
					<div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
						<div class="row">
							<div class="col-12 col-sm-4 col-md-2 col-lg-2 col-xl-2 d-flex ">
								<div class="form-group">
									<label>CONTACT <span class="fored"><b>*</b></span> :</label>
								</div>
							</div>
							<div class="col-12 col-sm-8 col-md-10 col-lg-10 col-xl-10">
								<div class="form-group ">
									<div class="controls">
										<input type="text" name="contactinformation" id="contactinformation" class="form-control" placeholder="ENTER CONTACT" required data-validation-required-message="This field is required"> 
									</div>
								</div>
							</div>
							<div class="col-12 col-sm-4 col-md-2 col-lg-2 col-xl-2 d-flex ">
								<div class="form-group">
									<label>MOBILE <span class="fored"><b>*</b></span> :</label>
								</div>
							</div>
							<div class="col-12 col-sm-8 col-md-10 col-lg-10 col-xl-10">
								<div class="form-group " id="staticParent">
									<div class="controls">
										<input type="text" name="mobile" id="mobile" maxlength="10" class="form-control" placeholder="ENTER MOBILE" required data-validation-required-message="This field is required"> 
									</div>
								</div>
							</div>
							<div class="col-12 col-sm-4 col-md-2 col-lg-2 col-xl-2 d-flex ">
								<div class="form-group">
									<label>EMAIL ID <span class="fored"><b>*</b></span> :</label>
								</div>
							</div>
							<div class="col-12 col-sm-8 col-md-10 col-lg-10 col-xl-10">
								<div class="form-group ">
									<div class="controls">
										<input type="text" id="emailid" name="emailid" class="form-control" placeholder="ENTER EMAIL ID" required data-validation-required-message="This field is required"> 
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
						<div class="row">
							
							<div class="col-12 col-sm-4 col-md-2 col-lg-2 col-xl-2 d-flex ">
								<div class="form-group">
									<label>PHONE 1 <span class="fored"><b>*</b></span> :</label>
								</div>
							</div>
							<div class="col-12 col-sm-8 col-md-10 col-lg-10 col-xl-10">
								<div class="form-group"  id="staticParent1">
									<div class="controls">
										<input type="text" name="phone1"  id="phone1" maxlength="10" class="form-control" placeholder="ENTER PHONE 1" required data-validation-required-message="This field is required"> 
									</div>
								</div>
							</div>
							<div class="col-12 col-sm-4 col-md-2 col-lg-2 col-xl-2 d-flex ">
								<div class="form-group">
									<label>PHONE 2 <span class="fored"><b>*</b></span> :</label>
								</div>
							</div>
							<div class="col-12 col-sm-8 col-md-10 col-lg-10 col-xl-10">
								<div class="form-group "  id="staticParent2">
									<div class="controls">
										<input type="text" name="phone2" id="phone2" maxlength="10" class="form-control" placeholder="ENTER PHONE 2" required data-validation-required-message="This field is required"> 
									</div>
								</div>
							</div>
							<div class="col-12 col-sm-4 col-md-2 col-lg-2 col-xl-2 d-flex ">
								<div class="form-group">
									<label>FAX<span class="fored"><b>*</b></span> :</label>
								</div>
							</div>
							<div class="col-12 col-sm-8 col-md-10 col-lg-10 col-xl-10">
								<div class="form-group ">
									<div class="controls">
										<input type="text" name="fax" id="fax" class="form-control" placeholder="ENTER FAX" required data-validation-required-message="This field is required"> 
									</div>
								</div>
							</div>
							
						</div>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="form-group">
					<div class="text-xs-right" style="margin-left: 8px;">
						<button type="submit" class="btn btn-success">Submit</button>
						<a  href="<?php echo base_url()?>Hastelist_controller" class="btn btn-info">
                            Cancel
                        </a>
					</div>
				</div>
			</div>
	
		</div>	
	</div>
</form>
	
</div>

<?php
if(!empty($edithastedata))
{
?>
<!-- <input type="hidden" value="inside" id="inside"> -->

<div class="tab-pane  p-20" id="editform" role="tabpanel">
<form class="" method="post" name="edithasteform" id="edithasteform" novalidate>

	<input type="hidden" value="<?=$edithastedata['HasteID'];?>" id="hasteid" name="hasteid">
		
	<div class="row common_master_form_div">
		<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 ">
			
			<div class="formtitle">
				<h4 class="backcolor">Account Information</h4>
				<div class="row removemargin">
					<div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
						<div class="row">
							<div class="col-12 col-sm-4 col-md-2 col-lg-2 col-xl-2 d-flex ">
								<div class="form-group">
									<label>ADATIYA NAME  <span class="fored"><b>*</b></span> :</label>
								</div>
							</div>
							<div class="col-12 col-sm-8 col-md-10 col-lg-10 col-xl-10">
								<div class="form-group field">
									<div class="controls">
										<select name="adatyaname" id="adatyaname" required class="form-control">

											<?php
											foreach ($ledgernamedata as $ledvalue1)
											{
												?>
												<option <?php if($ledvalue1->Name == $edithastedata['AdatiyaName']){echo "selected";}?> value="<?=$ledvalue->Ledger_Id;?>"><?=$ledvalue1->Name;?></option>
												<?php
											}
											?>
										</select>
									</div>
								</div>
							</div>
							<div class="col-12 col-sm-4 col-md-2 col-lg-2 col-xl-2 d-flex">
								<div class="form-group">
									<label> HASTE <span class="fored"><b>*</b></span> :</label>
								</div>
							</div>
							<div class="col-12 col-sm-8 col-md-10 col-lg-10 col-xl-10">
								<div class="form-group field">
									<div class="controls">
										<input type="text"  name="haste" id="haste" placeholder="ENTER HASTE" class="form-control" required data-validation-required-message="This field is required" value="<?=$edithastedata['Haste'];?>"> 
									</div>
								</div>
							</div>
							<div class="col-12 col-sm-4 col-md-2 col-lg-2 col-xl-2 d-flex ">
								<div class="form-group">
									<label>TRANSPORT  <span class="fored"><b>*</b></span> :</label>
								</div>
							</div>
							<div class="col-12 col-sm-8 col-md-10 col-lg-10 col-xl-10">
								<div class="form-group field">
									<div class="controls">
										<select name="transport" id="transport" required class="form-control">
											<option value=""> --Select -- </option>
											<?php
											foreach ($transportdata as $value)
											{												
												?>
												<option <?php if($edithastedata['Transport'] == $value->transportID){echo "selected";}?> value="<?=$value->transportID?>"> <?=$value->transportName?> </option>
												<?php												
											}
											?>
											
										</select>
									</div>
								</div>
							</div>
							<div class="col-12 col-sm-4 col-md-2 col-lg-2 col-xl-2 d-flex ">
								<div class="form-group">
									<label>STATION<span class="fored"><b>*</b></span> :</label>
								</div>
							</div>
							<div class="col-12 col-sm-8 col-md-10 col-lg-10 col-xl-10">
								<div class="form-group field">
									<div class="controls">

										<input placeholder="Enter Station" class="form-control"type="text" value="<?php echo $edithastedata['Station'];?>" name="station" id="station">
										<!-- <textarea name="station" id="station"><?php echo $edithastedata['Station'];?></textarea> -->
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
						<div class="row">
							<div class="col-12 col-sm-4 col-md-2 col-lg-2 col-xl-2 d-flex ">
								<div class="form-group">
									<label>SCREEN SERIES<span class="fored"><b>*</b></span> :</label>
								</div>
							</div>
							<div class="col-12 col-sm-8 col-md-10 col-lg-10 col-xl-10">
								<div class="form-group field">
									<div class="controls">
										<select name="screenseries" id="screenseries" required class="form-control">
											<option value=""> --Select -- </option>
											<?php
											foreach ($screenregisterentry as $value)
											{												
												?>
												<option <?php if($edithastedata['ScreenSeries'] == $value->ScreenRegisterID){echo "selected";}?> value="<?=$value->ScreenRegisterID;?>"><?=$value->KodasMain;?></option>
												<?php
											}
											?>
										</select>
									</div>
								</div>
							</div>
							<div class="col-12 col-sm-4 col-md-2 col-lg-2 col-xl-2 d-flex">
								<div class="form-group">
									<label>GSTIN<span class="fored"><b>*</b></span> :</label>
								</div>
							</div>
							<div class="col-12 col-sm-8 col-md-10 col-lg-10 col-xl-10">
								<div class="form-group field">
									<div class="controls">
										<input type="text" id="gstin"  name="gstin" placeholder="ENTER GSTIN" class="form-control" required data-validation-required-message="This field is required" value="<?=$edithastedata['GstIn'];?>"> 
									</div>
								</div>
							</div>	
							<div class="col-12 col-sm-4 col-md-2 col-lg-2 col-xl-2 d-flex">
								<div class="form-group">
									<label>ADDRESS 1<span class="fored"><b>*</b></span> :</label>
								</div>
							</div>
							<div class="col-12 col-sm-8 col-md-10 col-lg-10 col-xl-10">
								<div class="form-group field">
									<div class="controls">
										<input type="text"  name="address1" id="address1" placeholder="ENTER ADDRESS 1" class="form-control" required data-validation-required-message="This field is required" value="<?=$edithastedata['Address1'];?>"> 
									</div>
								</div>
							</div>
							<div class="col-12 col-sm-4 col-md-2 col-lg-2 col-xl-2 d-flex">
								<div class="form-group">
									<label>ADDRESS 2<span class="fored"><b>*</b></span> :</label>
								</div>
							</div>
							<div class="col-12 col-sm-8 col-md-10 col-lg-10 col-xl-10">
								<div class="form-group field">
									<div class="controls">
										<input type="text"  name="address2" id="address2" placeholder="ENTER ADDRESS 2" class="form-control" required data-validation-required-message="This field is required"  value="<?=$edithastedata['Address2'];?>"> 
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>	

			</div>
			
			<div class="formtitle">
				<div class="row removemargin">
					<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
						<div class="row" style="margin-top: 2px;margin-bottom: -3px;">
							<div class="col-12 col-sm-4 col-md-1 col-lg-1 col-xl-1 d-flex ">
								<div class="form-group">
									<label>REMARK <span class="fored"><b>*</b></span> :</label>
								</div>
							</div>
							<div class="col-12 col-sm-8 col-md-11 col-lg-11 col-xl-11">
								<div class="form-group">
									<div class="controls">
										<input type="text" id="remark" name="remark" placeholder="ENTER REMARK" class="form-control" required data-validation-required-message="This field is required" value="<?=$edithastedata['Remark'];?>"> 
									</div>
								</div>
							</div>
						</div>
					</div>
				
				</div>
			</div>
			
			<div class="formtitle">
				<h4 class="backcolor">CONTACT  Information</h4>
				<div class="row removemargin">
					<div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
						<div class="row">
							<div class="col-12 col-sm-4 col-md-2 col-lg-2 col-xl-2 d-flex ">
								<div class="form-group">
									<label>CONTACT <span class="fored"><b>*</b></span> :</label>
								</div>
							</div>
							<div class="col-12 col-sm-8 col-md-10 col-lg-10 col-xl-10">
								<div class="form-group ">
									<div class="controls">
										<input type="text" name="contactinformation" id="contactinformation" class="form-control" placeholder="ENTER CONTACT" required data-validation-required-message="This field is required"  value="<?=$edithastedata['Contact'];?>"> 
									</div>
								</div>
							</div>
							<div class="col-12 col-sm-4 col-md-2 col-lg-2 col-xl-2 d-flex ">
								<div class="form-group">
									<label>MOBILE <span class="fored"><b>*</b></span> :</label>
								</div>
							</div>
							<div class="col-12 col-sm-8 col-md-10 col-lg-10 col-xl-10">
								<div class="form-group " id="staticParent">
									<div class="controls">
										<input type="text" name="mobile" id="mobile" maxlength="10" class="form-control" placeholder="ENTER MOBILE" required data-validation-required-message="This field is required"  value="<?=$edithastedata['Mobile'];?>"> 
									</div>
								</div>
							</div>
							<div class="col-12 col-sm-4 col-md-2 col-lg-2 col-xl-2 d-flex ">
								<div class="form-group">
									<label>EMAIL ID <span class="fored"><b>*</b></span> :</label>
								</div>
							</div>
							<div class="col-12 col-sm-8 col-md-10 col-lg-10 col-xl-10">
								<div class="form-group ">
									<div class="controls">
										<input type="text" id="emailid" name="emailid" class="form-control" placeholder="ENTER EMAIL ID" required data-validation-required-message="This field is required"  value="<?=$edithastedata['EmailID'];?>"> 
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
						<div class="row">
							
							<div class="col-12 col-sm-4 col-md-2 col-lg-2 col-xl-2 d-flex ">
								<div class="form-group">
									<label>PHONE 1 <span class="fored"><b>*</b></span> :</label>
								</div>
							</div>
							<div class="col-12 col-sm-8 col-md-10 col-lg-10 col-xl-10">
								<div class="form-group"  id="staticParent1">
									<div class="controls">
										<input type="text" name="phone1"  id="phone1" maxlength="10" class="form-control" placeholder="ENTER PHONE 1" required data-validation-required-message="This field is required"   value="<?=$edithastedata['Phone1'];?>"> 
									</div>
								</div>
							</div>
							<div class="col-12 col-sm-4 col-md-2 col-lg-2 col-xl-2 d-flex ">
								<div class="form-group">
									<label>PHONE 2 <span class="fored"><b>*</b></span> :</label>
								</div>
							</div>
							<div class="col-12 col-sm-8 col-md-10 col-lg-10 col-xl-10">
								<div class="form-group "  id="staticParent2">
									<div class="controls">
										<input type="text" name="phone2" id="phone2" maxlength="10" class="form-control" placeholder="ENTER PHONE 2" required data-validation-required-message="This field is required" value="<?=$edithastedata['Phone2'];?>"> 
									</div>
								</div>
							</div>
							<div class="col-12 col-sm-4 col-md-2 col-lg-2 col-xl-2 d-flex ">
								<div class="form-group">
									<label>FAX<span class="fored"><b>*</b></span> :</label>
								</div>
							</div>
							<div class="col-12 col-sm-8 col-md-10 col-lg-10 col-xl-10">
								<div class="form-group ">
									<div class="controls">
										<input type="text" name="fax" id="fax" class="form-control" placeholder="ENTER FAX" required data-validation-required-message="This field is required"  value="<?=$edithastedata['Fax'];?>"> 
									</div>
								</div>
							</div>
							
						</div>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="form-group">
					<div class="text-xs-right" style="margin-left: 8px;">
						<button type="submit" class="btn btn-success">Submit</button>
						<a  href="<?php echo base_url()?>Hastelist_controller" class="btn btn-info">
                            Cancel
                        </a>
					</div>
				</div>
			</div>
	
		</div>	
	</div>
</form>


</div>
<?php
}
?>
</div>
</div>
</div>
</div>







</div>
</div>
<?php $this->load->view('common/footer'); ?>
<script>

var inside = $("#hasteid").val();

if(inside != "")
{
$("#home7").removeClass('active');
$(".nav-link").removeClass('active');
$(".foractive").addClass('active');
$("#editform").addClass('active');
}

$(function() {
$('#staticParent').on('keydown', '#child', function(e){-1!==$.inArray(e.keyCode,[46,8,9,27,13,110])||(/65|67|86|88/.test(e.keyCode)&&(e.ctrlKey===true||e.metaKey===true))&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()});
})
$(function() {
$('#staticParent1').on('keydown', '#child1', function(e){-1!==$.inArray(e.keyCode,[46,8,9,27,13,110])||(/65|67|86|88/.test(e.keyCode)&&(e.ctrlKey===true||e.metaKey===true))&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()});
})
$(function() {
$('#staticParent2').on('keydown', '#child2', function(e){-1!==$.inArray(e.keyCode,[46,8,9,27,13,110])||(/65|67|86|88/.test(e.keyCode)&&(e.ctrlKey===true||e.metaKey===true))&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()});
})

</script>


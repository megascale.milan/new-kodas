 <script src="<?php echo base_url(); ?>assets/node_modules/jquery/jquery-3.2.1.min.js"></script>
<script type="text/javascript">
function fun_edit(stateID)
{
	//alert();
	var frmobj=window.document.frm_state_list;
	frmobj.stateID.value=stateID;
	frmobj.action="<?php echo base_url()?>State_controller/index";
	frmobj.submit();

}



function fun_single_delete(stateID)
{
	if(confirm("Are you sure want to delete this record?"))
	{
		$.ajax({
			type: "POST",
			url: "<?php echo base_url() ?>State_controller/singledelete",
			data: "stateID=" + stateID,
			success: function(total){
			$("#ID_"+stateID).html(total);
			}
		
		});
	}
}

/******************* CHECK ALL CHECKBOX *****************/
jQuery(document).ready(function($)
{
	$('#checkall').click(function()
	{
		$(':checkbox').each(function()
		{
			if(this.checked)
			{
				this.checked = false;
			}
			else
			{
				this.checked = true;
			}
		});
		return false;
	}); 
}); 

/****************** MULTIPLE DELETE *************/
	function fun_multipleDelete()
	{
	  var count = $(":checkbox:checked").length;

	  if(count > 0)
	  {
		  var status = confirm("Are you sure want to delete this record?");
		  if(status==true)
		  {
		  	
			  frmobj=window.document.frm_state_list;
			  frmobj.method.value="multipleDelete";
			  document.frm_state_list.action="<?php echo base_url();?>State_controller/index";
			  frmobj.submit();
		  }
	  }
	  else
	  {
		alert('Please select atleast one record.');
	  }
	}

/****************** fun single Status *************/
function fun_single_status(stateID)
{
	//alert(stateID);
	$.ajax({
		type: "POST",
		url: "<?php echo base_url(); ?>State_controller/singleStatus",
		data: "stateID=" + stateID,
		success: function(result){
			///alert(result);
			if(result == 0)
			{
				$('#status_'+stateID).html("Inactive");
			}
			if(result == 1)
			{
				$('#status_'+stateID).html("Active");
			}
		}
	});
}

</script>
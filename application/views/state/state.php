<?php
$stateID=$this->input->post('stateID')?$this->input->post('stateID'):'';

$stateIDedit=$this->input->post('stateID')?$this->input->post('stateID'):'0';
$stateName=$this->input->post('stateName')?$this->input->post('stateName'):'';
$isActive=$this->input->post('isActive')?$this->input->post('isActive'):'';

if($stateIDedit != NULL && $stateIDedit >0)
{
    if(isset($getsinglestate))
    {
        print_r($getsinglestate);

        foreach($getsinglestate as $getsinglestatedata)
        {
            $stateName=$getsinglestatedata[0]->stateName;
            $isActive=$getsinglestatedata[0]->isActive;
        }
    }
}


?>
<?php $this->load->view('common/header'); ?>
<?php $this->load->view('state/state-js');?>
<style>
    #success_message{ display: none;}
</style>
<div class="page-wrapper">
    <div class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h4 class="text-themecolor">State Master</h4>
            </div>
            <div class="col-md-7 align-self-center text-right">
                <div class="d-flex justify-content-end align-items-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Master</a></li>
                        <li class="breadcrumb-item active">State Master</li>
                    </ol>
                    <button type="button" class="btn btn-info d-none d-lg-block m-l-15" data-toggle="modal" data-target="#exampleModal" data-whatever="@getbootstrap"><i class="fa fa-plus-circle"></i> Create New</button>
                    
                    

                        <a href="javascript:fun_multipleDelete();" class="btn btn-info d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i> All Delete</a>
                       
                   

                    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title" id="exampleModalLabel1">State Master</h4>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                </div>
                                <div class="modal-body">
                                    <form name="frm_state_add" id="frm_state_add"  action="<?php echo base_url();?>State_controller/savestate" method="post" class=" error" novalidate>

                                        <input type="hidden" name="stateIDedit" value="<?php echo $stateIDedit; ?>">
                                        <div class="form-group">
                                            <h5 style="    text-align: left;">Enter State <span class="text-danger">*</span></h5>
                                            <div class="controls" style="    text-align: left;">
                                                <input type="text" name="stateName" class="form-control" required data-validation-required-message="This field is required" value="<?php echo $stateName; ?>"> 
                                            </div>
                                        </div>




                                        <div class="form-group" style="float: left;">
                                              <input type="checkbox" value="1" <?php if($isActive==0){ echo "checked='checked'";} ?>  name="isActive" > isActive?
                                        </div>




                                        <div style="clear:both"></div>
                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-info">Submit</button>
                                        </div>
                                    </form>
                                    
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <form  name="frm_state_list" method="post" action="<?php echo base_url();?>State_controller/index">
            <input type="hidden" value="<?php echo $stateID; ?>" name="stateID" />
            <input type="hidden" name="method" value="" />
         <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <!-- <h4 class="card-title">Data Export</h4>
                                <h6 class="card-subtitle">Export data to Copy, CSV, Excel, PDF & Print</h6> -->
                                <div class="table-responsive m-t-40">
                                    <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th><input type="checkbox" name="checkall" id= "checkall" value=""  /> Checkbox</th>
                                                <th>State Name</th>
                                                <th>Is Active</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php 
                                            foreach($displayState as $displayStatedata)
                                            {
                                            ?>
                                            <tr id="ID_<?php echo $displayStatedata->stateID; ?>">
                                                <td><input class="checkbox" name="checkUncheck[]"  id="checkAllAuto" value="<?php echo $displayStatedata->stateID; ?>" type="checkbox" /></td>
                                                <td><?php echo $displayStatedata->stateName; ?></td>
                                                <td>
                                                    <a href="javascript:fun_single_status(<?php echo $displayStatedata->stateID; ?>);">
                                                    <span id="status_<?php echo $displayStatedata->stateID; ?>">
                                                        <?php if($displayStatedata->isActive==1){ echo"Active"; }else{ echo"Inactive";} ?>
                                                    </span>
                                                </a>
                                                    
                                                </td>
                                                <td class="editdelaction">
                                                    <a href="javascript:fun_edit(<?php echo 
                                                    $displayStatedata->stateID; ?>);" " ><i class="fa fa-edit"></i></a>&nbsp;&nbsp;
                                                    
                                                    <a href="javascript:fun_single_delete(<?php echo $displayStatedata->stateID; ?>);"><i class="fa fa-trash-o"></i></a>
                                                </td>
                                            </tr>
                                            <?php 
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        

                    </div>
                </div>
               </form> 


       
    </div>
</div>
 <?php $this->load->view('common/footer'); ?>

 
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Itemtype_controller extends CI_Controller {

	function __construct()
	{
        parent::__construct();
		$this->load->model('Itemtype_model');
    }
	public function index()
	{
		if(isset($_REQUEST['itemtypeid']))
		{
			$data['itemtypedata']=$this->Home_model->select('item_type');	
			$data['edititemtypedata']=$this->Home_model->select_where_row('item_type',array('ItemtypeID'=>$_REQUEST['itemtypeid']));		
		}
		else
		{
			$data['itemtypedata']=$this->Home_model->select('item_type');	
			$data['edititemtypedata']="";
		}

		$this->load->view('itemtype/itemtype',$data);
	}

	public function saveitemtype()
	{
		$data = array(
		'ClothType'=>(!empty($this->input->post('clothtype'))) ? $this->input->post('clothtype') : '',
		'Unit'=>(!empty($this->input->post('unit'))) ? $this->input->post('unit') : '',
		'Pack_Cost'=>(!empty($this->input->post('packcost'))) ? $this->input->post('packcost') : '',		
		'Cost_Per'=>(!empty($this->input->post('costper'))) ? $this->input->post('costper') : '',	
		'IsActive'=>(!empty($this->input->post('isActive'))) ? $this->input->post('isActive') : '0',
		'CreateDate'=>date('Y-m-d')
		);

		if($this->input->post('itemtypeid') != "")
		{
			$result=$this->Home_model->update('item_type',$data,array('ItemtypeID'=>$this->input->post('itemtypeid')));
			print_r($result);
		}
		else
		{
			$result=$this->Home_model->insert('item_type',$data);
			print_r($result);
		}
	}
}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Accounttype_controller extends CI_Controller {

	function __construct()
	{
        parent::__construct();
		$this->load->model('Accounttype_model');
    }

    public function saveaccount()
	{
		$data = array(
		'AccType' => $this->input->post('AccType'),
		'BalSheet' => $this->input->post('BalSheet'),
		'TRIALside' => $this->input->post('TRIALside'),
		'TrialPos' => $this->input->post('TrialPos'),
		'In_PL' => $this->input->post('inpl'),
		'In_Tranding' => $this->input->post('intranding'),
		'Creater' => 'Admin',
		'CreateTime' =>date('Y-m-d')
		);

		if($this->input->post('accid') != "")
		{
			$result=$this->Home_model->update('acc_type',$data,array('AccNo'=>$this->input->post('accid')));
			print_r($result);
		}
		else
		{
			$result=$this->Home_model->insert('acc_type',$data);
			print_r($result);		
		}
	}

	public function index()
	{

		if(isset($_REQUEST['accid']))
		{			
			$data['getsingleaddtype']=$this->Accounttype_model->getsingle_Product();
			$result = $this->Accounttype_model->getallaccounttype();
			$data['recordcount'] = $result['rows'];
			$data['getallaccounttype'] = $result['data'];
			$data['editaccountdata']=$this->Company_model->select_where_row('acc_type',array('AccNo'=>$_REQUEST['accid']));
		}
		else
		{			
			$data['getsingleaddtype']=$this->Accounttype_model->getsingle_Product();
			$result = $this->Accounttype_model->getallaccounttype();
			$data['recordcount'] = $result['rows'];
			$data['getallaccounttype'] = $result['data'];
			$data['editaccountdata']="";
		}

		$this->load->view('accounttype/accounttype',$data);
	}
	public function addeditproduct()
	{
		$this->load->view('accounttype/accounttype',$data);
	}
	public function saveuser()
	{
		$result=$this->Accounttype_model->addeditacctype($_REQUEST);
		
		if(is_array($result))
		{
			redirect(base_url().'Accounttype_controller/addeditproduct');
		}
		else
		{
			$path = base_url()."Accounttype_controller/index";
			redirect($path);
		}
		
	}
	function singledelete()
	{
		$AccNo = $this->input->post('AccNo');
		//print_r($userID);exit;
		$result=$this->Accounttype_model->singledelete('acc_type',$AccNo);
	}

}

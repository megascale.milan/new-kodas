<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home_controller extends CI_Controller {

	/* Created Date 24-11-2018 */
	public function index()
	{
		$this->load->view('dashboard');
	}

	public function deletedata()
	{
		$id = $this->input->post('id');
		$type = $this->input->post('type');

		if($type == "companydelete")
		{
			$where = array(
				'CompanyID'=>$id
			);

			$delete = $this->Home_model->deletedata('company_manager',$where);
			print_r($delete);
		}
		else if($type == "screendelete")
		{
			$where = array(
				'ScreenRegisterID'=>$id
			);

			$delete = $this->Home_model->deletedata('screenregister_entry',$where);
			print_r($delete);
		}
		else if($type == "categorydelete")
		{
			$where = array(
				'CategoryID'=>$id
			);

			$delete = $this->Home_model->deletedata('category',$where);
			print_r($delete);
		}
		else if($type == "itemtypedelete")
		{
			$where = array(
				'ItemtypeID'=>$id
			);

			$delete = $this->Home_model->deletedata('item_type',$where);
			print_r($delete);
		}
		else if($type == "packagedelete")
		{
			$where = array(
				'PackagestyleID'=>$id
			);

			$delete = $this->Home_model->deletedata('package_style',$where);
			print_r($delete);
		}
		else if($type == "hastedelete")
		{
			$where = array(
				'HasteID'=>$id
			);

			$delete = $this->Home_model->deletedata('haste',$where);
			print_r($delete);
		}
		
	}
}
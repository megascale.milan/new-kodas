<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Screenregister_controller extends CI_Controller {

	function __construct()
	{
        parent::__construct();
		$this->load->model('Screenregister_model');
    }
	public function index()
	{
		if(isset($_REQUEST['screenid']))
		{			
			$response['editscreendata']=$this->Home_model->select_where_row('screenregister_entry',array('ScreenRegisterID'=>$_REQUEST['screenid']));

			$response['screendata'] = $this->Home_model->select('screenregister_entry');
			$response['categorydata'] = $this->Home_model->select('category');			
		}
		else
		{			
			$response['editscreendata']="";
			$response['screendata'] = $this->Home_model->select('screenregister_entry');
			$response['categorydata'] = $this->Home_model->select('category');
		}

		$this->load->view('screenregister/screenregister',$response);
	}

	public function savescreen()
	{
		$data = array(
			'KodasMain'=>(!empty($this->input->post('kodasmain'))) ? $this->input->post('kodasmain') : '',
			'Cut'=>(!empty($this->input->post('cut'))) ? $this->input->post('cut') : '',
			'Category'=>(!empty($this->input->post('category'))) ? $this->input->post('category') : '',
			'Rate'=>(!empty($this->input->post('rate'))) ? $this->input->post('rate') : '',
			'FourMatching'=>(!empty($this->input->post('matching'))) ? $this->input->post('matching') : '',
			'KodasThely'=>(!empty($this->input->post('kodasthely'))) ? $this->input->post('kodasthely') : '',
			'BigBox'=>(!empty($this->input->post('bigbox'))) ? $this->input->post('bigbox') : '',
			'Kodas2'=>(!empty($this->input->post('kodastwo'))) ? $this->input->post('kodastwo') : '',
			'Screen6'=>(!empty($this->input->post('screensix'))) ? $this->input->post('screensix') : '',
			'Screen7'=>(!empty($this->input->post('screenseven'))) ? $this->input->post('screenseven') : '',
			'Packing'=>(!empty($this->input->post('packing'))) ? $this->input->post('packing') : '',
			'WorkCut'=>(!empty($this->input->post('workcut'))) ? $this->input->post('workcut') : '',
			'IsActive'=>0,
			'CreateDate'=>date('Y-m-d')
		);

		$result=$this->Home_model->insert('screenregister_entry',$data);
		print_r($result);
	}

	public function editscreensave()
	{
		$data = array(
			'KodasMain'=>(!empty($this->input->post('kodasmain'))) ? $this->input->post('kodasmain') : '',
			'Cut'=>(!empty($this->input->post('cut'))) ? $this->input->post('cut') : '',
			'Category'=>(!empty($this->input->post('category'))) ? $this->input->post('category') : '',
			'Rate'=>(!empty($this->input->post('rate'))) ? $this->input->post('rate') : '',
			'FourMatching'=>(!empty($this->input->post('matching'))) ? $this->input->post('matching') : '',
			'KodasThely'=>(!empty($this->input->post('kodasthely'))) ? $this->input->post('kodasthely') : '',
			'BigBox'=>(!empty($this->input->post('bigbox'))) ? $this->input->post('bigbox') : '',
			'Kodas2'=>(!empty($this->input->post('kodastwo'))) ? $this->input->post('kodastwo') : '',
			'Screen6'=>(!empty($this->input->post('screensix'))) ? $this->input->post('screensix') : '',
			'Screen7'=>(!empty($this->input->post('screenseven'))) ? $this->input->post('screenseven') : '',
			'Packing'=>(!empty($this->input->post('packing'))) ? $this->input->post('packing') : '',
			'WorkCut'=>(!empty($this->input->post('workcut'))) ? $this->input->post('workcut') : '',
			'IsActive'=>0,
			'CreateDate'=>date('Y-m-d')
		);

		$result=$this->Home_model->update('screenregister_entry',$data,array('ScreenRegisterID'=>$this->input->post('editscreenid')));
				print_r($result);
	}
}
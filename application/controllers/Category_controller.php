<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category_controller extends CI_Controller {

	function __construct()
	{
        parent::__construct();
		$this->load->model('Category_model');
    }
	public function index()
	{
		if(isset($_REQUEST['catid']))
		{
			$data['editcategrydata']=$this->Home_model->select_where_row('category',array('CategoryID'=>$_REQUEST['catid']));
			$data['categorydata']=$this->Home_model->select('category');			
		}
		else
		{
			$data['editcategrydata']="";
			$data['categorydata']=$this->Home_model->select('category');
		}
		
		$this->load->view('category/category',$data);
	}

	public function savecategory()
	{
		$data = array(
		'Category'=>(!empty($this->input->post('category'))) ? $this->input->post('category') : '',
		'Rate'=>(!empty($this->input->post('rate'))) ? $this->input->post('rate') : '',
		'CategoryCode'=>(!empty($this->input->post('categorycode'))) ? $this->input->post('categorycode') : '',		
		'CategoryType'=>(!empty($this->input->post('categorytype'))) ? $this->input->post('categorytype') : '',	
		'IsActive'=>(!empty($this->input->post('isActive'))) ? $this->input->post('isActive') : '0',
		'CreateDate'=>date('Y-m-d')
		);

		if($this->input->post('catid') != "")
		{
			$result=$this->Company_model->update('category',$data,array('CategoryID'=>$this->input->post('catid')));
			print_r($result);
		}
		else
		{
			$result=$this->Home_model->insert('category',$data);
			print_r($result);		
		}
	}
}
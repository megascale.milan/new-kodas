<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Remark_controller extends CI_Controller {

	function __construct()
	{
        parent::__construct();
		$this->load->model('Remark_model');
    }
	public function index()
	{
		$this->load->view('remark/remark');
	}
}
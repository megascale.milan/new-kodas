<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Grey_Quality extends CI_Controller {

	function __construct()
	{
        parent::__construct();
    }
	public function index()
	{
		if(isset($_REQUEST['qreyqualityid']))
		{
			$data['greyqualitydata']=$this->Home_model->select('grey_quality');
			// $data['editgreyqualitydata'] = $this->Home_model->select_where_haste();
		}
		else
		{
			$data['greyqualitydata']=$this->Home_model->select('grey_quality');
			$data['editgreyqualitydata'] = "";
		}

		$this->load->view('greyquality/greyquality',$data);
	}

	public function savegreyqty()
	{
		$data = array(
		'Grey_Quality'=>(!empty($this->input->post('greyquality'))) ? $this->input->post('greyquality') : '',
		'Grey_Quality'=>(!empty($this->input->post('isActive'))) ? $this->input->post('isActive') : '',
		'CreateDate'=>date('Y-m-d')
		);

		if($this->input->post('qreyqualityid') != "")
		{
			$result=$this->Home_model->update('haste',$data,array('HasteID'=>$this->input->post('hasteid')));
			print_r($result);
		}
		else
		{
			$result=$this->Home_model->insert('haste',$data);
			print_r($result);		
		}
	}
}
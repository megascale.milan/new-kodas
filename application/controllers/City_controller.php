<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class City_controller extends CI_Controller {

	function __construct()
	{
        parent::__construct();
		$this->load->model('City_model');
    }

	/* Created Date 24-11-2018 */
	public function index()
	{
		if($this->input->post('method')== "multipleDelete")
		{
			$result=$this->City_model->multipleDelete();
		}
		
		
		$result['State']=$this->City_model->selectstate();
		$result['displayCity']=$this->City_model->displayCity();
		$this->load->view('city/city',$result);
	}
	public function savecity()
	{
		$result=$this->City_model->addcity();
		redirect('City_controller/index');
	}
	function singledelete()
	{
		$cityID = $this->input->post('cityID');
		$result=$this->City_model->singledelete('city',$cityID);
	}
	
	function singleStatus()
	{
		$cityID = $this->input->post('cityID');
		//print_r($cityID);exit;
		$result=$this->City_model->singleStatus($cityID);
	}


}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Packagestyle_controller extends CI_Controller {

	function __construct()
	{
        parent::__construct();
		$this->load->model('Packagestyle_model');
    }
	public function index()
	{
		if(isset($_REQUEST['packageid']))
		{
			$data['packagedata']=$this->Home_model->select('package_style');	
			$data['editpackagedata']=$this->Home_model->select_where_row('package_style',array('PackagestyleID'=>$_REQUEST['packageid']));		
		}
		else
		{
			$data['packagedata']=$this->Home_model->select('package_style');	
			$data['editpackagedata']="";
		}

		$this->load->view('packagestyle/packagestyle',$data);
	}

	public function savepackage()
	{
		$data = array(
		'packing'=>(!empty($this->input->post('packing'))) ? $this->input->post('packing') : '',
		'PackAdd'=>(!empty($this->input->post('packadd'))) ? $this->input->post('packadd') : '',
		'BoxRate'=>(!empty($this->input->post('boxrate'))) ? $this->input->post('boxrate') : '',	
		'IsActive'=>(!empty($this->input->post('isActive'))) ? $this->input->post('isActive') : '0',
		'CreateDate'=>date('Y-m-d')
		);

		if($this->input->post('packageid') != "")
		{
			$result=$this->Home_model->update('package_style',$data,array('PackagestyleID'=>$this->input->post('packageid')));
			print_r($result);
		}
		else
		{
			$result=$this->Home_model->insert('package_style',$data);
			print_r($result);		
		}
	}
}
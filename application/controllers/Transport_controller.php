<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transport_controller extends CI_Controller {

	function __construct()
	{
        parent::__construct();
		$this->load->model('Transport_model');
    }
	
	public function index()
	{
		if($this->input->post('method')== "multipleDelete")
		{
			$result=$this->Transport_model->multipleDelete();
		}
		
		$data['getsingletransport']=$this->Transport_model->getsingle_transport();
		$result = $this->Transport_model->getalltransport();
		$data['recordcount'] = $result['rows'];
		$data['getalltransport'] = $result['data'];
		$this->load->view('transport/transport',$data);
	}
	
	public function addedittransport()
	{
		$this->load->view('transport/transport');
	}
	public function saveuser()
	{
		$result=$this->Transport_model->addedittransport($_REQUEST);
		
		if(is_array($result))
		{
			redirect(base_url().'Transport_controller/addedittransport');
		}
		else
		{
			$path = base_url()."Transport_controller/index";
			redirect($path);
		}
		
	}
	function singledelete()
	{
		$transportID = $this->input->post('transportID');
		//print_r($userID);exit;
		$result=$this->Transport_model->singledelete('transport',$transportID);
	}
	function singleStatus()
	{
		$transportID = $this->input->post('transportID');
		//print_r($cityID);exit;
		$result=$this->Transport_model->singleStatus($transportID);
	}
	
	
}
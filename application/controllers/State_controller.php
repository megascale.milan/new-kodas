<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class State_controller extends CI_Controller {

	function __construct()
	{
        parent::__construct();
		$this->load->model('State_model');
    }

	/* Created Date 24-11-2018 */
	public function index()
	{
		if($this->input->post('method')== "multipleDelete")
		{
			$result=$this->State_model->multipleDelete();
		}
		$result['displayState']=$this->State_model->displayState();
		$result['getsinglestate']=$this->State_model->getsingle_state();

		$this->load->view('state/state',$result);
	}
	


	public function savestate()
	{
		$result=$this->State_model->addstate();
		redirect('State_controller/index');
	}
	function singledelete()
	{
		$stateID = $this->input->post('stateID');
		$result=$this->State_model->singledelete('state',$stateID);
	}

	function singleStatus()
	{
		$stateID = $this->input->post('stateID');
		$result=$this->State_model->singleStatus($stateID);
	}






}

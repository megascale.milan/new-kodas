<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Accountgroup_controller extends CI_Controller {

	function __construct()
	{
        parent::__construct();
		$this->load->model('Accountgroup_model');
    }
	  public function index()
	{
		$result=$this->Accountgroup_model->getselect_City();
		$data['selectCity']=$result['data'];
		$result1=$this->Accountgroup_model->getselect_state();
		$data['selectState']=$result1['state'];
		$result1=$this->Accountgroup_model->getselect_transport();
		$data['selectTransport']=$result1['transport'];
		
		$this->load->view('accountgroup/accountgroup',$data);
	}
}
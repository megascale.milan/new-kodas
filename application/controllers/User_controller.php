<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_controller extends CI_Controller {

	function __construct()
    {
        parent::__construct();
		$this->load->model('User_model');
    }
	
	/* Created Date 24-11-2018 */
	public function index()
	{
		if($this->input->post('method')== "multipleDelete")
		{
			$result=$this->User_model->multipleDelete();
		}
		
		$result['displayUser']=$this->User_model->displayUser();
		$this->load->view('user/user',$result);
	}
	public function saveuser()
	{
		$result=$this->User_model->adduser();
		redirect('User_controller/index');
	}
	function singledelete()
	{
		$UserId = $this->input->post('UserId');
		$result=$this->User_model->singledelete('user',$UserId);
	}
	function singleStatus()
	{
		$UserId = $this->input->post('UserId');
		//print_r($cityID);exit;
		$result=$this->User_model->singleStatus($UserId);
	}
}

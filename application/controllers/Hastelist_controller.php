<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hastelist_controller extends CI_Controller {

	function __construct()
	{
        parent::__construct();
		$this->load->model('Hastelist_model');
    }
	public function index()
	{
		if(isset($_REQUEST['hasteid']))
		{
			$data['ledgernamedata']=$this->db->query('SELECT Ledger_Id,Name From ledger')->result();
			$data['hastedata']=$this->Home_model->select('haste');
			$data['screenregisterentry']=$this->Home_model->select('screenregister_entry');
			$data['transportdata']=$this->Home_model->select('transport');
			// $data['edithastedata']=$this->Home_model->select_where_row('haste',array('HasteID'=>$_REQUEST['hasteid']));
			$data['edithastedata'] = $this->Home_model->select_where_haste();
		}
		else
		{
			$data['ledgernamedata']=$this->db->query('SELECT Ledger_Id,Name From ledger')->result();
			// $data['hastedata']=$this->Home_model->select('haste');	
			$data['hastedata']=$this->Home_model->selecthastelistdata();	
			$data['screenregisterentry']=$this->Home_model->select('screenregister_entry');
			$data['transportdata']=$this->Home_model->select('transport');
			// $data['remarkdata']=$this->Home_model->select('remark');
			$data['edithastedata'] = "";
		}

		$this->load->view('hastelist/hastelist',$data);
	}

	public function savehaste()
	{
		$data = array(
		'AdatiyaName'=>(!empty($this->input->post('adatyaname'))) ? $this->input->post('adatyaname') : '',
		'Haste'=>(!empty($this->input->post('haste'))) ? $this->input->post('haste') : '',
		'Transport'=>(!empty($this->input->post('transport'))) ? $this->input->post('transport') : '',
		'Station'=>(!empty($this->input->post('station'))) ? $this->input->post('station') : '',
		'ScreenSeries'=>(!empty($this->input->post('screenseries'))) ? $this->input->post('screenseries') : '',
		'GstIn'=>(!empty($this->input->post('gstin'))) ? $this->input->post('gstin') : '',	
		'Address1'=>(!empty($this->input->post('address1'))) ? $this->input->post('address1') : '',	
		'Address2'=>(!empty($this->input->post('address2'))) ? $this->input->post('address2') : '',	
		'Remark'=>(!empty($this->input->post('remark'))) ? $this->input->post('remark') : '',	
		'Contact'=>(!empty($this->input->post('contactinformation'))) ? $this->input->post('contactinformation') : '',	
		'Mobile'=>(!empty($this->input->post('mobile'))) ? $this->input->post('mobile') : '',	
		'EmailID'=>(!empty($this->input->post('emailid'))) ? $this->input->post('emailid') : '',	
		'Phone1'=>(!empty($this->input->post('phone1'))) ? $this->input->post('phone1') : '',	
		'Phone2'=>(!empty($this->input->post('phone2'))) ? $this->input->post('phone2') : '',	
		'Fax'=>(!empty($this->input->post('fax'))) ? $this->input->post('fax') : '',
		'CreateDate'=>date('Y-m-d')
		);

		if($this->input->post('hasteid') != "")
		{
			$result=$this->Home_model->update('haste',$data,array('HasteID'=>$this->input->post('hasteid')));
			print_r($result);
		}
		else
		{
			$result=$this->Home_model->insert('haste',$data);
			print_r($result);		
		}
	}

	public function transportaddress()
	{
		$transportid = $this->input->post('transportid');
		$transport = $this->Home_model->select_where_row('transport',array('transportID'=>$transportid));
		// echo "<option value=".$transport['Taddress'].">".$transport['Taddress']."</option>";
		echo $transport['Taddress'];
	}
}
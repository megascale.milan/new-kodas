<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Company_controller extends CI_Controller {

	function __construct()
	{
        parent::__construct();
		$this->load->model('Company_model');
    }
	public function index()
	{
		if(isset($_REQUEST['compid']))
		{
			$data['companyname'] = $this->db->query('SELECT CompanyID,Name from company_manager')->result_array();
			$data['editcompanydata']=$this->Company_model->select_where_row('company_manager',array('CompanyID'=>$_REQUEST['compid']));
			$data['companydata']=$this->Company_model->select('company_manager');
			$data['citydata']=$this->Company_model->select('city');
			
		}
		else
		{
			$data['companyname'] = $this->db->query('SELECT CompanyID,Name from company_manager')->result_array();
			$data['editcompanydata']="";
			$data['companydata']=$this->Company_model->select('company_manager');
			$data['citydata']=$this->Company_model->select('city');
		}
		
		$this->load->view('company/company',$data);
	}

	public function savecomapny()
	{
		if(empty($this->input->post('companygroup')))
		{
			$companygroup = "";
		}
		else
		{
			$companygroup = implode(',', $this->input->post('companygroup'));
		}

		$data = array(
			'Code'=>(!empty($this->input->post('code'))) ? $this->input->post('code') : '',
			'ShortName'=>(!empty($this->input->post('shortname'))) ? $this->input->post('shortname') : '',
			'Name'=>(!empty($this->input->post('name'))) ? $this->input->post('name') : '',
			'CompanyType'=>(!empty($this->input->post('companytype'))) ? $this->input->post('companytype') : '',
			'CompanyGoup'=>$companygroup,
			'Address'=>(!empty($this->input->post('address'))) ? $this->input->post('address') : '',
			'AddressCont'=>(!empty($this->input->post('addcont'))) ? $this->input->post('addcont') : '',
			'City'=>(!empty($this->input->post('city'))) ? $this->input->post('city') : '',
			'Pin'=>(!empty($this->input->post('pin'))) ? $this->input->post('pin') : '',
			'Email'=>(!empty($this->input->post('email'))) ? $this->input->post('email') : '',
			'MobileNo'=>(!empty($this->input->post('mobileno'))) ? $this->input->post('mobileno') : '',
			'Fax'=>(!empty($this->input->post('fax'))) ? $this->input->post('fax') : '',
			'PhoneNo'=>(!empty($this->input->post('phoneno'))) ? $this->input->post('phoneno') : '',
			'Address1'=>(!empty($this->input->post('address'))) ? $this->input->post('address') : '',
			'AddressCont1'=>(!empty($this->input->post('addcont'))) ? $this->input->post('addcont') : '',
			'BusinessDesc'=>(!empty($this->input->post('bussinessdesc'))) ? $this->input->post('bussinessdesc') : '',
			'Proprietor'=>(!empty($this->input->post('proprietor'))) ? $this->input->post('proprietor') : '',
			'MultiChal'=>(!empty($this->input->post('multichal'))) ? $this->input->post('multichal') : '',
			'Selected'=>(!empty($this->input->post('isActive'))) ? $this->input->post('isActive') : '',
			'JvFormDate'=>(!empty($this->input->post('fromdate'))) ? $this->input->post('fromdate') : '',
			'PanNo'=>(!empty($this->input->post('panno')))? $this->input->post('panno') : '',
			'TdsacNo'=>(!empty($this->input->post('tdsacno'))) ? $this->input->post('tdsacno') : '',
			'Ward'=>(!empty($this->input->post('ward'))) ? $this->input->post('ward') : '',
			'EccNo'=>(!empty($this->input->post('eccno'))) ? $this->input->post('eccno') : '',
			'Range'=>(!empty($this->input->post('range'))) ? $this->input->post('range') : '',
			'Division'=>(!empty($this->input->post('division'))) ? $this->input->post('division') : '',
			'Collectrate'=>(!empty($this->input->post('collectrate'))) ? $this->input->post('collectrate') : '',
			'PolicyNo'=>(!empty($this->input->post('policyno'))) ? $this->input->post('policyno') : '',
			'Date'=>(!empty($this->input->post('date'))) ? $this->input->post('date') : '',
			'GstNoVat'=>(!empty($this->input->post('gstno'))) ? $this->input->post('gstno') : '',
			'Dt'=>(!empty($this->input->post('dt'))) ? $this->input->post('dt') : '',
			'CinNo'=>(!empty($this->input->post('cinno'))) ? $this->input->post('cinno') : '',
			'GstInUin'=>(!empty($this->input->post('gstinuin'))) ? $this->input->post('gstinuin') : '',
			'CenregNo'=>(!empty($this->input->post('cenexcise'))) ? $this->input->post('cenexcise') : '',
			'InsurancePolicy'=>(!empty($this->input->post('insurance'))) ? $this->input->post('insurance') : '',
			'IsActive'=>0,
			'CreateDate'=>date('Y-m-d')
		);
			
			$result=$this->Company_model->insert('company_manager',$data);
			print_r($result);
	}

	public function editsavecomapny()
	{
		if(empty($this->input->post('companygroup')))
		{
			$companygroup = "";
		}
		else
		{
			$companygroup = implode(',', $this->input->post('companygroup'));
		}

		$data = array(
			'Code'=>(!empty($this->input->post('code'))) ? $this->input->post('code') : '',
			'ShortName'=>(!empty($this->input->post('shortname'))) ? $this->input->post('shortname') : '',
			'Name'=>(!empty($this->input->post('name'))) ? $this->input->post('name') : '',
			'CompanyType'=>(!empty($this->input->post('companytype'))) ? $this->input->post('companytype') : '',
			'CompanyGoup'=>$companygroup,
			'Address'=>(!empty($this->input->post('address'))) ? $this->input->post('address') : '',
			'AddressCont'=>(!empty($this->input->post('addcont'))) ? $this->input->post('addcont') : '',
			'City'=>(!empty($this->input->post('city'))) ? $this->input->post('city') : '',
			'Pin'=>(!empty($this->input->post('pin'))) ? $this->input->post('pin') : '',
			'Email'=>(!empty($this->input->post('email'))) ? $this->input->post('email') : '',
			'MobileNo'=>(!empty($this->input->post('mobileno'))) ? $this->input->post('mobileno') : '',
			'Fax'=>(!empty($this->input->post('fax'))) ? $this->input->post('fax') : '',
			'PhoneNo'=>(!empty($this->input->post('phoneno'))) ? $this->input->post('phoneno') : '',
			'Address1'=>(!empty($this->input->post('address'))) ? $this->input->post('address') : '',
			'AddressCont1'=>(!empty($this->input->post('addcont'))) ? $this->input->post('addcont') : '',
			'BusinessDesc'=>(!empty($this->input->post('bussinessdesc'))) ? $this->input->post('bussinessdesc') : '',
			'Proprietor'=>(!empty($this->input->post('proprietor'))) ? $this->input->post('proprietor') : '',
			'MultiChal'=>(!empty($this->input->post('multichal'))) ? $this->input->post('multichal') : '',
			'Selected'=>(!empty($this->input->post('isActive'))) ? $this->input->post('isActive') : '',
			'JvFormDate'=>(!empty($this->input->post('fromdate'))) ? $this->input->post('fromdate') : '',
			'PanNo'=>(!empty($this->input->post('panno')))? $this->input->post('panno') : '',
			'TdsacNo'=>(!empty($this->input->post('tdsacno'))) ? $this->input->post('tdsacno') : '',
			'Ward'=>(!empty($this->input->post('ward'))) ? $this->input->post('ward') : '',
			'EccNo'=>(!empty($this->input->post('eccno'))) ? $this->input->post('eccno') : '',
			'Range'=>(!empty($this->input->post('range'))) ? $this->input->post('range') : '',
			'Division'=>(!empty($this->input->post('division'))) ? $this->input->post('division') : '',
			'Collectrate'=>(!empty($this->input->post('collectrate'))) ? $this->input->post('collectrate') : '',
			'PolicyNo'=>(!empty($this->input->post('policyno'))) ? $this->input->post('policyno') : '',
			'Date'=>(!empty($this->input->post('date'))) ? $this->input->post('date') : '',
			'GstNoVat'=>(!empty($this->input->post('gstno'))) ? $this->input->post('gstno') : '',
			'Dt'=>(!empty($this->input->post('dt'))) ? $this->input->post('dt') : '',
			'CinNo'=>(!empty($this->input->post('cinno'))) ? $this->input->post('cinno') : '',
			'GstInUin'=>(!empty($this->input->post('gstinuin'))) ? $this->input->post('gstinuin') : '',
			'CenregNo'=>(!empty($this->input->post('cenexcise'))) ? $this->input->post('cenexcise') : '',
			'InsurancePolicy'=>(!empty($this->input->post('insurance'))) ? $this->input->post('insurance') : '',
			'IsActive'=>0,
			'CreateDate'=>date('Y-m-d')
		);

		if($this->input->post('editcompanyid') != "")
		{
				$result=$this->Company_model->update('company_manager',$data,array('CompanyID'=>$this->input->post('editcompanyid')));
				print_r($result);
		}


	}
	
}
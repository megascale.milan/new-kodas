<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Itemdetail_controller extends CI_Controller {

	function __construct()
	{
        parent::__construct();
		$this->load->model('Itemdetail_model');
    }
	public function index()
	{
		$data['item_detail'] = $this->db->query("
		SELECT item_detail.ItemdetailID,item_detail.Code,item_detail.ItemsrNo,item_detail.Name,item_detail.MainScreen,item_detail.Cut,item_detail.Packing,item_detail.Grayquality,item_detail.Type,item_detail.ItemType,item_detail.Screenserie,item_detail.Category,item_detail.SellingRate,item_detail.Unit,item_detail.Rate2,item_detail.Rate3,item_detail.Selected,item_detail.WorkCut,item_detail.PackCutCost,item_detail.SaleRate,item_detail.Hsn,item_detail.Gst,item_detail.IsActive,package_style.PackagestyleID,package_style.packing,item_type.ItemtypeID,category.CategoryID,category.Category
		FROM item_detail
		INNER JOIN package_style ON item_detail.Packing=package_style.PackagestyleID
		INNER JOIN item_type ON item_detail.ItemType=item_type.ItemtypeID
		INNER JOIN category ON item_detail.Category=category.CategoryID
		");

		if(isset($_REQUEST['actypedetailsid']))
		{
			$data['ledgernamedata']=$this->db->query('SELECT Ledger_Id,Name From ledger')->result();
			$data['screenregisterentry']=$this->Home_model->select('screenregister_entry');
			$data['itemtypedata']=$this->Home_model->select('item_type');
			$data['packagestyledata']=$this->Home_model->select('package_style');
			$data['transportdata']=$this->Home_model->select('transport');
			$data['categorydata']=$this->Home_model->select('category');
			$data['editactypedetails'] = $this->Home_model->select_where_haste();
		}
		else
		{
			$data['ledgernamedata']=$this->db->query('SELECT Ledger_Id,Name From ledger')->result();
			$data['screenregisterentry']=$this->Home_model->select('screenregister_entry');
			$data['itemtypedata']=$this->Home_model->select('item_type');
			$data['packagestyledata']=$this->Home_model->select('package_style');
			$data['transportdata']=$this->Home_model->select('transport');
			$data['categorydata']=$this->Home_model->select('category');
			$data['editactypedetails'] = "";
		}

		$this->load->view('itemdetail/itemdetail',$data);
	}

	public function saveitemdetails()
	{
		$data = array(
		'AdatiyaName'=>(!empty($this->input->post('adatyaname'))) ? $this->input->post('adatyaname') : '',
		'Haste'=>(!empty($this->input->post('haste'))) ? $this->input->post('haste') : '',
		'Transport'=>(!empty($this->input->post('transport'))) ? $this->input->post('transport') : '',
		'Station'=>(!empty($this->input->post('station'))) ? $this->input->post('station') : '',
		'ScreenSeries'=>(!empty($this->input->post('screenseries'))) ? $this->input->post('screenseries') : '',
		'GstIn'=>(!empty($this->input->post('gstin'))) ? $this->input->post('gstin') : '',	
		'Address1'=>(!empty($this->input->post('address1'))) ? $this->input->post('address1') : '',	
		'Address2'=>(!empty($this->input->post('address2'))) ? $this->input->post('address2') : '',	
		'Remark'=>(!empty($this->input->post('remark'))) ? $this->input->post('remark') : '',	
		'Contact'=>(!empty($this->input->post('contactinformation'))) ? $this->input->post('contactinformation') : '',	
		'Mobile'=>(!empty($this->input->post('mobile'))) ? $this->input->post('mobile') : '',	
		'EmailID'=>(!empty($this->input->post('emailid'))) ? $this->input->post('emailid') : '',	
		'Phone1'=>(!empty($this->input->post('phone1'))) ? $this->input->post('phone1') : '',	
		'Phone2'=>(!empty($this->input->post('phone2'))) ? $this->input->post('phone2') : '',	
		'Fax'=>(!empty($this->input->post('fax'))) ? $this->input->post('fax') : '',
		'CreateDate'=>date('Y-m-d')
		);

		if($this->input->post('hasteid') != "")
		{
			$result=$this->Home_model->update('haste',$data,array('HasteID'=>$this->input->post('hasteid')));
			print_r($result);
		}
		else
		{
			$result=$this->Home_model->insert('haste',$data);
			print_r($result);		
		}
	}
}